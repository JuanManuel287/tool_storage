# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 16:10:50 2019

@author: JOliva
"""
import numpy as np 
import matplotlib.pylab as plt
import matplotlib as mpl
from matplotlib.backends.backend_pdf import PdfPages
mpl.use('Agg')

import os
from datetime import datetime,timedelta
import h5py

import time
from sys import getsizeof
import sys


from tensorflow.keras.models import load_model
import new_tool_utility as ntu
import new_tool_files as ntf


#from tool_utility import tool_utility as tu
import new_tool_graphic as ntg
import gc

import tool_cluster as tc
import tool_velocity as tv

        

def get_nearest_point(point,DicAllPoints,R):
    #Función que busca el punto mas cercano dado un radio R al punto xy
    #
    NearestPoint=''
    xc,yc=ntu.strXY_to_floatXY(point)
#    print('punto float : ',xc,yc)
    for x in DicAllPoints:
        xx=float(x)
        if np.sqrt((xc-xx)**2)<R:
            for y in DicAllPoints[x]:
                yy=float(y)
                d=np.sqrt((xc-xx)**2 + (yc-yy)**2)
                if d<=R:
                    R=d
                    NearestPoint=x+'-'+y
  
    
    return NearestPoint
def get_nearest_point2(xy,x_min,y_min,cell_size,k):
    x,y=ntu.strXY_to_floatXY(xy,k)
    i,j=ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size)
    x,y=ntu.get_xy_from_ij(i,j,x_min,y_min,cell_size)
    return ntu.floatXY_to_strXY(x,y,k)
        
def create_dic_allpoint(XY):
    DicAllPoints={}
    
    for i,xy in enumerate(XY):
        x,y=xy.split('-')
        if x not in DicAllPoints:
            DicAllPoints[x]={}
        DicAllPoints[x][y]=i
    return DicAllPoints


def create_all_cells(fileCSV4D,k,NumCell=1200):
    # data=get_xyzd_from_csv4d(fileCSV4D,5)
    data=ntf.get_xyzd_from_csv4d(fileCSV4D,5)
    x_min=np.inf
    x_max=-1
    y_min=np.inf
    y_max=-1
    
    dist_min=np.inf
    x_aux=0
    y_aux=0
    dist_x=np.inf
    dist_y=np.inf
    
    # XY=np.array([strXY_to_floatXY(xy,5) for xy in data])
    XY=np.array([xy for xy in data])
    X=sorted(list({x:0 for x in XY[:,0]}.keys()))
    Y=sorted(list({y:0 for y in XY[:,1]}.keys()))
    
    for i in range(len(X)-1):
        if abs(X[i]-X[i+1])<dist_x:
            dist_x=abs(X[i]-X[i+1])
        if abs(Y[i]-Y[i+1])<dist_y:
            dist_y=abs(Y[i]-Y[i+1])
    
    cell_size=np.round(dist_x,5)
    x_max=X[-1]
    x_min=X[0]
    y_max=Y[-1]
    y_min=Y[0]
    
    
    numcell_x=int((x_max-x_min)/cell_size)
    numcell_y=int((y_max-y_min)/cell_size)
    if numcell_x>NumCell or  numcell_y>NumCell:
        NumCell=max(numcell_x,numcell_y)+100
        
    if numcell_x<NumCell: 
        cell_expand_x=int((NumCell-numcell_x)/2.)+1
    if numcell_y<NumCell: 
        cell_expand_y=int((NumCell-numcell_y)/2.)+1
    
    # x_min=np.trunc((x_min-cell_expand_x*cell_size)*10**k)/10**k
    # x_max=np.trunc((x_max+cell_expand_x*cell_size)*10**k)/10**k
    # y_min=np.trunc((y_min-cell_expand_y*cell_size)*10**k)/10**k
    # y_max=np.trunc((y_max+cell_expand_y*cell_size)*10**k)/10**k
    
    x_min=np.round((x_min-cell_expand_x*cell_size),k)
    x_max=np.round((x_max+cell_expand_x*cell_size),k)
    y_min=np.round((y_min-cell_expand_y*cell_size),k)
    y_max=np.round((y_max+cell_expand_y*cell_size),k)
    
    
    m=int((x_max-x_min)/cell_size)+1
    n=int((y_max-y_min)/cell_size)+1
    
    info=[m,n,x_min,x_max,y_min,y_max,cell_size,dist_x,dist_y]
    
    XY=ntu.create_list_XY(x_min,m,y_min,n,cell_size,k)
    #
    return XY,info


    
    
def get_XY_from_H5(path_fileh5):
    with h5py.File(path_fileh5, 'r') as  hf:
        X=np.array(hf.get('X'),dtype=np.float64)
        Y=np.array(hf.get('Y'),dtype=np.float64)
    return np.hstack((X,Y))

def get_dates_from_H5(path_fileh5):
    with h5py.File(path_fileh5, 'r') as  hf:
        ListaDatesStr=list(hf.keys())
        ListaDatesStr.remove('X')
        ListaDatesStr.remove('Y')
        ListaDates=[ntu.str_to_datetime(f) for f in ListaDatesStr]
    # ListaDatesSorted=[datetime_to_str(f) for  f in sorted(ListaDates)]

    return sorted(ListaDates)




def save_historical_data(fileCSV4D,PathOUT_DataCube,INFO):
    '''
    fileCSV4D: direccion y nombre del archivo csv4d
    PathOUT=directorio de salida
    '''
    
    _,EndDate,_=ntf.get_info_from_csv4d(fileCSV4D)
    EndDate=ntu.datetime_to_str(EndDate)
    
    _,_,_,n_cols,lim_axes,cell_size,_,_=INFO  
    x_min,_,y_min,_=lim_axes    

    
    XY=get_XY_from_H5(PathOUT_DataCube+'.h5')
    with h5py.File(PathOUT_DataCube+'.h5', 'a') as  hf:
        
        
        data=ntf.get_xyzd_from_csv4d(fileCSV4D,5)
        desplazamiento=np.empty(len(XY),dtype=np.float32)
        desplazamiento[:]=np.nan
        dist=[]
        indexes={}
        for x,y,_,desp in data:
            
            i,j=ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size)
            index=j+i*n_cols
            desplazamiento[index]=desp
            
            xl,yl=XY[index,0],XY[index,1]
            d=np.sqrt((x-xl)**2 + (y-yl)**2)
            dist.append(d)
            
        dist_max=np.max(dist)
        dist_min=np.min(dist)

            
        
        cont=len(np.argwhere(~np.isnan(desplazamiento)))
            
        print('csv4d: ',EndDate,'celdas guardadas: ',cont,'de',len(data),'; dist max y min: ',dist_max,dist_min,';fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
        hf.create_dataset(EndDate, data=np.array(desplazamiento,dtype=np.float32))




def get_names_datacubes(ListFiles):
    DiccNamesData={}
    NameSorted=[]
    DateBig=datetime(1900,1,1)
    for fileCSV4D in ListFiles:
        _,EndDate,Name=ntf.get_info_from_csv4d(fileCSV4D)
        if ntf.check_data_in_csv4d(fileCSV4D):
            if Name not in DiccNamesData :
                DiccNamesData[Name]=datetime(1900,1,1)
        
            if DiccNamesData[Name]<EndDate:
                DiccNamesData[Name]=EndDate
            if DateBig<EndDate:
                DateBig=EndDate
                
    for name in DiccNamesData:
        if DiccNamesData[name]>=DateBig:
            LastName=name
        else:
            NameSorted.append(name)
    NameSorted.append(LastName)
    
    return NameSorted
    

def get_files_for_datacube(PathIN,name=None):
    ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
    if name is None:
        return ListFiles
    else:
        NewListFile=[fileCSV4D for fileCSV4D in ListFiles if name in fileCSV4D]
        return NewListFile


            
    
def init_datacube(PathOUT,PathOUT2,fileCSV4D,NameRegistry,NameRegistryLog,NameRegistryGlobal):
    StartDate,_,Name=ntf.get_info_from_csv4d(fileCSV4D)
    StartDate=ntu.datetime_to_str(StartDate)
    
    XY, info = create_all_cells(fileCSV4D,5)
    ntf.save_registry_global(PathOUT+NameRegistryGlobal,Name,StartDate,info)
    
    PathOUT_DataCube=PathOUT2+Name+'.h5'
    with h5py.File(PathOUT_DataCube, 'w') as  hf:
        hf.create_dataset('X',shape=(len(XY),1), dtype=np.float64, data=XY[:,0])
        hf.create_dataset('Y',shape=(len(XY),1), dtype=np.float64, data=XY[:,1])
        # hf.create_dataset('X-Y',shape=(len(List_XY),1), dtype='S24', data=List_XY)
        print('Se inicializa el cubo de datos : ',Name,' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
                    
    with open(PathOUT2+NameRegistry,'w') as f:
        print('Se inicializa el registro de cubo de datos : ',NameRegistry,' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
        
    with open(PathOUT2+NameRegistryLog,'w') as f:
        print('Se inicializa el registro log de cubo de datos : ',NameRegistryLog,' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
        
    
    
def process_data_cube(PathIN,PathOUT):
    PathOUT1=PathOUT
    
    fileprint='prints_datacube.txt'
    NameRegistry='read_csv4d_datacube.dat'
    NameRegistryLog='registry_csv4d_datacube.log'
    NameRegistryGlobal='Header_Data.csv'
    
    while True:
        ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
        if len(ListFiles)>1:
            NameSorted=get_names_datacubes(ListFiles)
            NameSortedStart=[name for name in NameSorted]
            break
        time.sleep(3)
        
    print('name datacube: ',NameSortedStart)
    while True: 
        name=NameSortedStart[0]
        PathOUT=PathOUT1+name+'/'
        PathOUT2=PathOUT+'Data Cube/'
        ntf.make_dir(PathOUT2)
        
        ListFiles=get_files_for_datacube(PathIN,name)
        NewListFiles=ntf.get_new_files_with_data(ListFiles,PathOUT2+NameRegistry)
        
        if len(NewListFiles)>1:
            NewListFilesSorted=ntf.sorted_files_list_for_date(NewListFiles)
            
            if not os.path.exists(PathOUT2+name+'.h5'):
                f = open(PathOUT2+fileprint, 'a')
                sys.stdout = f
                fileCSV4D=NewListFilesSorted[0]
                init_datacube(PathOUT,PathOUT2,fileCSV4D,NameRegistry,NameRegistryLog,NameRegistryGlobal)
                f.close()
            INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
            end=-1
            if name!=NameSortedStart[-1]:
                end=len(NewListFilesSorted)
                NameSortedStart.pop(0)
            else:
                ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
                NameSortedAux=get_names_datacubes(ListFiles)
                for n in NameSortedAux:
                    if n not in NameSorted:
                        NameSortedStart.append(n)
                
            for fileCSV4D in NewListFilesSorted:
                
                f = open(PathOUT2+fileprint, 'a')
                sys.stdout = f
                if NewListFilesSorted[-1]==fileCSV4D:
                    ntf.check_read_file(fileCSV4D)
                try:
                    save_historical_data(fileCSV4D,PathOUT2+name,INFO)
                    ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
                    ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
                except:
                    pass
                
                f.close()
                    
       
        time.sleep(3)

    
def update_dtm(fileCSV4D,EndDate,filename_dtm,INFO):
    
    _,_,_,n_cols,lim_axes,cell_size,_,_=INFO  
    x_min,_,y_min,_=lim_axes  
    if type(EndDate)!=type(str()):
        EndDate=ntu.datetime_to_str(EndDate)
    
    XY=get_XY_from_H5(filename_dtm) 
    with h5py.File(filename_dtm, 'a') as  hf:
        XY_Z=ntf.get_xyzd_from_csv4d(fileCSV4D,5)
        ListZ=np.empty(len(XY),dtype=np.float32)
        ListZ[:]=np.nan
        dist=[]
        for x,y,z,desp in XY_Z:
            i,j=ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size)
            index=j+i*n_cols
            ListZ[index]=z
            xl,yl=XY[index,0],XY[index,1]
            dist.append(np.sqrt((x-xl)**2 + (y-yl)**2))
            
        dist_max=np.max(dist)
        dist_min=np.min(dist)
        
        cont=len(np.argwhere(~np.isnan(ListZ)))
        hf.create_dataset(EndDate, data=ListZ,dtype=np.float32)
        print('Fecha del archivo csv4d: ',EndDate,';total celdas guardadas: ',cont,'de',len(XY_Z),'; dist max y min: ',dist_max,dist_min,';fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
        
        


def init_data_dtm(PathOUT,PathOUT2,fileCSV4D,NameRegistryLog,NameRegistryGlobal,filename_dtm):
    INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
    Name,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
                
    
    
    _,EndDate,_=ntf.get_info_from_csv4d(fileCSV4D)
    XY=ntu.create_list_XY(x_min,n_cols,y_min,n_rows,cell_size,5)
                    
    # se inicializa el archivo h5 
    with h5py.File(filename_dtm, 'w') as  hf:
        hf.create_dataset('X',shape=(len(XY),1), dtype=np.float64, data=XY[:,0])
        hf.create_dataset('Y',shape=(len(XY),1), dtype=np.float64, data=XY[:,1])
        # hf.create_dataset('X-Y',shape=(len(List_XY),1), dtype='S24', data=List_XY)
        print('Se inicializó el h5 como dtm : ',filename_dtm,'; fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
                    
                    
        with open(PathOUT2+NameRegistryLog,'w') as f:
            print('Se inicializó el registro log de cubo de datos : ',NameRegistryLog,' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
                   
        update_dtm(fileCSV4D,EndDate,filename_dtm,INFO)
        ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
 
def process_dtm(PathIN,PathOUT):
    PathOUT1=PathOUT
    
    fileprint='prints_dtm.txt'
    NameRegistry='read_csv4d_dtm.dat'
    NameRegistryLog='registry_csv4d_dtm.log'
    NameRegistryGlobal='Header_Data.csv'
    
    while True:
        ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
        if len(ListFiles)>1:
            NameSorted=get_names_datacubes(ListFiles)
            NameSortedStart=[name for name in NameSorted]
            break
        time.sleep(3)
    
    intervalTime=(1,8,0) # day, hour, minutes
    while True: 
        name=NameSortedStart[0]
        PathOUT=PathOUT1+name+'/'
        PathOUT2=PathOUT+'Data_DTM/'
        ntf.make_dir(PathOUT2)
        
        
        ListFiles=get_files_for_datacube(PathIN,name)
        NewListFiles=ntf.get_new_files_with_data(ListFiles,PathOUT2+NameRegistry)
        
        if len(NewListFiles)>1 and os.path.exists(PathOUT+NameRegistryGlobal):
            NewListFilesSorted=ntf.sorted_files_list_for_date(NewListFiles)
            
            f = open(PathOUT2+fileprint, 'a')
            sys.stdout = f
            fileCSV4D=NewListFilesSorted[0]
            filename_dtm=PathOUT2+name+'_DTM'+'.h5'
            if not os.path.exists(filename_dtm):
                init_data_dtm(PathOUT,PathOUT2,fileCSV4D,NameRegistryLog,NameRegistryGlobal,filename_dtm)
            f.close()
               
            end=-1
            if name!=NameSortedStart[-1]:
                end=len(NewListFilesSorted)
                NameSortedStart.pop(0)
            else:
                ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
                NameSortedAux=get_names_datacubes(ListFiles)
                for n in NameSortedAux:
                    if n not in NameSorted:
                        NameSortedStart.append(n)
                  
            ListDates=get_dates_from_H5(filename_dtm)     
            lastDate=ListDates[-1] + ntu.delta_time(ListDates[-1], intervalTime)
                    
            NewListFiles=ntf.get_files_for_time_interval(NewListFilesSorted,lastDate,intervalTime)
            
            for fileCSV4D in NewListFiles:
                f = open(PathOUT2+fileprint, 'a')
                sys.stdout = f
                if NewListFilesSorted[-1]==fileCSV4D:
                    ntf.check_read_file(fileCSV4D)
                    
                _,EndDate,_=ntf.get_info_from_csv4d(fileCSV4D)
                
                # ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
                try:
                    INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
                    update_dtm(fileCSV4D,EndDate,filename_dtm,INFO)
                    ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
                    
                except:
                    pass
                
                f.close()
        
       
        time.sleep(1)
    



def set_time_series(fileCSV4D,TS,Cond,TW,INFO,EndDate):
    
    data=ntf.get_xyzd_from_csv4d(fileCSV4D,5)
    n,m=TS.shape
    TW[0:-1]=TW[1:]
    TW[-1]=EndDate
    
    _,_,_,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes

    
    TS[:,0:-1]=TS[:,1:]
    
    TS[:,-1]=np.nan
    desplazamiento={}
    
    dist=[]
    t1=time.time()
    for x,y,_,desp in data:
        i,j=ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size)
        index=j+i*n_cols
        TS[index,-1]=desp
        desplazamiento[index]=None
        # Cond[index]=len(np.argwhere(~np.isnan(TS[index,:])))
    
    # Se repite el valor anterior de aquellas celdas que no tienen datos
    indexs=np.argwhere(np.isnan(TS[:,-1]))
    TS[indexs,-1]=TS[indexs,-2]
    
    indexs=np.argwhere(~np.isnan(TS[:,-1]))
    Cond[indexs]=Cond[indexs]+1
    
    indexs=np.argwhere(np.isnan(TS[:,-1]))
    Cond[indexs]=Cond[indexs]-1
        
    cont=len(desplazamiento)
    Cond[Cond<0]=0
    Cond[Cond>m]=m
    
    # indexs=[i[0] for i in np.argwhere(Cond==m)]
    # if len(indexs)>0:
    #     for i in indexs:
    #         TS[i,:]=TS[i,:]-TS[i,0]
    # print('csv4d: ',EndDate,'celdas guardadas: ',cont,'de',len(data),'; dist max y min: ',dist_max,dist_min,'; celdas con 144 reg: ',len(np.argwhere(Cond==m)),';fecha:', datetime.now())
    print('csv4d: ',EndDate,'celdas guardadas: ',cont,'de',len(data),'; celdas con 144 reg: ',len(np.argwhere(Cond==m)),';fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
    return TS,Cond,TW




def update_times_series(fileCSV4D,PathOUT2,INFO,NameTS):
    
    StartDate1,EndDate1,_=ntf.get_info_from_csv4d(fileCSV4D)

    pahtfilename=ntf.get_all_files(PathOUT2,ext='.npz')
    if len(pahtfilename)==0:
        print('Error: No existe un arhivo .npz en ',PathOUT2,' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
    elif len(pahtfilename)>1:
        print('Error: Existen más de un arhivo .npz en ',PathOUT2,' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
    else:
        
        oldfilename,_ = os.path.splitext(os.path.basename(pahtfilename[0]))

        Cond,XY,TS,TW=ntu.get_data_times_series(PathOUT2+ oldfilename+'.npz')
        os.remove(PathOUT2+oldfilename+'.npz')
        
        TS, Cond, TW = set_time_series(fileCSV4D,TS,Cond,TW,INFO,EndDate1)
        
        pos=[i for i in range(len(TW)) if TW[i] is not None]
        if len(pos)>0:

            StartDate=ntu.datetime_to_str(TW[pos[0]],fmt='%Y_%b_%d-%H_%M')
        else:
            StartDate=ntu.datetime_to_str(TW[0],fmt='%Y_%b_%d-%H_%M')
            
        EndDate=ntu.datetime_to_str(TW[-1],fmt='%Y_%b_%d-%H_%M')
        newfilename=StartDate+'_to_'+EndDate
        
       
        np.savez(PathOUT2+newfilename+'.npz',Cond,XY,TS,TW)
        save_name_timeserie(PathOUT2+NameTS,newfilename)
        
        return PathOUT2+newfilename+'.npz'
        
        
#        TS[:,0:-1]=TS[:,1:]
#        for i,xy in enumerate(XY):
#            if xy in data:
#                if abs(data[xy]-TS[i,-2])<=4.3:
#                    TS[i,-1]=data[xy]-TS[i,-2]
#                TS[i,:]=TS[i,:]-TS[i,0]
#            else:
#                TS[i,-1]=TS[i,-2]
                
def get_time_series_eval(path_npz):
    Cond,XY,TS,TW=ntu.get_data_times_series(path_npz)
    n,m=TS.shape
    indexs=[i[0] for i in np.argwhere(Cond==m)]
    XY=XY[indexs]
    TS=TS[indexs,:]
    for i in range(len(TS)):
        TS[i]=TS[i]-TS[i,0]
    return XY,TS,TW,{index: i for i,index in enumerate(indexs)}

def init_time_serie(PathOUT,PathOUT2,NameRegistry,NameRegistryLog,INFO,NameTS,NewListFilesSorted):
    t1=time.time()
    _,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    
    
    XY=ntu.create_list_XY(x_min,n_cols,y_min,n_rows,cell_size,5)
    Cond=np.array(np.zeros(len(XY),dtype=np.int32))
    TS=np.empty((len(XY),144), dtype=np.float32)
    TS[:,:]=np.nan
    TW=np.empty(144, dtype=datetime)
    
    
    
                
    #los arreglos almcanados son el siguiente orden : Condicion, 
    # coordendas, time series y ventana temporal, cuyos accesos son mediante
    # 'arr_0','arr_1','arr_2','arr_3'
    
    print('Tiempo en inicializar y guardar npz:',time.time()-t1,' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
    with open(PathOUT2+NameRegistry,'w') as f:
        print('Se inicializa el registro de csv4d para la TS',' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
        pass
                    
    with open(PathOUT2+NameRegistryLog,'w') as f:
        print('Se inicializa el registro log de csv4d para la TS',' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
        pass    
    
    with open(PathOUT2+NameTS,'w') as f:
        print('Se inicializa el registro log de nombres para la TS',' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
        pass
    
    if len(NewListFilesSorted)>144:
       NewList=NewListFilesSorted[-145:-1]
    else:
       NewList=NewListFilesSorted[:-1]
    
    for fileCSV4D in NewList:
        
        _,EndDate,_=ntf.get_info_from_csv4d(fileCSV4D)
        TS, Cond, TW = set_time_series(fileCSV4D,TS,Cond,TW,INFO,EndDate)
        
        ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
        ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
        
        
    _,StartDate,_=ntf.get_info_from_csv4d(NewList[0])
    _,EndDate,_=ntf.get_info_from_csv4d(NewList[-1])
    
    StartDate=ntu.datetime_to_str(StartDate,fmt='%Y_%b_%d-%H_%M')
    EndDate=ntu.datetime_to_str(EndDate,fmt='%Y_%b_%d-%H_%M')
    
    newfilename=StartDate+'_to_'+EndDate
    np.savez(PathOUT2+newfilename+'.npz',Cond,XY,TS,TW)
    save_name_timeserie(PathOUT2+NameTS,newfilename)

def save_name_timeserie(NameTS,filename):
    with open(NameTS,'a') as f:
        f.write('%s;%s\n'%(filename,ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S')))
        

def process_time_series(PathIN,PathOUT):
    PathOUT1=PathOUT
    
    fileprint='prints_ts.txt'
    NameRegistry='read_csv4d_ts.dat'
    NameRegistryGlobal='Header_Data.csv'
    NameRegistryLog='registry_ts.log'
    NameTS='name_registry_ts.log'
    
    while True:
        ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
        if len(ListFiles)>1:
            NameSorted=get_names_datacubes(ListFiles)
            NameSortedStart=[name for name in NameSorted]
            break
        time.sleep(3)
        
        
    NameSortedStart=[NameSortedStart[-1]]
    

    INFO=None
    while True: 
        name=NameSortedStart[0]
        PathOUT=PathOUT1+name+'/'
        PathOUT2=PathOUT+'temp_Time_Serie/'
        ntf.make_dir(PathOUT2)
        
        ListFiles=get_files_for_datacube(PathIN,name)
        NewListFiles=ntf.get_new_files_with_data(ListFiles,PathOUT2+NameRegistry)
        
        if len(NewListFiles)>0 and os.path.exists(PathOUT+NameRegistryGlobal):
            
            NewListFilesSorted=ntf.sorted_files_list_for_date(NewListFiles)
            
            if not ntf.exist_file_with_ext(PathOUT2,'.npz'):
                f = open(PathOUT2+fileprint, 'a')
                sys.stdout = f
                INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
                init_time_serie(PathOUT,PathOUT2,NameRegistry,NameRegistryLog,INFO,NameTS,NewListFilesSorted)
                
                f.close()
            else:
                if INFO is None:
                    INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
                
                if name!=NameSortedStart[-1]:
                    NameSortedStart.pop(0)
                else:
                    ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
                    NameSortedAux=get_names_datacubes(ListFiles)
                    for n in NameSortedAux:
                        if n not in NameSorted:
                            NameSortedStart.append(n)
                    
                for fileCSV4D in NewListFilesSorted[:-1]:
                    
                    f = open(PathOUT2+fileprint, 'a')
                    sys.stdout = f
                    
                    # if NewListFilesSorted[-1]==fileCSV4D:
                    #     ntf.check_read_file(fileCSV4D)
                    try:
                        path_npz=update_times_series(fileCSV4D,PathOUT2,INFO,NameTS)
                        ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
                        ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
                        
                        
                    except:
                        pass
                    
                    # Z=ntu.set_values_Z(INFO,fileCSV4D,Z)
                    
                    # process_evaluation(PathOUT,PathModel,NameModel,path_npz,INFO,DataColors_list,X,Y,Z,PM)
                    
                    f.close()
                    
     
        time.sleep(1)


def info_radares():
    path_excel="./info_radares.xlsx"
    radar_list=[ntf.get_info_radares(path_excel,radar) for radar in range(1,8)]
    return radar_list

def info_alertas_seguridad(DateMin):
    path_excel="./alertas_seguridad.xlsx"
    alerts_list=ntf.get_info_security_alerts(path_excel,DateMin)
    return alerts_list
    

def process_evaluation(PathOUT,PathModel,NameModel,path_npz,INFO,DataColors_list,X,Y,Z,PM):
    
    PathOUT2=PathOUT+'Results/'+NameModel+'/'
    PathImg=PathOUT2+'Images/'
    PathPM=PathOUT2+'Prob_Matrix/'
    PathAC=PathOUT2+'Adj_Cells/'
    ntf.make_dir(PathImg)
    ntf.make_dir(PathPM)
    ntf.make_dir(PathAC)
    
    t1=time.time()
    XY_eval,TS_eval,TW,indexs_dict=get_time_series_eval(path_npz)
    # TS_eval[:,-1]=TS_eval[:,-2]
    print('shape TS_eval: ',TS_eval.shape)
    
    SaveName=ntu.datetime_to_str(TW[-1]).replace('-','').replace(':','_').replace(' ','_')
    #Se obtiene las caracteristicas de las celdas del radar
    ProyectName,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    
    #Se carga el modelo 
    model = load_model(PathModel+NameModel+'.h5')
    
    
    
   
    print('Tiempo en obtener ts para eval: ', time.time()-t1)
    t1=time.time()
    results=model.predict(TS_eval)
    print('tiempo en predecir la cantidad de %d ts: %.1f '%(len(TS_eval),time.time()-t1))
    Prob=results[:,1]
    
    t1=time.time()
    IJ=np.array([ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size) for x,y in XY_eval],dtype=np.int32)
    PM[:,:]=0
    PM[IJ[:,0],IJ[:,1]]=Prob
    
    np.save(PathPM+SaveName,PM)
    do_graphs(X,Y,Z,PM,Prob,XY,TS,TW,DataColors_list,INFO)
    
def do_graphs(X,Y,Z,PM,Prob,XY,TS,TW,DataColors_list,INFO,PathImg,SaveName,NameModel,radar_list):
    ProyectName,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    IJ=np.array([ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size) for x,y in XY],dtype=np.int32)
    ################################graficas###################################
    
    #Caracteristicas de colores
    norm,cmap,cmap1,CellsColors,interval=DataColors_list
    IndexColor=norm(Prob)
    

    CellsColors[:,:]=[1.,1.,1.,1.]
    
    ii=ProyectName.upper().find('IBIS')
    NameRadar=ProyectName[ii:ii+6].replace('_','').replace('-','').replace(' ','')
    
    # azimut,elev=
    FechaHora=ntu.datetime_to_str(TW[-1])
    title='Radar '+NameRadar+'\n'+'Fecha: '+FechaHora+'\n'+NameModel
    
    #Evaluar solo una zona con time serie
    
        
    # fig,ax2D,ax3D=ntg.create_graph_2D_3D(title)
    fig2D, ax2D=ntg.create_graph_2D(title)
    fig3D, ax3D=ntg.create_graph_3D(title)
    t1=time.time()
    for index,ij in zip(IndexColor,IJ):
        i,j=ij[0],ij[1]
        CellsColors[i,j]=cmap1[index]
    
    print('tiempo en asignar valores a MP, CellsColor : ', time.time()-t1)
        
    t1=time.time()
    ntg.init_point_graph_2D(ax2D,X,Y,Z)
    ntg.Graph_Point2D(fig2D,ax2D,XY,Prob,cmap,norm,interval)
    
    
    radar_dic={name:(azi,elev) for _,_,_,name,azi,elev in radar_list}
    azi,elev=radar_dic[NameRadar]
    for x,y,z,texto,_,_ in radar_list:
        ntg.insert_point_graph_2D(ax2D,x,y,r=5,text=texto)
    print('Tiempo en graficar 2D: ',time.time()-t1)
        
    t1=time.time()
    ntg.Graph_Point3D(fig3D,ax3D,X,Y,Z,CellsColors,norm,cmap,cmap1,azi,elev)
    print('Tiempo en graficar 3D: ',time.time()-t1)
        
    t1=time.time()
    clusters=tc.do_cluster(PM,umbral=0.9,len_min_cluster=10)
    print('tiempo en obtener los clusters : ', time.time()-t1)
    
    t1=time.time()
    radio=30
    Ncells=int(np.round(radio/cell_size))+1
    fig_list=[]
    
    
    
    if len(clusters)>0:
        pp = PdfPages(PathImg+SaveName+'.pdf')
        for num,ij in enumerate(clusters):
            print('############## cluster: ',num,' ####################')
            i,j=ij[0],ij[1]
            x,y=ntu.get_xy_from_ij(i,j,x_min,y_min,cell_size)
            
            ntg.insert_point_graph_2D(ax2D,x,y,text=str(num))
            z=Z[ij[0],ij[1]]
            if z is np.nan:
                z=np.nanmean(Z[ij[0]-3:ij[0]+3,ij[1]-3:ij[1]+3])
            ntg.insert_point_graph_3D(ax3D,x,y,z,text=str(num))
            
            fig,axs1,ax2D1,ax3D1=ntg.do_graph_TS_2D_3D(num,x,y,z,i,j,radio,Ncells,title,Prob,TW,TS,XY,X,Y,Z,IJ,
                                      IndexColor,CellsColors,cmap1,cmap,norm,interval,azi,elev)
            
            fig.savefig(pp, format='pdf')
            fig.clf()
            ax2D1.clear(),ax3D1.clear()
            plt.close(fig)
            del fig,axs1,ax2D1,ax3D1
            gc.collect()
            # fig_list.append(fig)
            
        pp.close()
        
        xyzg_list=ntu.get_xyzg_from_cluster(clusters,Z,x_min,y_min,cell_size)
        ntf.create_csv_for_ibis(PathImg,SaveName,xyzg_list)

    
    # plt.tight_layout()
    
    t1=time.time()
    fig2D.savefig(PathImg+'2D_'+SaveName+'.png',dpi=400)
    print('Tiempo en guardar 2D: ',time.time()-t1)
    t1=time.time()
    fig3D.savefig(PathImg+'3D_'+SaveName+'.png',dpi=400)
    print('Tiempo en guardar 3D: ',time.time()-t1)
    # fig.canvas.mpl_connect('button_press_event', OnSelect)
    fig2D.clf(),fig3D.clf()
    ax2D.clear(),ax3D.clear()
    plt.close(fig2D)
    plt.close(fig3D)
    del fig2D,fig3D,ax2D, ax3D
    gc.collect()

    
    
def file_is_registry(path_file,PathNameRegistry):
    OnlyFileName, _ = os.path.splitext(os.path.basename(path_file))
    NewListFile=[]
    if os.path.exists(PathNameRegistry):
        with open(PathNameRegistry,'r') as f:
            ListSaved=f.readlines()
            DicListSave={ls.replace('\n',''):0 for ls in ListSaved}
            if OnlyFileName not in DicListSave:
                return True
            else:
                return False

def create_title(ProyectName,Fecha,NameModel):
    
    ii=ProyectName.upper().find('IBIS')
    NameRadar=ProyectName[ii:ii+6].replace('_','').replace('-','').replace(' ','')
    
    FechaHora=ntu.datetime_to_str(Fecha)
    title='Radar '+NameRadar+'\n'+'Fecha: '+FechaHora+'\n'+NameModel
    
    return title,NameRadar

def get_setting(pathFile):
    with open(pathFile,'r') as f:
        setting=[line.split('=')[1].replace('\n','') for line in f.readlines()]
        
    setting[0]=eval(setting[0])
    setting[1]=datetime.strptime(setting[1],'%Y-%m-%d %H:%M')
    setting[2]=timedelta(minutes=eval(setting[2]))
    setting[3]=eval(setting[3])
    setting[4]=eval(setting[4])
    setting[5]=eval(setting[5])
    setting[6]=eval(setting[6])
    setting[7]=eval(setting[7])
    setting[8]=eval(setting[8])
    setting[9]=eval(setting[9])
    setting[10]=eval(setting[10])
    setting[11]=eval(setting[11])
    setting[12]=eval(setting[12])

    return setting
    

    
    
def process_evaluation2(PathIN,PathOUT,PathModel,NameModel):
    # path_npz,INFO,DataColors_list,X,Y,Z,PM
    NameRegistry='read_npz.dat'
    NameRegistryGlobal='Header_Data.csv'
    NameRegistryLog='registry_npz.log'
    fileprint='print_results.txt'
    PathOUT1=PathOUT
    
    while True:
        ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
        if len(ListFiles)>1:
            NameSorted=get_names_datacubes(ListFiles)
            NameSortedStart=[name for name in NameSorted]
            break
        time.sleep(3)
    
    INFO = None
    X = None
    Y,Z,PM,VM,CellsColors=None,None,None,None,None
    
    NameSortedStart=[NameSortedStart[-1]]
    
    #Se carga el modelo 
    model = load_model(PathModel+NameModel+'.h5')
    while True:
        name=NameSortedStart[0]
        PathOUT=PathOUT1+name+'/'
        PathOUT2=PathOUT+'Results/'+NameModel+'/'
        PathImg=PathOUT2+'Images/'
        PathPM=PathOUT2+'Prob_Matrix/'
        PathAC=PathOUT2+'Adj_Cells/'
        Path_TS=PathOUT+'temp_Time_Serie/'
        
        ntf.make_dir(PathImg)
        ntf.make_dir(PathPM)
        ntf.make_dir(PathAC)
        
        
        if os.path.exists(PathOUT+NameRegistryGlobal):
           
            if INFO is None:
                # f = open(PathOUT2+fileprint, 'a')
                # sys.stdout = f
                INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
                DataColors_list=ntg.create_colors_map(INFO)
                ntf.create_file(PathOUT2+NameRegistry)
                ntf.create_file(PathOUT2+NameRegistryLog)
                
                # f.close()
                
                
            if ntf.exist_file_with_ext(Path_TS,'.npz') and X is not None:
                path_npz_list=ntf.get_all_files(Path_TS,ext='.npz')
                if len(path_npz_list)>0:
                    path_npz=path_npz_list[0]
                    # ntf.check_read_file(path_npz)
                    if file_is_registry(path_npz,PathOUT2+NameRegistry):
                        t1=time.time()
                        
                        try:
                            XY,TS,TW,indexs_dict=get_time_series_eval(path_npz)
                        except:
                            print('Error en extraer datos time series')
                            TS=[]
                            pass
                        
                        
                        if len(TS)>0:
                            radar_list=info_radares()
                            
                            
                            # f = open(PathOUT2+fileprint, 'a')
                            # sys.stdout = f
                            print('Tiempo en obtener ts para eval: ', time.time()-t1)
                            ListFiles=get_files_for_datacube(PathIN,name)
                            _,EndDate=ntf.get_dates_from_pathnpz(path_npz)
                                    
                            fileCSV4D=ntf.get_csv4d_for_Z(ListFiles,EndDate)
                                    
                            Z=ntu.set_values_Z(INFO,fileCSV4D,Z)
                                
                            # TS_eval[:,-1]=TS_eval[:,-2]
                            print('shape TS_eval: ',TS.shape)
                                    
                            SaveName=ntu.datetime_to_str(TW[-1]).replace('-','').replace(':','_').replace(' ','_')
                            #Se obtiene las caracteristicas de las celdas del radar
                            ProyectName,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
                            title,NameRadar=create_title(ProyectName,TW[-1],NameModel)
                            x_min,_,y_min,_=lim_axes
                            
                            
                            #parametros de diseños
                            setting=get_setting('Setting_'+NameRadar+'_'+NameModel+'.txt')
                            setAlart=setting[0]
                            FromAlert=setting[1]
                            DeltaTime=setting[2]
                            umbrals_velocity_list=setting[3]
                            level_umbral=setting[4]
                            umbrals_probability_list=setting[5]
                            colors_list=setting[6]
                            umbral_probability=setting[7]
                            len_min_cluster=setting[8]
                            radio=setting[9]
                            setRegion=setting[10]
                            centroXYZ=setting[11]
                            RadioRegion=setting[12]
                            print(centroXYZ[0],centroXYZ[1],centroXYZ[2])
                            if setAlart:
                                alerts_list=info_alertas_seguridad(FromAlert)
                            
                           
                                    
                            
                            # DeltaTime=timedelta(minutes=30) # tiempo para velocidad media
                            # umbrals_velocity_list=[0,3,6,10,np.inf]
                            # level_umbral=3
                            
                            
                            # umbrals_probability_list=[0,0.5,0.7,0.9,1.001]
                            # colors_list=['limegreen','yellow','orange', 'red']
                            
                            # umbral_probability=0.9 # umbral para probabilidad 
                            # len_min_cluster=4 # numero minimo de celdas contiguas
                            # radio=30 # radio para agrupar conjunto de celdas contiguas
                            
                            
                            
                            cmap,cmap1,norm=ntg.get_colors_map(umbrals_probability_list,colors_list)
                            DataColors_list=[norm,cmap,cmap1,umbrals_probability_list]
                                
                            t1=time.time()
                            #calculo de las probabilidades por medio del modelo RNN
                            results=model.predict(TS)
                            print('tiempo en predecir la cantidad de %d ts: %.1f '%(len(TS),time.time()-t1))
                            Prob=results[:,1]
                                    
                            t1=time.time()
                            IJ=np.array([ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size) for x,y in XY],dtype=np.int32)
                            PM[:,:]=0
                            PM[IJ[:,0],IJ[:,1]]=Prob
                            print('tiempo en asignar probabilidad a la matriz: %.1f '%(time.time()-t1))
                            
                            #Calculo de las velocidades medias moviles para cada celda
                            
                            t1=time.time()
                            
                            VM[:,:]=0
                            velocity_mm=tv.get_velocity_media_movil(TW,TS,DeltaTime)
                            VM[IJ[:,0],IJ[:,1]]=np.abs(velocity_mm)
                            
                            # mpl.use('Qt5Agg')
                            # plt.plot(range(len(velocity_mm)),velocity_mm )
                            
                            # plt.pause(600)
                                
                            print('tiempo calcular velocidad media movil : %.1f '%(time.time()-t1))
                            
                            
                            np.save(PathPM+SaveName,PM)
                            t1=time.time()
                            # if NameRadar=='IBIS3':
                            #     umbral_probability=0.9
                            #     if NameModel=='MMLP_Media_EV_6-7-8-9-10-13-16_Nobalanced':
                            #         len_min_cluster=5
                                
                            #     if NameModel=='MMLP_Media_EV_1-2-3_Nobalanced':
                            #         len_min_cluster=10
                            #     if NameModel=='MMLP_Inflexion_EV_1-2-3_Nobalanced':
                            #         len_min_cluster=100
                                        
                            print('len_min_cluster: ',len_min_cluster)
                            
                            
                            
                            
                            if setRegion:
                                # centroXYZ=(58395.20,91173.05)
                                #Algortimo encuentra conjunto de celdas contiguas con CR
                                clusters3=tc.do_cluster(PM,umbral=umbral_probability,len_min_cluster=len_min_cluster)
                                #Algortimo que agrupa conjunto de celdas contiguas con CR
                                clusters2={}
                                for i,j in clusters3:
                                    if np.sqrt((X[i,j]-centroXYZ[0])**2 + (Y[i,j]-centroXYZ[1])**2)<=RadioRegion:
                                        clusters2[(i,j)]=clusters3[(i,j)]
                            else:
                                clusters2=tc.do_cluster(PM,umbral=umbral_probability,len_min_cluster=len_min_cluster)
                                
                            
                            clusters=tc.join_clusters(clusters2,cell_size,r=radio)
                            print('Numero de cluster inicial: ',len(clusters2))
                            print('Numero de cluster final  : ',len(clusters))
                            print('tiempo en obtener los clusters : ', time.time()-t1)
                            
                            t1=time.time()
                            ntg.set_cells_color(CellsColors,norm(Prob),IJ,cmap1)
                            umbral_velocity=umbrals_velocity_list[level_umbral-1]
                            IJ1=np.array([(i,j) for ij in clusters for i,j in clusters[ij][0] if VM[i,j]>=umbral_velocity],dtype=np.int32)
                            for i,j in IJ1:
                                if umbrals_velocity_list[level_umbral-1] <= VM[i,j] < umbrals_velocity_list[level_umbral]:
                                    CellsColors[i,j]=list(mpl.colors.to_rgba(c='cyan', alpha=1.0))
                                elif umbrals_velocity_list[level_umbral] <= VM[i,j] < umbrals_velocity_list[level_umbral+1]:
                                    CellsColors[i,j]=list(mpl.colors.to_rgba(c='blue', alpha=1.0))
                                else:
                                    pass
                            
                            # for i,j in IJ:
                            #     if PM[i,j]>=umbral_probability and VM[i,j]>=10:
                            #         CellsColors[i,j]=[0.0,0.0,1.0,1.0]
                                
                            print('tiempo en asignar color azul : ', time.time()-t1)
                            
                            
                            
                            ntg.do_graphs(clusters,clusters2,X,Y,Z,PM,Prob,XY,TS,TW,CellsColors,DataColors_list,INFO,PathImg,SaveName,title,radar_list,alerts_list,NameRadar)
                            # ntg.do_graphs(X,Y,Z,PM,Prob,XY,TS,TW,DataColors_list,INFO,PathImg,SaveName,NameModel,radar_list,alerts_list)
                            gc.collect()
                            del TS,XY,TW,IJ,ListFiles,Prob
                            ntf.update_registry(PathOUT2+NameRegistry,path_npz)
                            ntf.update_registry_log(PathOUT2+NameRegistryLog,path_npz)
                            if name!=NameSortedStart[-1]:
                                NameSortedStart.pop(0)
                            else:
                                ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
                                NameSortedAux=get_names_datacubes(ListFiles)
                                for n in NameSortedAux:
                                    if n not in NameSorted:
                                        NameSortedStart.append(n)
                            # f.close()
                else:
                    print('Error en leer npz')
            else:
                if ntf.exist_file_with_ext(Path_TS,'.npz') and X is None:
                    path_npz=ntf.get_all_files(Path_TS,ext='.npz')[0]
                    try:
                        # f = open(PathOUT2+fileprint, 'a')
                        # sys.stdout = f
                        t1=time.time()
                        X,Y,Z,PM,VM,CellsColors=ntu.create_matrixs_XYZ_PM(path_npz,INFO)
                        print('tiempo en crear matrices X,Y,Z y PM: ',time.time()-t1)
                        # f.close()
                    except:
                        pass
            
                
        else:
            INFO = None
    
    
        time.sleep(1)
                    
        
    
if __name__=='__main__':
    # csv4d='MASTER_MLP_24122019_IBIS-1_MLP_24122019_Displacement_Map_from_19_Dec_24-00_00_to_19_Dec_24-14_25.csv4d'
    if True:
        PathIN="C:/EMT/Data_CSV4D/IBIS_3/" 
        PathOUT="C:/EMT/Data_OUT/IBIS_3/"
        PathModel="C:/piloto/modelos/"
        # NameModel='MMLP_Media_EV_1-2-3_Nobalanced'
        NameModel='MMLP_Media_EV_6-7-8-9-10-13-16_Nobalanced'
        
        
        
        #NameModel='MMLP_Inflexion_EV_1-2-3_balanced'
        # NameModel='MMLP_Inflexion_EV_1-2-3_Nobalanced'
        # NameModel='MMLP_MaxAcel_EV_1-2-3_Nobalanced'
        opc=1
    # else:
    #     PathIN="F:/Data_CSV4D/Nueva Data_CSV4D/IBIS_2/"
    #     PathOUT="D:/DataOUT_Radar/IBIS_2/"
    #     PathModel="C:/Users/JOliva/Documents/modelos/Modelos por CD/MMLP_Media_EV_1-2-3_Nobalanced/"
    
    # NameModel="MMLP_Media_EV_1-2-3_Nobalanced"
    
   
    
    # data=placement_from_csv4d(PathIN+csv4d)
    # xy=np.array([strXY_to_floatXY(xy,5) for xy in list(data.keys())])
    # plt.plot(xy[:,0],xy[:,1],'go')
    
    if len(sys.argv)>1:
        opc=int(sys.argv[1])
        PathIN=sys.argv[2]
        PathOUT=sys.argv[3]
        if opc==4:
            PathModel=sys.argv[4]
            NameModel=sys.argv[5]
        
    ntf.make_dir(PathOUT)
    print(PathIN,PathOUT)
    
    # time.sleep(10)
    # test_graph()

    print('Escoger uno de los procesos a realizar:')
    print('[1] Generar cubo de datos')
    print('[2] Generar DTM')
    print('[3] Generar Time Serie Temporal')
    print('[4] Generar Evaluacion')
    print('opcion seleccionada: ',opc)
    if opc==1:
        print('opcion 1')
        process_data_cube(PathIN,PathOUT)
    elif opc==2:
        print('opcion 2')
        process_dtm(PathIN,PathOUT)
    elif opc==3:
        print('opcion 3')
        process_time_series(PathIN,PathOUT)
    elif opc==4:
        print('opcion 4')
        print('Nombre Modelo: ',NameModel)
        process_evaluation2(PathIN,PathOUT,PathModel,NameModel)
