# -*- coding: utf-8 -*-
import os
import sys
import time
from datetime import datetime

from data_struct import DataCube
import new_tool_files as ntf
import new_tool_utility as ntu


def set_time_series(fileCSV4D,TS,Cond,TW,INFO,EndDate):
    
    data=ntf.get_xyzd_from_csv4d(fileCSV4D,5)
    n,m=TS.shape
    TW[0:-1]=TW[1:]
    TW[-1]=EndDate
    
    _,_,_,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes

    
    TS[:,0:-1]=TS[:,1:]
    
    TS[:,-1]=np.nan
    desplazamiento={}
    
    dist=[]
    t1=time.time()
    for x,y,_,desp in data:
        i,j=ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size)
        index=j+i*n_cols
        TS[index,-1]=desp
        desplazamiento[index]=None
        # Cond[index]=len(np.argwhere(~np.isnan(TS[index,:])))
    
    # Se repite el valor anterior de aquellas celdas que no tienen datos
    indexs=np.where(np.isnan(TS[:,-1]))[0]
    TS[indexs,-1]=TS[indexs,-2]
    
    indexs=np.where(~np.isnan(TS[:,-1]))[0]
    Cond[indexs]=Cond[indexs]+1
    
    indexs=np.where(np.isnan(TS[:,-1]))[0]
    Cond[indexs]=Cond[indexs]-1
        
    cont=len(desplazamiento)
    Cond[Cond<0]=0
    Cond[Cond>m]=m
    
    # indexs=[i[0] for i in np.argwhere(Cond==m)]
    # if len(indexs)>0:
    #     for i in indexs:
    #         TS[i,:]=TS[i,:]-TS[i,0]
    # print('csv4d: ',EndDate,'celdas guardadas: ',cont,'de',len(data),'; dist max y min: ',dist_max,dist_min,'; celdas con 144 reg: ',len(np.argwhere(Cond==m)),';fecha:', datetime.now())
    print('csv4d: ',EndDate,'celdas guardadas: ',cont,'de',len(data),'; celdas con 144 reg: ',len(np.argwhere(Cond==m)),';fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
    return TS,Cond,TW




def update_times_series(fileCSV4D,PathOUT2,INFO,NameTS):
    
    StartDate1,EndDate1,_=ntf.get_info_from_csv4d(fileCSV4D)

    pahtfilename=ntf.get_all_files(PathOUT2,ext='.npz')
    if len(pahtfilename)==0:
        print('Error: No existe un arhivo .npz en ',PathOUT2,' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
    elif len(pahtfilename)>1:
        print('Error: Existen más de un arhivo .npz en ',PathOUT2,' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
    else:
        
        oldfilename,_ = os.path.splitext(os.path.basename(pahtfilename[0]))

        Cond,XY,TS,TW=ntu.get_data_times_series(PathOUT2+ oldfilename+'.npz')
        os.remove(PathOUT2+oldfilename+'.npz')
        
        TS, Cond, TW = set_time_series(fileCSV4D,TS,Cond,TW,INFO,EndDate1)
        
        pos=[i for i in range(len(TW)) if TW[i] is not None]
        if len(pos)>0:

            StartDate=ntu.datetime_to_str(TW[pos[0]],fmt='%Y_%b_%d-%H_%M')
        else:
            StartDate=ntu.datetime_to_str(TW[0],fmt='%Y_%b_%d-%H_%M')
            
        EndDate=ntu.datetime_to_str(TW[-1],fmt='%Y_%b_%d-%H_%M')
        newfilename=StartDate+'_to_'+EndDate
        
       
        np.savez(PathOUT2+newfilename+'.npz',Cond,XY,TS,TW)
        save_name_timeserie(PathOUT2+NameTS,newfilename)
        
        return PathOUT2+newfilename+'.npz'


def init_time_serie(PathOUT,PathOUT2,NameRegistry,NameRegistryLog,INFO,NameTS,NewListFilesSorted):
    t1=time.time()
    _,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    
    
    XY=ntu.create_list_XY(x_min,n_cols,y_min,n_rows,cell_size,5)
    Cond=np.array(np.zeros(len(XY),dtype=np.int32))
    TS=np.empty((len(XY),144), dtype=np.float32)
    TS[:,:]=np.nan
    TW=np.empty(144, dtype=datetime)
    
    
    
                
    #los arreglos almcanados son el siguiente orden : Condicion, 
    # coordendas, time series y ventana temporal, cuyos accesos son mediante
    # 'arr_0','arr_1','arr_2','arr_3'
    
    print('Tiempo en inicializar y guardar npz:',time.time()-t1,' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
    with open(PathOUT2+NameRegistry,'w') as f:
        print('Se inicializa el registro de csv4d para la TS',' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
        pass
                    
    with open(PathOUT2+NameRegistryLog,'w') as f:
        print('Se inicializa el registro log de csv4d para la TS',' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
        pass    
    
    with open(PathOUT2+NameTS,'w') as f:
        print('Se inicializa el registro log de nombres para la TS',' fecha:', ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S'))
        pass
    
    if len(NewListFilesSorted)>144:
       NewList=NewListFilesSorted[-145:-1]
    else:
       NewList=NewListFilesSorted[:-1]
    
    for fileCSV4D in NewList:
        
        EndDate=ntf.get_info_from_csv4d(fileCSV4D)[1]
        TS, Cond, TW = set_time_series(fileCSV4D,TS,Cond,TW,INFO,EndDate)
        
        ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
        ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
        
        
    StartDate=ntf.get_info_from_csv4d(NewList[0])[1]
    EndDate=ntf.get_info_from_csv4d(NewList[-1])[1]
    
    StartDate=ntu.datetime_to_str(StartDate,fmt='%Y_%b_%d-%H_%M')
    EndDate=ntu.datetime_to_str(EndDate,fmt='%Y_%b_%d-%H_%M')
    
    newfilename=StartDate+'_to_'+EndDate
    np.savez(PathOUT2+newfilename+'.npz',Cond,XY,TS,TW)
    save_name_timeserie(PathOUT2+NameTS,newfilename)

def save_name_timeserie(NameTS,filename):
    with open(NameTS,'a') as f:
        f.write('%s;%s\n'%(filename,ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S')))

def process_time_series(PathIN,PathOUT):
    PathOUT1=PathOUT
    
    fileprint='prints_ts.txt'
    NameRegistry='read_csv4d_ts.dat'
    NameRegistryGlobal='Header_Data.csv'
    NameRegistryLog='registry_ts.log'
    NameTS='name_registry_ts.log'
    
    while True:
        ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
        if len(ListFiles)>1:
            NameSorted=ntf.get_names_datacubes(ListFiles)
            NameSortedStart=[name for name in NameSorted]
            break
        time.sleep(3)
        
        
    NameSortedStart=[NameSortedStart[-1]]
    

    INFO=None
    while True: 
        name=NameSortedStart[0]
        PathOUT=PathOUT1+name+'/'
        PathOUT2=PathOUT+'temp_Time_Serie/'
        ntf.make_dir(PathOUT2)
        
        ListFiles=ntf.get_files_for_datacube(PathIN,name)
        NewListFiles=ntf.get_new_files_with_data(ListFiles,PathOUT2+NameRegistry)
        
        if len(NewListFiles)>0 and os.path.exists(PathOUT+NameRegistryGlobal):
            
            NewListFilesSorted=ntf.sorted_files_list_for_date(NewListFiles)
            
            if not ntf.exist_file_with_ext(PathOUT2,'.npz'):
                f = open(PathOUT2+fileprint, 'a')
                sys.stdout = f
                INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
                init_time_serie(PathOUT,PathOUT2,NameRegistry,NameRegistryLog,INFO,NameTS,NewListFilesSorted)
                
                f.close()
            else:
                if INFO is None:
                    INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
                
                if name!=NameSortedStart[-1]:
                    NameSortedStart.pop(0)
                else:
                    ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
                    NameSortedAux=ntf.get_names_datacubes(ListFiles)
                    for n in NameSortedAux:
                        if n not in NameSorted:
                            NameSortedStart.append(n)
                    
                for fileCSV4D in NewListFilesSorted[:-1]:
                    
                    f = open(PathOUT2+fileprint, 'a')
                    sys.stdout = f
                    
                    # if NewListFilesSorted[-1]==fileCSV4D:
                    #     ntf.check_read_file(fileCSV4D)
                    try:
                        path_npz=update_times_series(fileCSV4D,PathOUT2,INFO,NameTS)
                        ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
                        ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
                        
                        
                    except:
                        pass
                    
                    # Z=ntu.set_values_Z(INFO,fileCSV4D,Z)
                    
                    # process_evaluation(PathOUT,PathModel,NameModel,path_npz,INFO,DataColors_list,X,Y,Z,PM)
                    
                    f.close()
                    
     
        time.sleep(1)
