# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 10:29:47 2020

@author: joliva
"""

import numpy as np
import matplotlib.pylab as plt
import time
import numba
# @numba.njit
def cal(TS):
    
    return list(map(lambda x:len(np.argwhere(~np.isnan(x))) , TS))
    

def strXY_to_floatXY(StrCoord,k):
    xy=list(map(float,StrCoord.split('-')))
    # xc=np.trunc(xy[0]*10**k)/10**k
    # yc=np.trunc(xy[1]*10**k)/10**k
    xc=np.round(xy[0],k)
    yc=np.round(xy[1],k)
    return xc,yc


# PathFile='C:/Users/joliva/Documents/puntos_errantesIBIS2.txt'
# XY1=[]
# XY2=[]
# XY3=[]
# with open(PathFile,'r') as f:
#     for line in f:
#         data=line.split(' ')
#         xy1=data[1]
#         xy2=data[2]
#         xy3=data[5]
        
#         XY1.append(strXY_to_floatXY(xy1,5))
#         XY2.append(strXY_to_floatXY(xy2,5))
#         XY3.append(strXY_to_floatXY(xy3,5))
        
# XY1=np.array(XY1)
# XY2=np.array(XY2)
# XY3=np.array(XY3)

# plt.plot(XY1[:,0],XY1[:,1],'or')
# plt.plot(XY2[:,0],XY2[:,1],'og')
# plt.plot(XY3[:,0],XY3[:,1],'ob')


# t1=time.time()
# n=1500000
# TS=np.empty((n,144), dtype=np.float32)
# TS[:,:]=np.nan
# len_nan=lambda x: len(np.argwhere(~np.isnan(x)))
# cont=np.array([len_nan(ts) for ts in TS ],dtype=np.int32)
# print('tiempo 1: ',time.time()-t1, len(cont),np.max(cont),np.min(cont))

# t1=time.time()
# cont=cal(TS)
# print('tiempo 2: ',time.time()-t1, len(cont),np.max(cont),np.min(cont))
# # print(cont)



# PathIN="C:/EMT/Data_CSV4D/Nueva Data_CSV4D/IBIS_2/"
# csv4d='MASTER_MLP_24122019_IBIS-2_MLP_24122019_Displacement_Map_from_19_Dec_24-00_00_to_20_Jan_07-02_31.csv4d'
# XYZ=get_xyz_from_csv4d(PathIN+csv4d)
# X,Y,Z=XYZ[:,0],XYZ[:,1],XYZ[:,2]


# import numpy as np
# from mayavi import mlab

# X, Y = np.meshgrid(, np.arange(128))
# Z = np.zeros_like(X)
# im = np.sin(X/10 + Y/100)

# #fig = plt.figure()
# #x = fig.add_subplot(111, projection='3d')

# src = mlab.pipeline.array2d_source(im)
# warp = mlab.pipeline.warp_scalar(src)
# normals = mlab.pipeline.poly_data_normals(warp)
# surf = mlab.pipeline.surface(normals)
# mlab.show()
    



import matplotlib.pyplot as plt

fg = plt.figure(1)
fg.clf()
renderer = fg.canvas.get_renderer()
ax = fg.add_subplot(1,1,1)

# text_object=ax.annotate('Norte', xy=(1.1, 0.8),xytext=(1.1, 1))

# text_window_extent = text_object.get_window_extent(renderer)
# new_x_position = 1.1 - text_window_extent.width / 2
# text_object.set_position((new_x_position, 1))

ax.annotate('N', xytext=(1.05, 1),xy=(1.05, 0.8),ha= 'center', xycoords='axes fraction', 
            arrowprops=dict(arrowstyle="<-", color='k',shrinkB=10,lw=2))


# ax.text(1.1, 1,'N')
x=np.arange(0,10,1)
y=np.exp(x)
ax.plot(x,y)
ax.grid(True)
fg.canvas.draw()
    
    
    
    
    
    

    
# def test_graph():
    
#     import matplotlib.pylab as plt
#     import matplotlib as mpl
#     from mpl_toolkits.mplot3d import Axes3D 
    
#     PathIN="C:/EMT/Data_CSV4D/Nueva Data_CSV4D/IBIS_2/"
#     csv4d='MASTER_MLP_24122019_IBIS-2_MLP_24122019_Displacement_Map_from_19_Dec_24-00_00_to_20_Jan_07-02_31.csv4d'
#     t1=time.time()
#     XYZ=ntf.get_xyz_from_csv4d(PathIN+csv4d)
#     print('Tiempo en extraer datos csv4d: ', time.time()-t1)
    
#     t1=time.time()
#     data=ntf.get_xyzd_from_csv4d(PathIN+csv4d,5)
#     print('Tiempo en extraer desplazamiento csv4d: ', time.time()-t1)
    
#     t1=time.time()
#     data=ntf.get_xyzd_from_csv4d(PathIN+csv4d,5)
#     print('Tiempo en extraer desplazamiento 2 csv4d: ', time.time()-t1)
#     # fig= plt.figure(1,figsize=(14,8))
#     # ax = fig.gca(projection='3d')
    
#     _,info=create_all_cells(PathIN+csv4d,5,NumCell=1)
#     m,n,x_min,x_max,y_min,y_max,cell_size,dist_x,dist_y=info
    
#     X=np.empty((n,m))
#     Y=np.empty((n,m))
#     Z=np.empty((n,m))
#     X[:,:]=np.nan
#     Y[:,:]=np.nan
#     Z[:,:]=np.nan
    
#     t1=time.time()
#     IJ=np.array([ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size) for x,y,z in XYZ],dtype=np.int32)
#     print('Tiempo en crear ij  : ', time.time()-t1)
#     t1=time.time()
#     X[IJ[:,0],IJ[:,1]]=XYZ[:,0]
#     Y[IJ[:,0],IJ[:,1]]=XYZ[:,1]
#     Z[IJ[:,0],IJ[:,1]]=XYZ[:,2]
#     # for ij,xyz in zip(IJ,XYZ):
#     #     X[ij[0],ij[1]]=xyz[0]
#     #     Y[ij[0],ij[1]]=xyz[1]
#     #     Z[ij[0],ij[1]]=xyz[2]
#     print('Tiempo en asignar valores a X,Y y Z : ', time.time()-t1)
#     return 0
#     cmap = mpl.colors.ListedColormap(['limegreen','red'])
    
#     # ax.plot_surface(X, Y, Z,cmap=cmap ,cstride=1,rstride=1,linewidth=0.2, antialiased=True,shade=True)
#     # X,Y,Z=XYZ[:,0],XYZ[:,1],XYZ[:,2]
#     prob=np.random.rand(len(X))
    
    
#     interval=[0.0,0.5,0.7,0.9,1.00]
#     n_pal=len(interval)-1
#     cmap = mpl.colors.ListedColormap(['limegreen','yellow','orange', 'red'])
#     cmap1 = cmap(np.linspace(0,1,n_pal))
#     norm = mpl.colors.BoundaryNorm(interval, cmap.N)
#     colores=[cmap1[norm(p)] for p in prob]
#     t1=time.time()
#     # ax.plot_trisurf(X, Y, Z, cmap=mpl.colors.ListedColormap(colores),linewidth=0.2, antialiased=True)
#     print('tiempo en graficar trisurf: ',time.time()-t1)
#     t1=time.time()
#     # fig.savefig('3DD.png',dpi=500)
#     print('tiempo en guardar trisurf: ',time.time()-t1)
#     # ax.plot_trisurf(X, Y, Z, linewidth=0.2)
    
#     # fig2, ax = plt.subplots(figsize=(20,14))
#     # ax.grid(linewidth=1)
#     # ax.set_axisbelow(True)
#     # ax.scatter(X,Y,marker='s',c='limegreen',s=0.25,linewidths=0.25,alpha=0.8)
#     # theta=np.linspace(0,2*np.pi,1000)
#     # r1=30
#     # r2=60
#     # r3=100
#     # ax.plot(X[11500] + r1*np.cos(theta), Y[11500] +  r1*np.sin(theta),'--k',alpha=0.8)
#     # ax.plot(X[11500] + r2*np.cos(theta), Y[11500] +  r2*np.sin(theta),'--k',alpha=0.8)
#     # ax.plot(X[11500] + r3*np.cos(theta), Y[11500] +  r3*np.sin(theta),'--k',alpha=0.8)
#     # ax.set_aspect('equal') 
    
#     # # plt.plot(X,Y,'ob',ms=0.01)
#     # fig2.savefig('2D.png',dpi=1500)
#     # print('tiempo en generara y guardar imagenes 2D y 3D: ',time.time()-t1)
    
    
    
#     # _,info=create_all_cells(PathIN+csv4d,5,NumCell=1)
#     # m,n,x_min,x_max,y_min,y_max,cell_size,dist_x,dist_y=info
    
#     # X=np.empty((n,m))
#     # Y=np.empty((n,m))
#     # Z=np.empty((n,m))
#     # X[:,:]=np.nan
#     # Y[:,:]=np.nan
#     # Z[:,:]=np.nan
    
#     # for x,y,z in XYZ:
#     #     i,j=get_ij_from_xy(x,y,x_min,y_min,cell_size)
#     #     X[i,j]=x
#     #     Y[i,j]=y
#     #     Z[i,j]=z
            
    
#     from mayavi import mlab
    
#     # X, Y = np.meshgrid( , )
#     # Z = np.zeros_like(X)
#     # im = np.sin(X/10 + Y/100)
    
#     #fig = plt.figure()
#     #x = fig.add_subplot(111, projection='3d')
    
#     # src = mlab.pipeline.array2d_source(X,Y,Z)
#     # warp = mlab.pipeline.warp_scalar(src)
#     # normals = mlab.pipeline.poly_data_normals(warp)
#     # surf = mlab.pipeline.surface(normals)
#     # mlab.show()
    
    

#     # x, y = np.mgrid[-7.:7.05:0.1, -5.:5.05:0.05]
#     n,m=Z.shape
#     prob=np.random.rand(n,m)
#     cellcolors=np.array([])
#     colores=[cmap1[norm(prob[i,j])] for j in range(m) for i in range(n)]
#     s = mlab.surf(Z,colormap=cmap(norm(prob)))
#     #cs = contour_surf(x, y, f, contour_z=0)
    
# def f(x, y):
#         sin, cos = np.sin, np.cos
#         return sin(x + y) + sin(2 * x - y) + cos(3 * x + 4 * y)



# def test_h5():
#      Path=r"C:\Users\JOliva\Documents\DataOUT\IBIS_2\MASTER_MLP_24122019_IBIS-2_MLP_24122019\Data Cube\MASTER_MLP_24122019_IBIS-2_MLP_24122019.h5"
#      t1=time.time()
#      L=get_XY_from_H5(Path)
#      print('Tiempo en obtener XY de h5: ',time.time()-t1)
#      print(L)
     

    # time.sleep(15)
    
#    process_time_series(PathIN,PathOUT)
    # process_dtm(PathIN,PathOUT)
    # process_evaluation(PathIN,PathOUT)
#    test_read()
#    test_Pto_cercano()
#    test_read_write_file()
#    np.random.seed(0)
#    n=5000
#    X=np.linspace(0,1,n)
#    Y=np.linspace(0,1,n)
        
    
    # PathOUT='C:/Users/joliva/Documents/DataOUT/IBIS_4/MASTER_MLP_24122019_IBIS-2_MLP_24122019/'  
    # PathOUT1=PathOUT+'Data Cube/'
    # # PathOUT2='D:/DataOUT_Radar/IBIS_1/Data DTM/'
    
    # NameRegistryGlobal='Header_Data.csv'
    
    # INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
    # Name,_,_,n_cols,lim_axes,cell_size,_,_=INFO
    # x_min,_,y_min,_=lim_axes
    
    # fileCD=Name
    # # fileDTM=Name+'_DTM'
    # XY_CD=get_XY_from_H5(PathOUT1+fileCD+'.h5')
    # with open('C:/Users/joliva/Documents/ptos_xy.txt','w') as f:
    #     f.write('X;Y\n')
    #     for xy in XY_CD:
    #         x,y=strXY_to_floatXY(xy,5)
    #         f.write('%s;%s\n'%(x,y))
    # XY_DTM=get_XY_from_H5(PathOUT2+fileDTM+'.h5')
    
    # cont=0
    # for xy1,xy2 in zip(XY_CD,XY_DTM):
    #     if xy1==xy2:
    #         cont=cont+1
    
    
    # for i,xy in enumerate(XY):
    #     index=get_pos_for_XY(xy,x_min,y_min,cell_size,n_cols,5)
    #     if  abs(i-index)>0:
    #         print(xy,i,index,abs(i-index))
    #         # break
        
#    XY=np.array(['%.5f-%.5f'%(x,y) for x in X for y in Y],dtype=np.str)
##    print(XY[:10])
##    dic_XY={xy: 0 for xy in XY}
##    for i,xy in enumerate(dic_XY):
##        print(xy)
##        if i>10: break
#    
#    np.random.seed(0)
#    v= '%.5f-%.5f'%(np.random.rand(),np.random.rand())
#    v='0.00000-0.00917'
#    t1=time.time()
#    if v in XY:
#        print('EL valor esta dentro del arreglo')
#    else:
#        print('El valor NO esta dentro del arreglo')
#    print('El tiempo de buscar es: ',time.time()-t1)
#    
#    
#    t1=time.time()
#    if v in dic_XY:
#        print('EL valor esta dentro del arreglo')
#    else:
#        print('El valor NO esta dentro del arreglo')
#    print('El tiempo de buscar es: ',time.time()-t1)