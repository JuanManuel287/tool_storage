# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 14:20:02 2020

@author: joliva
"""
import new_tool_files as ntf

path=r"C:\EMT\Data_CSV4D\IBIS_2"
fileCSV4D1=r"\MASTER_MLP_18012020_IBIS-2_MLP_24122019_Displacement_Map_from_19_Dec_24-00_00_to_20_Jan_28-13_45.csv4d"
fileCSV4D2=r"\MASTER_MLP_18012020_IBIS-2_MLP_24122019_Displacement_Map_from_19_Dec_24-00_00_to_20_Jan_28-14_29.csv4d"
Data1=ntf.get_IDxyzd_from_csv4d(path+fileCSV4D1,5)
Data2=ntf.get_IDxyzd_from_csv4d(path+fileCSV4D2,5)

dic1={}
for idd,_,_,_,d in Data1:
    idd=int(idd)
    dic1[idd]=d

dif_list=[]
for idd,_,_,_,d in Data2:
    idd=int(idd)
    if idd in dic1:
        dif=abs(d-dic1[idd])
        if dif>4.3:
            dif_list.append(dif)
            

import matplotlib.pylab as plt
print('Cantidad de celdas con diferencias mayores a 4.3mm : ', len(dif_list))
plt.bar(range(len(dif_list)),dif_list)


from scipy.optimize import curve_fit

import numpy as np
def func(x, k):
    return k/x

xdata=[30., 50., 100., 250.,2000.]
ydata=[10., 7., 3., 0.6,0.25]

popt, pcov = curve_fit(func, xdata, ydata)
print(popt)