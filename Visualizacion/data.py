# -*- coding: utf-8 -*-
"""
Created on Sun Jan 12 13:09:07 2020

@author: JOliva
"""
import os
import datetime as dt
import numpy as np
import h5py
import time
import new_tool_utility as ntu
# import new_tool_graphic as ntg
class DataCube():
    
    def __init__(self, path):
        
        filename, ext = os.path.splitext(os.path.basename(path))
        self.path=path
        self.fileName=filename
        self.ext=ext        
        i=filename.upper().find('IBIS')
        self.radar=filename[i:i+6].replace('_',' ').replace('-',' ')
        self.StartDate=None
        if not os.path.exists(path):
            with h5py.File(self.path, 'w') as  hf:
                pass
        else:
            self.set_property()
            
    def set_xy(self,X,Y):
        with h5py.File(self.path, 'a') as  hf:
            hf.create_dataset('X',shape=(len(X),1), dtype=np.float64, data=X)
            hf.create_dataset('Y',shape=(len(Y),1), dtype=np.float64, data=Y)
        self.set_property()
            
    def set_property(self):
        
        XY=self.get_xy()

        X=sorted(list({x:0 for x in XY[:,0]}.keys()))
        Y=sorted(list({y:0 for y in XY[:,1]}.keys()))
        DistMinX=np.min(np.diff(X))
        DistMinY=np.min(np.diff(Y))
        self.CellSize=DistMinX
        
        self.xmin=np.min(X)
        self.xmax=np.max(X)
        self.ymin=np.min(Y)
        self.ymax=np.max(Y)
    
        self.ncols=len(X)
        self.nrows=len(Y)
        
        
    def set_desplacement(self,Date,desplacement):
        '''
        Parameters
        ----------
        date : datetime
            fecha de la adiquición del desplazamiento
        desplacement : array
            arreglo que contiene el desplazamiento de cada celda
        Returns
        None.
        '''
        Date=ntu.datetime_to_str(Date)
        with h5py.File(self.path, 'w') as  hf:
            hf.create_dataset(Date, data=np.array(desplacement,dtype=np.float32))
    
    
    def __str__(self):
        if self.StartDate is None:
            self.StartDate=self.get_dates()[0]
        s1='Name Proyect: %s\n'%(self.fileName)
        s2='Start Date  : %s\n'%(self.StartDate)
        s3='Cell size   : %.5f\n'%(self.CellSize)
        s4='x min       : %s\n'%(self.xmin)
        s5='x max       : %s\n'%(self.xmax)
        s6='y min       : %s\n'%(self.ymin)
        s7='y max       : %s\n'%(self.ymax)
        s8='n cols      : %s\n'%(self.ncols)
        s9='n rows      : %s\n'%(self.nrows)
        return s1+s2+s3+s4+s5+s6+s7+s8+s9
        
    def get_parameters(self):
        return self.xmin,self.ymin,self.ncols,self.nrows,self.CellSize
    def get_cells_size(self):
        return self.CellSize
    def get_xmin(self):
        return self.xmin
    def get_ymin(self):
        return self.ymin
    def get_name(self):
        return self.fileName
    
    def get_path(self):
        return self.path
    
    def get_radar(self):
        return self.radar
    
    def get_xy(self):
        '''
        Returns
        -------
        array nx2
        retorna un arreglo con nx2 qie contiene las componentes XY
        '''
        with h5py.File(self.path, 'r') as  hf:
            X=np.array(hf.get('X'),dtype=np.float64)
            Y=np.array(hf.get('Y'),dtype=np.float64)
        return np.hstack((X,Y))
    
    def get_dates(self):
        with h5py.File(self.path, 'r',libver='latest', swmr=True) as  hf:
            ListaDatesStr=list(hf.keys())
            ListaDatesStr.remove('X')
            ListaDatesStr.remove('Y')
            ListaDates=[ntu.str_to_datetime(f) for f in ListaDatesStr]
            # ListaDatesSorted=[datetime_to_str(f) for  f in sorted(ListaDates)]

        return sorted(ListaDates)
    
    def get_index_nearest_point(self,x,y):
            i,j=ntu.get_ij_from_xy(x,y,self.xmin,self.ymin,self.CellSize)
            index=j+i*self.ncols
            return index
    
    def get_desplacement_xy_for_dates(self, StartDate, Nsamples=1,EndDate=None,indexs=None):
        dates=self.get_dates()
        if dates[0]>StartDate:
            print('La fecha StartDate es menor que: ', dates[0])
        NewStartDate,StartIndex=ntu.find_date_nearest(StartDate,dates)
        
        if EndDate is None and len(dates[StartIndex:])<Nsamples:
            print('contiene menos muestras que : ',Nsamples)
            EndIndex=len(dates)
        elif EndDate is None and len(dates[StartIndex:])>=Nsamples:
            EndIndex=StartIndex+Nsamples
            
        elif EndDate is not None and len(dates[StartIndex:])>=Nsamples:
            if EndDate >dates[-1]:
                print('La fecha EndDate es mayor que: ', dates[-1])
            NewEndDate,EndIndex=ntu.find_date_nearest(EndDate,dates)
            EndIndex=EndIndex+1
        
        
        TW=[d for d in dates[StartIndex:EndIndex]]
        NameCols=[ntu.datetime_to_str(d) for d in TW]
        
        if indexs is None:
            with h5py.File(self.path, 'r',libver='latest', swmr=True) as  hf:
                desplacements=np.array(hf.get(NameCols[0]),dtype=np.float32)
                for Name in NameCols[1:]:
                    d=np.array(hf.get(Name),dtype=np.float32)
                    desplacements=np.vstack((desplacements,d))
                desplacements=np.transpose(desplacements)
        else:
            
            with h5py.File(self.path, 'r',libver='latest', swmr=True) as  hf:
                desplacements=np.array(hf.get(NameCols[0]),dtype=np.float32)[indexs]
                for Name in NameCols[1:]:
                    d=np.array(hf.get(Name),dtype=np.float32)[indexs]
                    desplacements=np.vstack((desplacements,d))
                desplacements=np.transpose(desplacements)
        return desplacements,TW
    
    def get_allxy_nearest_and_ts(self,xc,yc,r,StartDate,EndDate, form=1):
        XY=self.get_xy()

        if form==0:
            #forma de cuadrado
            indexs=[i for i,xy in enumerate(XY) if abs(xy[0]-xc)<=r and  abs(xy[1]-yc)<=r]
        elif form==1:
            #forma circular
            indexs=[i for i,xy in enumerate(XY) if np.sqrt((xy[0]-xc)**2 +(xy[1]-yc)**2)<=r]
        
        
        TS,TW=self.get_desplacement_xy_for_dates(StartDate,EndDate=EndDate,indexs=indexs)
        XY=XY[indexs]
        indexs_nonan=[i[0] for i in np.argwhere(~np.isnan(TS[:,0]))]
        
        newTS=TS[indexs_nonan]
        print('shapes: ',TS.shape, newTS.shape)
        for i in range(len(newTS)):
            newTS[i]=newTS[i]-newTS[i,0]
        newXY=XY[indexs_nonan,:]
        del XY, TS
        
        return newXY, newTS, np.array(TW,dtype=dt.datetime)


                


        
if __name__=='__main__':
    if False:
        path=r"C:\Users\JOliva\Documents\DataOUT\IBIS_2\MASTER_MLP_24122019_IBIS-2_MLP_24122019\Data Cube"
        nameCD=r"\MASTER_MLP_24122019_IBIS-2_MLP_24122019.h5"
    else:
        path=r"C:\EMT\Data_OUT\IBIS_2\MASTER_MLP_18012020_IBIS-2_MLP_24122019\Data Cube"
        nameCD=r"\MASTER_MLP_18012020_IBIS-2_MLP_24122019.h5"
    
    CuboDatos=DataCube(path+nameCD)
    
    print(CuboDatos)
    dates=CuboDatos.get_dates()
    print('Total de fechas: ',len(dates))
    print('Fecha de inicio: ',dates[0])
    print('Fecha de final : ',dates[-1])
    
    print('Radar:',CuboDatos.get_radar())
    t1=time.time()
    XY=CuboDatos.get_xy()
    print('tiempo en obtener XY del Cubo de datos: ',time.time()-t1)
    print(XY.shape)
    
    xc,yc=58750.0,91250.0
    radio = 250
    TS=CuboDatos.get_desplacement_xy_for_dates(dates[0],Nsamples=1)
    print('Dimension TS: ',TS.shape)
    indexs=np.argwhere(~np.isnan(TS))
    if True:
        indexs=[i for i,xy in enumerate(XY) if abs(xy[0]-xc)<=radio and  abs(xy[1]-yc)<=radio]
        
    print('Numero de indexs: ',len(indexs))
    t1=time.time()
    TS_eval=CuboDatos.get_desplacement_xy_for_dates(dates[0],EndDate=dates[143],indexs=indexs)
    print('tiempo en obtener sub XY: ',time.time()-t1)
    print('Numero de ts: ',TS_eval.shape)
    
    
    t1=time.time()
    for i in range(len(TS_eval)):
        TS_eval[i,:]=TS_eval[i,:]-TS_eval[i,0]
    
    
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    mpl.use('Qt5Agg')
    # plt.figure(1)
    # plt.plot(TS_eval)
    # plt.figure(2)
    # fig, ax=ntg.create_graph_2D('title')
    # ax.scatter(XY[indexs,0],XY[indexs,1],marker='s',c='silver',s=3,linewidths=1,alpha=0.8)
    plt.figure(3)
    x=np.array(range(0,10))
    y=np.nan*x
    plt.plot(x,y)
    # plt.plot(x,np.sin(x))
    # plt.show()
    
    
    
    

    
            
            
            
            
            
            
            
        

        
    
        
        
        