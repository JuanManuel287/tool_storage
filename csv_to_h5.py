import h5py
import os
from data import DataCube
import new_tool_utility as ntu
import new_tool_files as ntf
import numpy as np
import time

class CSV_TO_H5(DataCube):
	def __init__(self, pathCSV):
		path, ext = os.path.splitext(pathCSV)
		path=path+'.h5'
		print(path)
		DataCube.__init__(self,path)
		self._pathcsv4d=None
		self._pathcsv=pathCSV

	def do_grid(self,pathCSV4D,margin=50):
		self._pathcsv4d=pathCSV4D
		XY, _=ntu.do_grid(pathCSV4D,margin=1)
		self.set_xy(XY[:,0],XY[:,1])

	def converter_csv_to_h5(self):
		N=len(self.get_xy())
		print(self.get_ncols(),self.get_nrows())
		print(N)
		array=np.empty((N,1),dtype=np.float32)
		array[:]=np.nan
		with open(self._pathcsv,'r') as f:
			dates=f.readline().split(',')[1:]

		print('Total de fechas: ',len(dates))
		t1=time.time()
		with h5py.File(self._path, 'a') as  hf:
			for date in dates:
				hf.create_dataset(date,shape=(N,1), dtype=np.float32)
		print('tiempo en crear cada campo: %.2f'%(time.time()-t1))
		t1=time.time()
		with h5py.File(self._path, 'a') as  hf:
			with open(self._pathcsv,'r') as f:
				next(f)
				for i,line in enumerate(f):
					line=line.split(',')
					x,y=ntu.strXY_to_floatXY(line[0],5)
					index=self.get_index_nearest_point(x,y)
					print('fila : ',i)
					for value,date  in zip(map(float,line[1:]),dates):
						hf[date][index]=value

		print('tiempo en asignar los valores : %.2f'%(time.time()-t1))
				
if __name__=='__main__':

	

	pathCSV=r'D:\EMT\AAV\Datos y Algoritmos\Data Out\Cubo de Datos\Cubo de Datos 2019\DataCube_IBIS1_from_19_Ene_01-00_02_to_19_Jul_01-00_01.csv'
	pathCSV4D=r'D:\EMT\AAV\Datos y Algoritmos\Data In\Data para Cubo\Proyectos 2019\DataIn por proyecto\Proyecto 01Ene-01Jul_2019_IBIS1\Evento5_IBIS1_PRIMERSEMESTRE_2019_Mapa de Desplazamiento_from_19_Ene_01-00_02_to_19_Jul_01-00_01.csv4d'

	print(pathCSV)

	cubodatos=CSV_TO_H5(pathCSV)
	t1=time.time()
	cubodatos.do_grid(pathCSV4D)
	print('tiempo en crear la malla: %.2f'%(time.time()-t1))
	t1=time.time()
	cubodatos.converter_csv_to_h5()
	print('tiempo en convertir el csv a h5: %.2f'%(time.time()-t1))






		




    	

	    



