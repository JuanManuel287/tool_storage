
def main():
    path='"C:/piloto/tool_storage/tool_storage_data.py"'
    #'START /node 0 /affinity {} cmd /C "C:\Program Files (x86)\Rocscience\Slide 6.0\aslidew.exe"'.format()
    
    
    node = 0

    if False:
        ListIBIS=["IBIS_1/","IBIS_2/","IBIS_3/","IBIS_4/","IBIS_5/","IBIS_6/"]
        List_to_Cpu=[0,1,2,3,4,5]
    else:
        ListIBIS=["IBIS_2/","IBIS_3/"]
        List_to_Cpu_DataCube=[0,1]
        List_to_Cpu_DTM=[2,3]
        List_to_Cpu_TS=[4,5]
        List_to_Cpu_M1=[6,7]
        List_to_Cpu_M2=[8,9]
        List_to_Cpu_M3=[10,11]
        List_to_Cpu_M4=[12,13]
        List_to_Cpu_Eval=[List_to_Cpu_M1,List_to_Cpu_M2,List_to_Cpu_M3,List_to_Cpu_M4]
        
        PathModels="C:/piloto/modelos/"
        List_NameModels=["MMLP_Media_EV_1-2-3_Nobalanced","MMLP_Media_EV_6-7-8-9-10-13-16_Nobalanced"]
        
        
    PathINDir="C:/EMT/Data_CSV4D/"
    PathOUTDir="C:/EMT/Data_OUT/"
    

    
    with open('affinity_CUBO_DATOS.bat', 'w') as output:
        opc=1
        line_write='call C:/Anaconda3/Scripts/activate.bat\n'
        output.writelines(line_write)
        for j,radar in zip(List_to_Cpu_DataCube,ListIBIS):
            PathIN='"{}{}"'.format(PathINDir,radar)
            PathOUT='"{}{}"'.format(PathOUTDir,radar)
            k = 2**j
            print(j,k)
            line_write='START /node {} /affinity {} python {} {} {} {}\n'.format(node, k,path,opc,PathIN,PathOUT)
            output.writelines(line_write)
# 	    line_write = 'wmic process where name="aslidew.exe" CALL setpriority "high priority"'
# 	    output.writelines(line_write)
                
                
    with open('affinity_DTM.bat', 'w') as output:
        opc=2
        line_write='call C:/Anaconda3/Scripts/activate.bat\n'
        output.writelines(line_write)
        for j,radar in zip(List_to_Cpu_DTM,ListIBIS):
            PathIN='"{}{}"'.format(PathINDir,radar)
            PathOUT='"{}{}"'.format(PathOUTDir,radar)
            k = 2**j
            print(j,k)
            line_write='START /node {} /affinity {} python {} {} {} {}\n'.format(node, k,path,opc,PathIN,PathOUT)
            output.writelines(line_write)
            
    with open('affinity_TS.bat', 'w') as output:
        opc=3
        line_write='call C:/Anaconda3/Scripts/activate.bat\n'
        output.writelines(line_write)
        for j,radar in zip(List_to_Cpu_TS,ListIBIS):
            PathIN='"{}{}"'.format(PathINDir,radar)
            PathOUT='"{}{}"'.format(PathOUTDir,radar)
            k = 2**j
            print(j,k)
            line_write='START /node {} /affinity {} python {} {} {} {}\n'.format(node, k,path,opc,PathIN,PathOUT)
            output.writelines(line_write)
            
    with open('affinity_Eval.bat', 'w') as output:
        opc=4
        line_write='call C:/Anaconda3/Scripts/activate.bat\n'
        output.writelines(line_write)
        for NameModel,List_to_Cpu in zip(List_NameModels,List_to_Cpu_Eval):
            for j,radar in zip(List_to_Cpu,ListIBIS):
                PathIN='"{}{}"'.format(PathINDir,radar)
                PathOUT='"{}{}"'.format(PathOUTDir,radar)
                k = 2**j
                print(j,k)
                line_write='START /node {} /affinity {} python {} {} {} {} "{}" {}\n'.format(node, k,path,opc,PathIN,PathOUT,PathModels,NameModel)
                output.writelines(line_write)
            
    
            

if __name__ == '__main__':
    main()
