import os
import datetime as dt
import numpy as np
import h5py
import time
import new_tool_utility as ntu

class DataCube():
    
    def __init__(self, path):
        
        filename, ext = os.path.splitext(os.path.basename(path))
        self._pathdir= path.replace(filename+ext,'')
        self._path=path
        self._fileName=filename
        self._ext=ext        
        i=filename.upper().find('IBIS')
        self._radar=filename[i:i+6].replace('_',' ').replace('-',' ')
        self._StartDate=None
        self._formatRegistry='date from csv4d: %s; length save data:  %d; distance max y min: (%.3f,%.3f); date save: %s\n'.format()
        if not os.path.exists(path):
            with h5py.File(self._path, 'w') as  hf:
                pass
        else:
            try:
                self.set_property()
            except:
                pass
            
    def set_xy(self,X,Y):
        try:
            with h5py.File(self._path, 'a') as  hf:
                hf.create_dataset('X',shape=(len(X),1), dtype=np.float64, data=X)
                hf.create_dataset('Y',shape=(len(Y),1), dtype=np.float64, data=Y)
            self.set_property()
        except:
            print('No se puede agregar XY, dado que ya contiene las coordenadas.')
            
    def set_property(self):
        
        XY=self.get_xy()

        X=sorted(list({x:0 for x in XY[:,0]}.keys()))
        Y=sorted(list({y:0 for y in XY[:,1]}.keys()))
        DistMinX=np.min(np.diff(X))
        DistMinY=np.min(np.diff(Y))
        self._CellSize=DistMinX
        
        self._xmin=np.min(X)
        self._xmax=np.max(X)
        self._ymin=np.min(Y)
        self._ymax=np.max(Y)
    
        self._ncols=len(X)
        self._nrows=len(Y)
        
    def add_data(self,name):
    	with h5py.File(self._path, 'a') as  hf:
            try:
                del hf[Date]
            except:
                pass
            hf.create_dataset(Date, data=np.array(data,dtype=dtype))

    def set_data(self,Date,data,dtype=np.float32,lenCSV4D=np.nan,DistMaxMin=(np.nan,np.nan)):
        Date=ntu.datetime_to_str(Date)
        self.add_data(Date,data)
        lenData=len(np.where(~np.isnan(data)))
        self.save_registry(Date,lenCSV4D,lenData,DistMaxMin)
        del data

    def set_data_from_csv4d(self,pathCSV4D,dtype=np.float32):
        XYZD=ntf.get_xyzd_from_csv4d(pathCSV4D,5)
        INDEXS=self.get_INDEX_nearest_point(XYZD[:,0],XYZD[:,1])
        DateCSV4D=ntu.datetime_to_str(ntf.get_info_from_csv4d(pathCSV4D)[1])
        
        data=np.empty(self._nrows*self._ncols,dtype=np.float32)
        data[:]=np.nan
        XY=self.get_xy()
        if len(INDEXS)==len(XYZD):
        	data[INDEXS]=XYZD[:,-1]
        	dist=np.sqrt((XY[INDEXS,0]-XYZD[:,0])**2 + (XY[INDEXS,1]-XYZD[:,1])**2)
        else:
        	dist=[]
        	for x,y,_,d in XYZD:
	            index=self.get_index_nearest_point(x,y)
	            data[index]=d
	            xl,yl=XY[index,0],XY[index,1]
	            dist.append(np.sqrt((x-xl)**2 + (y-yl)**2))
        
        DistMaxMin=(np.nanmax(dist),np.nanmin(dist))

        self.add_data(DateCSV4D,data)
        lenData=len(INDEXS)
        lenCSV4D=len(XYZD)
        self.save_registry(dateCSV4d,lenCSV4D,lenData,DistMaxMin)
        del data, XY, XYZD, INDEXS, dist, 

    def save_registry(self,DateCSV4D,lenCSV4D,lenData,DistMaxMin):
        DateNow=ntu.datetime_to_str(dt.datetime.now(),fmt='%Y-%m-%d %H:%M:%S')
        with open(self._pathdir+'registry.log','a') as f:
            f.write(self._formatRegistry%(DateCSV4D,lenCSV4D,lenData,DistMaxMin[0],DistMaxMin[1],DateNow))


    
    
    def __str__(self):
        if self._StartDate is None:
            self._StartDate=self.get_dates()[0]
        s1='Name Proyect: %s\n'%(self._fileName)
        s2='Start Date  : %s\n'%(self._StartDate)
        s3='Cell size   : %.5f\n'%(self._CellSize)
        s4='x min       : %s\n'%(self._xmin)
        s5='x max       : %s\n'%(self._xmax)
        s6='y min       : %s\n'%(self._ymin)
        s7='y max       : %s\n'%(self._ymax)
        s8='n cols      : %s\n'%(self._ncols)
        s9='n rows      : %s\n'%(self._nrows)
        return s1+s2+s3+s4+s5+s6+s7+s8+s9
        
    def get_parameters(self):
        return self._xmin,self._ymin,self._ncols,self._nrows,self._CellSize
    def get_cells_size(self):
        return self._CellSize
    def get_xmin(self):
        return self._xmin
    def get_ymin(self):
        return self._ymin
    def get_xy_min(self):
    	return (self._xmin,self._ymin)
    def get_ncols(self):
        return self._ncols
    def get_nrows(self):
        return self._nrows
    def get_dim(self):
    	return (self._nrows,self._ncols)
    def get_name(self):
        return self._fileName
    
    def get_path(self):
        return self._path
    
    def get_radar(self):
        return self._radar
    
    def get_xy(self):
        '''
        Returns
        -------
        array nx2
        retorna un arreglo con nx2 que contiene las componentes XY
        '''
        with h5py.File(self._path, 'r') as  hf:
            X=np.array(hf.get('X'),dtype=np.float64)
            Y=np.array(hf.get('Y'),dtype=np.float64)
        return np.hstack((X,Y))
    
    def get_dates(self):
        with h5py.File(self._path, 'r',libver='latest', swmr=True) as  hf:
            ListaDatesStr=list(hf.keys())
            ListaDatesStr.remove('X')
            ListaDatesStr.remove('Y')
            try:
                ListaDatesStr.remove('model')
            except:
                pass
            try:
                ListaDatesStr.remove('Z')
            except:
                pass

            ListaDates=[ntu.str_to_datetime(f) for f in ListaDatesStr]
            # ListaDatesSorted=[datetime_to_str(f) for  f in sorted(ListaDates)]

        return np.array(sorted(ListaDates),dtype=dt.datetime)
    
    def get_index_nearest_point(self,x,y):
        i,j=ntu.get_ij_from_xy(x,y,self._xmin,self._ymin,self._CellSize)
        index=j+i*self._ncols
        return index
    def get_INDEX_nearest_point(self,X,Y):
        I,J=ntu.get_IJ_from_XY(X,Y,self._xmin,self._ymin,self._CellSize)
        INDEX=J+I*self._ncols
        return INDEX

    def get_xy_nearest(self,xc,yc,r,geometry=1):
        XY=self.get_xy()
        if geometry==0:
            #forma de cuadrado
            indexs=[i for i,xy in enumerate(XY) if abs(xy[0]-xc)<=r and  abs(xy[1]-yc)<=r]
        if geometry==1:
            #forma circular
            indexs=[i for i,xy in enumerate(XY) if np.sqrt((xy[0]-xc)**2 +(xy[1]-yc)**2)<=r]
        
        return XY[indexs],indexs
    
    def get_data(self,Date):
        NameCol=ntu.datetime_to_str(Date)
        with h5py.File(self._path, 'r',libver='latest', swmr=True) as hf:
            return np.array(hf.get(NameCol),dtype=np.float32)

    def get_subdata(self,XY,Date,indexs=None):
        if indexs is None:
            indexs=self.get_INDEX_nearest_point(XY[:,0],XY[:,1])
        NameCol=ntu.datetime_to_str(Date)
        with h5py.File(self._path, 'r',libver='latest', swmr=True) as hf:
            return np.array(hf[NameCol],dtype=np.float32)[indexs]
            
    def get_subdata_block(self, StartDate,EndDate,indexs=None):
        t1=time.time()
        dates=self.get_dates()
        #print('tiempo extraer todas las fechas : ',time.time()-t1)
        
        t1=time.time()
        NewStartDate,StartIndex=ntu.find_date_nearest(StartDate,dates)
        NewEndDate,EndIndex=ntu.find_date_nearest(EndDate,dates)
        #print('tiempo en buscar fecha mas cercana : ',time.time()-t1)
        
        t1=time.time()
        TW= dates[StartIndex:EndIndex+1]
        NameCols=[ntu.datetime_to_str(d) for d in TW]
        #print('Tiempo en convertir en string las fechas: ',time.time()-t1)
        if indexs is None:
            XY=self.get_xy()
            indexs=[i for i in range(len(XY))]
        # print('total de fechas: ',len(TW))
        t1=time.time()
        with h5py.File(self._path, 'r',libver='latest', swmr=True) as  hf:
            subdata=[np.array(hf.get(Name),dtype=np.float32)[indexs] for Name in NameCols]
            subdata=np.transpose(subdata)
        #print('Tiempo en extraer todos los datos de cubo de datos: ',time.time()-t1)
            # print(displacements.shape)
        return subdata,TW

    def check_data_status(self):
        n=len(self.get_xy())
        dates=self.get_dates()
        dates_corrupted=[Date for Date in dates if len(self.get_data(Date))!=n]
        if len(dates_corrupted)>0:
            print('Existen datos incompletos en las fechas:\n',dates_corrupted)
        else:
            print('No existen datos incompletos')



class DataCubeProbability(DataCube):
    def __init__(self,path,nameTSP,pathmodel,namemodel):
        ntf.make_dir(path+'/'+namemodel+'/')
        pathDataRecover=path+'/'+namemodel+'/data_recover.npz'
        path=path+'/'+namemodel+nameTSP
        DataCube.__init__(self,path)
        self._pathDataRecover=pathDataRecover
        self._pathmodel=pathmodel
        self._namemodel=namemodel
        #self.set_pathmodel(pathmodel,namemodel)
        

    def get_pathmodel(self):
        with h5py.File(self._path, 'r',libver='latest', swmr=True) as hf:
            data=np.array(hf.get('model'),dtype=np.str)
        
        self._pathmodel=data
        self._namemodel=data
        return self._pathmodel,self._namemodel


    def set_pathmodel(self,pathmodel,namemodel):
        with h5py.File(self._path, 'r',libver='latest', swmr=True) as  hf:
            Listakey=list(hf.keys())
        if 'model' in Listakey:
            self.get_pathmodel()
        else:
            self._pathmodel=pathmodel
            self._namemodel=namemodel
            data=[pathmodel,namemodel]
            print('data:',data)
            with h5py.File(self._path, 'a') as  hf:
                hf.create_dataset('model', (len(data),1),'|S24',data)

    def create_ts_probability_from_ts_TimeSerie(self,CDTimeSerie):
        model=load_model(self._pathmodel+self._namemodel+'.h5')
        XY=CDTimeSerie.get_xy()
        self.set_xy(XY[:,0],XY[:,1])
        dates=CDTimeSerie.get_dates()
        datesProb=self.get_dates()
        #self.check_data_status()
        StartDate=dates[143]
        if len(datesProb)>0:
            print('ultima fecha guardada: %s'%(datesProb[-1]))
            StartDate=dates[dates>=datesProb[-1]][0]


        print(StartDate)
        EndDate=dates[-1]
        t1=time.time()
        XY_TS_TW_INDEXS=CDTimeSerie.get_time_serie_for_eval_sequential(XY,StartDate,EndDate,pathRec=self._pathDataRecover)
        print('tiempo en obtener toda las time serie como generador: ',time.time()-t1)
        t2=time.time()
        data=np.empty(len(XY),dtype=np.float16)
        
        for XY_eval, TS_eval, TW_eval, indexs  in XY_TS_TW_INDEXS:
            error=True
            while error:
                try:
                    t1=time.time()
                    
                    data[:]=np.nan
                    Date=TW_eval[-1]
                    
                    data[indexs]=model.predict(TS_eval)[:,1]
                    self.set_data(Date,data,dtype=np.float16)
                    print('tiempo en un ciclo:%.1f '%(time.time()-t1), ', Num celdas: %d, fecha:%s, momento:%s'%(len(XY_eval),Date,dt.datetime.now()))
                    gc.collect()
                    error=False
                except:
                    pass
           

        print('tiempo total: ',time.time()-t2)



    def get_sub_ts_probability(self,XY,StartDate,EndDate):
        indexs = self.get_INDEX_nearest_point(XY[:,0],XY[:,1])
        PM, TW = self.get_subdata_block(StartDate,EndDate,indexs=indexs)
        return PM, TW

    def get_number_cell_above_threshold(self,XY,StartDate,EndDate,threshold=0.9):

        TSProbablity, TW=self.get_sub_ts_probability(XY,StartDate,EndDate)
        

        # index_nonan=[k for k in range(len(PM)) if ~np.isnan(PM[k,:]).all()]
        # if len(index_nonan)>0:
        #     PM=PM[index_nonan]
        #     XY=XY[index_nonan]
        NCells=[]

        n,m = TSProbablity.shape
        for v in np.transpose(TSProbablity):
            NCells.append(len(v[v>threshold]))
        NCells=np.array(NCells,dtype=np.int32)
        return NCells,TW


def process_time_series(PathIN,PathOUT):
    PathOUT1=PathOUT
    
    fileprint='prints_ts.txt'
    NameRegistry='read_csv4d_ts.dat'
    NameRegistryGlobal='Header_Data.csv'
    NameRegistryLog='registry_ts.log'
    NameTS='name_registry_ts.log'
    
    while True:
        ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
        if len(ListFiles)>1:
            NameSorted=get_names_datacubes(ListFiles)
            NameSortedStart=[name for name in NameSorted]
            break
        time.sleep(3)
        
        
    NameSortedStart=[NameSortedStart[-1]]
    

    INFO=None
    while True: 
        name=NameSortedStart[0]
        PathOUT=PathOUT1+name+'/'
        PathOUT2=PathOUT+'temp_Time_Serie/'
        ntf.make_dir(PathOUT2)
        
        ListFiles=get_files_for_datacube(PathIN,name)
        NewListFiles=ntf.get_new_files_with_data(ListFiles,PathOUT2+NameRegistry)
        
        if len(NewListFiles)>0 and os.path.exists(PathOUT+NameRegistryGlobal):
            
            NewListFilesSorted=ntf.sorted_files_list_for_date(NewListFiles)
            
            if not ntf.exist_file_with_ext(PathOUT2,'.npz'):
                f = open(PathOUT2+fileprint, 'a')
                sys.stdout = f
                INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
                init_time_serie(PathOUT,PathOUT2,NameRegistry,NameRegistryLog,INFO,NameTS,NewListFilesSorted)
                
                f.close()
            else:
                if INFO is None:
                    INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
                
                if name!=NameSortedStart[-1]:
                    NameSortedStart.pop(0)
                else:
                    ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
                    NameSortedAux=get_names_datacubes(ListFiles)
                    for n in NameSortedAux:
                        if n not in NameSorted:
                            NameSortedStart.append(n)
                    
                for fileCSV4D in NewListFilesSorted[:-1]:
                    
                    f = open(PathOUT2+fileprint, 'a')
                    sys.stdout = f
                    
                    # if NewListFilesSorted[-1]==fileCSV4D:
                    #     ntf.check_read_file(fileCSV4D)
                    try:
                        path_npz=update_times_series(fileCSV4D,PathOUT2,INFO,NameTS)
                        ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
                        ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
                        
                        
                    except:
                        pass
                    
                    # Z=ntu.set_values_Z(INFO,fileCSV4D,Z)
                    
                    # process_evaluation(PathOUT,PathModel,NameModel,path_npz,INFO,DataColors_list,X,Y,Z,PM)
                    
                    f.close()
                    
     
        time.sleep(1)