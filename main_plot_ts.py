# -*- coding: utf-8 -*-
"""
Created on Wed Jan 29 12:43:27 2020

@author: joliva
"""

from data_struct import DataCube
import new_tool_utility as ntu
import new_tool_graphic as ntg



import os
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import datetime as dt
import time
mpl.use('Qt5Agg')

import gc

def get_path_and_filename():
    validate=True
    while validate:
        path=r"{}".format(input('Ingresar path del cubo de datos: '))
        if os.path.isdir(path):
            validate=False
    validate=True
    while validate:
        nameCD=r"\{}.h5".format(input('Ingresar nombre del cubo de datos: '))
        if os.path.exists(path+nameCD):
            validate=False
    return path,nameCD
        
    
def get_xy_and_radio():
    validate=True
    while validate:
        try:
            xy=input('Ingresar punto x,y : ')
            xy=xy.split(',')
            xc,yc=float(xy[0]),float(xy[1])
            validate=False
        except:
            print('Error en los datos al ingresar las coordenadas')
            
    validate=True
    while validate:
        try:
            r=float(input('Ingresar el radio : '))
            validate=False
            if r<0:
                validate=True
                print('El radio debe ser mayor a cero')
        except:
            print('El radio debe ser un numero y mayor que cero')
    return xc,yc,r


def get_dates():
    validate=False
    while not validate:
        StartDate=input('Ingresar fecha de inicio (yyyy-mm-dd HH:MM) : ')
        StartDate,validate=validate_date(StartDate)
    
    validate=False
    while not validate:
        EndDate=input('Ingresar fecha final (yyyy-mm-dd HH:MM) : ')
        EndDate,validate=validate_date(EndDate)
    return StartDate,EndDate
    
    
def validate_date(Date):
    try:
        Date=ntu.str_to_datetime(Date,fmt='%Y-%m-%d %H:%M')
        validate=True
        return Date,validate
    except:
        validate=False
        print('Fecha incorrecta, verificar formato')
        return Date,validate
    
def get_datacube():
    path,nameCD=get_path_and_filename()
    CuboDatos=DataCube(path+nameCD)
    return CuboDatos,nameCD

def OnSelect(event,fig,axTS,ax2D,s,XY,TS,TW,DiccXY):

    ax = event.inaxes
    Labels=list(DiccXY.keys())
    if ax==axTS:
        for line in ax.get_lines():
            if line.contains(event)[0]:
                xy=line.get_label()
                x,y=ntu.strXY_to_floatXY(xy,2)
                ntg.graph_2D_only_xy(ax2D,XY,Labels,s=s,linewidths=s)
                ax2D.scatter(x,y,marker='s',c='r',s=s,linewidths=s)
                fig.canvas.draw()
                fig.canvas.flush_events()
                break
    else:
        axTS.clear()
        x,y=event.xdata, event.ydata
        print(x,y)
        # i,j=ntu.get_ij_from_xy(x,y,xmin,ymin,cell_size)
        # xmin,ymin,cells_size=Parameters
        for line in ax.get_lines():
            if line.contains(event)[0]:
                axTS.clear()
                
                ymin=np.nanmin(TS)
                ymax=np.nanmax(TS)
                ntg.set_properties_graphic_TS(axTS,ymin,ymax)
                xy=line.get_label()
                index=DiccXY[xy]
                x,y=xy.split('-')
                
                # ntg.graph_2D_only_xy(ax2D,XY,Labels,s=s,linewidths=s)
                ax2D.scatter(float(x),float(y),marker='s',c='r',s=s,linewidths=s)
                axTS.plot(TW,TS[index,:],label=xy)
                fig.canvas.draw()
                fig.canvas.flush_events()
                gc.collect()
                break
def complete_nan(XY,TS):
    t1=time.time()
    index_nonan=[k for k in range(len(TS)) if ~np.isnan(TS[k,:]).all()]
    print('Tiempo en buscar los nonan : ',time.time()-t1)
    newTS=TS[index_nonan]
    newXY=XY[index_nonan]
    
    t1=time.time()
    indexs_nan=np.where(np.isnan(newTS[:,0]))[0]
    for k in indexs_nan:
        i=np.where(~np.isnan(newTS[k,:]))[0][0]
        newTS[k,0]=newTS[k,i]
    print('tiempo en cambiar nan de la primera columna con valores: ',time.time()-t1)
       
    t1=time.time()
    mask = np.isnan(newTS)
    idx = np.where(~mask,np.arange(mask.shape[1]),0)
    np.maximum.accumulate(idx,axis=1, out=idx)
    newTS[mask] = newTS[np.nonzero(mask)[0], idx[mask]]

    return newXY,newTS   
def get_time_series(CuboDatos,XY,StartDate,EndDate,indexs=None):
    
    #t1=time.time()
    if indexs is None:
        indexs=CuboDatos.get_INDEX_nearest_point(XY[:,0],XY[:,1])
    #print('tiempo en crear index ts: ',time.time()-t1)
    #t1=time.time()
    TS,TW=CuboDatos.get_subdata_block(StartDate,EndDate,indexs=indexs)
    #print('tiempo sub ts: ',time.time()-t1)
    
    return TS,TW

def get_time_serie_for_eval(CuboDatos,XY,StartDate,EndDate):
    TS,TW=get_time_series(CuboDatos,XY,StartDate,EndDate)
    newXY,newTS=complete_nan(XY,TS)
    newTS=np.transpose(np.subtract(np.transpose(newTS), newTS[:,0]))
    return newXY,newTS,TW
    

def main():

    salir=False
    

    # CuboDatos,nameCD=get_datacube()
    # xc,yc,r=get_xy_and_radio()
    # StartDate,EndDate=get_dates()
    
    if False:
        # path=r"C:\EMT\Data_OUT\IBIS_3\MASTER_MLP_18012020_IBIS-3_MLP_24122019\Data Cube"
        # nameCD=r"\MASTER_MLP_18012020_IBIS-3_MLP_24122019.h5"
        
        path=r"C:\EMT\Data_OUT\IBIS_2\MASTER_MLP_18012020_IBIS-2_MLP_24122019\Data Cube"
        nameCD=r"\MASTER_MLP_18012020_IBIS-2_MLP_24122019.h5"
    else:
        path=r"D:\Respaldo OUT\IBIS_2\MASTER_MLP_18012020_IBIS-2_MLP_24122019\Data Cube"
        nameCD=r"\MASTER_MLP_18012020_IBIS-2_MLP_24122019.h5"

    
    CuboDatos=DataCube(path+nameCD)
    
    # xc,yc,radio=59491.5,90367.3,250
    xc,yc,radio=58395.2,91166.5,15 #Coordenadas evento 2 IBIS2
    StartDate=ntu.str_to_datetime('2020-02-13 00:00',fmt='%Y-%m-%d %H:%M')
    EndDate=ntu.str_to_datetime('2020-02-14 12:00',fmt='%Y-%m-%d %H:%M')
    # EndDate=dt.datetime.now()
    # StartDate=EndDate-dt.timedelta(days=10,hours=0)
    print(StartDate,EndDate)

    while not salir:
        XY,_=CuboDatos.get_xy_nearest(xc,yc,radio)
        XY,TS,TW=get_time_serie_for_eval(CuboDatos,XY,StartDate,EndDate)

        ts_min=np.nanmin(TS)-2
        ts_max=np.nanmax(TS)+2
        title='Time Serie\nCubo de datos: %s\nVentana temporal: %s - %s\n'%(nameCD,TW[0],TW[-1])
        title=title+'x-y: %s - %s, radio: %s'%(xc,yc,radio)
        
        fig,ax,ax2D=ntg.create_graph_TS_2D(ts_min,ts_max,TW,title,'%d %H:%M')
        
        s=310.84577978/radio
        print('s=',s)
        
        Labels=[str(x)+'-'+str(y) for x,y in XY]
        DiccXY={label:i for i,label in enumerate(Labels)}
        
        ntg.graph_2D_only_xy(ax2D,XY,Labels,s=s,linewidths=s)
        
        
        
        for ts,label in zip(TS,Labels):
            ax.plot(TW,ts,label=label)
            
        fig.canvas.mpl_connect('button_press_event', lambda event:  OnSelect(event,fig, ax,ax2D,s,XY,TS,TW,DiccXY))

        plt.show()
        resp=input('Desea evaluar otro punto? S/N : ')
        if resp.upper()=='S':
            xc,yc,radio=get_xy_and_radio()
            resp=input('Desea mantener el cubo de datos? S/N : ')
            if resp.upper()!='S':
                CuboDatos,nameCD=get_datacube()
            
            resp=input('Desea mantener la ventana temporal? S/N : ')
            if resp.upper()!='S':
                StartDate,EndDate=get_dates()
            
                
            salir=False
        else:
            salir=True


            
            
        
if __name__=='__main__':
    main()