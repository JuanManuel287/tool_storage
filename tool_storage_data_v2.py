# -*- coding: utf-8 -*-

import numpy as np 
import matplotlib.pylab as plt
import matplotlib as mpl
mpl.use('Agg')

import os
from datetime import datetime,timedelta
import h5py

import time
from sys import getsizeof
import sys


from tensorflow.keras.models import load_model
import new_tool_utility as ntu
import new_tool_files as ntf



#from tool_utility import tool_utility as tu
import new_tool_graphic as ntg

from data import DataCube

def get_nearest_point(point,DicAllPoints,R):
    #Función que busca el punto mas cercano dado un radio R al punto xy
    #
    NearestPoint=''
    xc,yc=ntu.strXY_to_floatXY(point)
#    print('punto float : ',xc,yc)
    for x in DicAllPoints:
        xx=float(x)
        if np.sqrt((xc-xx)**2)<R:
            for y in DicAllPoints[x]:
                yy=float(y)
                d=np.sqrt((xc-xx)**2 + (yc-yy)**2)
                if d<=R:
                    R=d
                    NearestPoint=x+'-'+y
  
    
    return NearestPoint
def get_nearest_point2(xy,x_min,y_min,cell_size,k):
    x,y=ntu.strXY_to_floatXY(xy,k)
    i,j=ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size)
    x,y=ntu.get_xy_from_index(i,j,x_min,y_min,cell_size)
    return ntu.floatXY_to_strXY(x,y,k)
        
def create_dic_allpoint(XY):
    DicAllPoints={}
    
    for i,xy in enumerate(XY):
        x,y=xy.split('-')
        if x not in DicAllPoints:
            DicAllPoints[x]={}
        DicAllPoints[x][y]=i
    return DicAllPoints


def create_all_cells(fileCSV4D,k,NumCell=1200):
    # data=get_xyzd_from_csv4d(fileCSV4D,5)
    data=ntf.get_xyzd_from_csv4d(fileCSV4D,5)
    x_min=np.inf
    x_max=-1
    y_min=np.inf
    y_max=-1
    
    dist_min=np.inf
    x_aux=0
    y_aux=0
    dist_x=np.inf
    dist_y=np.inf
    
    # XY=np.array([strXY_to_floatXY(xy,5) for xy in data])
    XY=np.array([xy for xy in data])
    X=sorted(list({x:0 for x in XY[:,0]}.keys()))
    Y=sorted(list({y:0 for y in XY[:,1]}.keys()))
    
    for i in range(len(X)-1):
        if abs(X[i]-X[i+1])<dist_x:
            dist_x=abs(X[i]-X[i+1])
        if abs(Y[i]-Y[i+1])<dist_y:
            dist_y=abs(Y[i]-Y[i+1])
    
    cell_size=np.round(dist_x,5)
    x_max=X[-1]
    x_min=X[0]
    y_max=Y[-1]
    y_min=Y[0]
    
    
    numcell_x=int((x_max-x_min)/cell_size)
    numcell_y=int((y_max-y_min)/cell_size)
    if numcell_x>NumCell or  numcell_y>NumCell:
        NumCell=max(numcell_x,numcell_y)+100
        
    if numcell_x<NumCell: 
        cell_expand_x=int((NumCell-numcell_x)/2.)+1
    if numcell_y<NumCell: 
        cell_expand_y=int((NumCell-numcell_y)/2.)+1
    
    # x_min=np.trunc((x_min-cell_expand_x*cell_size)*10**k)/10**k
    # x_max=np.trunc((x_max+cell_expand_x*cell_size)*10**k)/10**k
    # y_min=np.trunc((y_min-cell_expand_y*cell_size)*10**k)/10**k
    # y_max=np.trunc((y_max+cell_expand_y*cell_size)*10**k)/10**k
    
    x_min=np.round((x_min-cell_expand_x*cell_size),k)
    x_max=np.round((x_max+cell_expand_x*cell_size),k)
    y_min=np.round((y_min-cell_expand_y*cell_size),k)
    y_max=np.round((y_max+cell_expand_y*cell_size),k)
    
    
    m=int((x_max-x_min)/cell_size)+1
    n=int((y_max-y_min)/cell_size)+1
    
    info=[m,n,x_min,x_max,y_min,y_max,cell_size,dist_x,dist_y]
    
    XY=create_list_XY(x_min,m,y_min,n,cell_size,k)
    #
    return XY,info

def create_list_XY(x_min,m,y_min,n,cell_size,k):
    X=np.array([x_min + cell_size*i for i in range(m)])
    Y=np.array([y_min + cell_size*i for i in range(n)])

    return np.round(np.array([(x,y) for y in Y for x in X],dtype=np.float64),k)
    
    
def get_XY_from_H5(path_fileh5):
    with h5py.File(path_fileh5, 'r') as  hf:
        X=np.array(hf.get('X'),dtype=np.float64)
        Y=np.array(hf.get('Y'),dtype=np.float64)
    return np.hstack((X,Y))

def get_dates_from_H5(path_fileh5):
    with h5py.File(path_fileh5, 'r') as  hf:
        ListaDatesStr=list(hf.keys())
        ListaDatesStr.remove('X-Y')
        ListaDates=[ntu.str_to_datetime(f) for f in ListaDatesStr]
    # ListaDatesSorted=[datetime_to_str(f) for  f in sorted(ListaDates)]

    return sorted(ListaDates)




def save_historical_data(fileCSV4D,cubo_datos):
    '''
    fileCSV4D: direccion y nombre del archivo csv4d
    PathOUT=directorio de salida
    '''
    
    x_min,y_min,n_cols,_,cell_size= cubo_datos.get_parameters()
     
    XY=cubo_datos.get_xy()
    data=ntf.get_xyzd_from_csv4d(fileCSV4D,5)
    desplazamiento=np.empty(len(XY),dtype=np.float32)
    desplazamiento[:]=np.nan
    dist=[]
    indexes={}
    for x,y,_,desp in data:
        i,j=ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size)
        index=j+i*n_cols
        desplazamiento[index]=desp
            
        xl,yl=XY[index,0],XY[index,1]
        d=np.sqrt((x-xl)**2 + (y-yl)**2)
        dist.append(d)
            
    dist_max=np.max(dist)
    dist_min=np.min(dist)
    cont=len(np.argwhere(~np.isnan(desplazamiento)))
        
    print('csv4d: ',EndDate,'celdas guardadas: ',cont,'de',len(data),'; dist max y min: ',dist_max,dist_min,';fecha:', datetime.now())
    
    _,EndDate,_=ntf.get_info_from_csv4d(fileCSV4D)
    cubo_datos.set_desplacement(EndDate,desplazamiento)




def get_names_datacubes(ListFiles):
    DiccNamesData={}
    NameSorted=[]
    DateBig=datetime(1900,1,1)
    for fileCSV4D in ListFiles:
        _,EndDate,Name=ntf.get_info_from_csv4d(fileCSV4D)
        if ntf.check_data_in_csv4d(fileCSV4D):
            if Name not in DiccNamesData :
                DiccNamesData[Name]=datetime(1900,1,1)
        
            if DiccNamesData[Name]<EndDate:
                DiccNamesData[Name]=EndDate
            if DateBig<EndDate:
                DateBig=EndDate
                
    for name in DiccNamesData:
        if DiccNamesData[name]>=DateBig:
            LastName=name
        else:
            NameSorted.append(name)
    NameSorted.append(LastName)
    
    return NameSorted
    

def get_files_for_datacube(PathIN,name=None):
    ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
    if name is None:
        return ListFiles
        
    
    NewListFile=[]
    for fileCSV4D in ListFiles:
        if name in fileCSV4D:
            NewListFile.append(fileCSV4D)
    return NewListFile
            
    
def init_datacube(PathOUT,PathOUT2,fileCSV4D,NameRegistry,NameRegistryLog,NameRegistryGlobal,cubo_datos):
    StartDate,_,Name=ntf.get_info_from_csv4d(fileCSV4D)
    StartDate=ntu.datetime_to_str(StartDate)
    
    XY, info = create_all_cells(fileCSV4D,5)
    ntf.save_registry_global(PathOUT+NameRegistryGlobal,Name,StartDate,info)
    
    cubo_datos.set_xy(XY[:,0],XY[:,1])
    print('Se inicializa el cubo de datos : ',Name,' fecha:', datetime.now())
                    
    with open(PathOUT2+NameRegistry,'w') as f:
        print('Se inicializa el registro de cubo de datos : ',NameRegistry,' fecha:', datetime.now())
        
    with open(PathOUT2+NameRegistryLog,'w') as f:
        print('Se inicializa el registro log de cubo de datos : ',NameRegistryLog,' fecha:', datetime.now())
        
    
    
def process_data_cube(PathIN,PathOUT):
    PathOUT1=PathOUT
    
    fileprint='prints_datacube.txt'
    NameRegistry='read_csv4d_datacube.dat'
    NameRegistryLog='registry_csv4d_datacube.log'
    NameRegistryGlobal='Header_Data.csv'
    
    while True:
        ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
        if len(ListFiles)>1:
            NameSorted=get_names_datacubes(ListFiles)
            NameSortedStart=[name for name in NameSorted]
            break
        time.sleep(3)
        
    print('name datacube: ',NameSortedStart)
    while True: 
        name=NameSortedStart[0]
        PathOUT=PathOUT1+name+'/'
        PathOUT2=PathOUT+'Data Cube/'
        ntf.make_dir(PathOUT2)
        
        ListFiles=get_files_for_datacube(PathIN,name)
        NewListFiles=ntf.get_new_files_with_data(ListFiles,PathOUT2+NameRegistry)
        
        if len(NewListFiles)>1:
            NewListFilesSorted=ntf.sorted_files_list_for_date(NewListFiles)
            Path_DataCube=PathOUT2+name+'.h5'
            if not os.path.exists(Path_DataCube):
                cubo_datos=DataCube(Path_DataCube)
                f = open(PathOUT2+fileprint, 'a')
                sys.stdout = f
                fileCSV4D=NewListFilesSorted[0]
                init_datacube(PathOUT,PathOUT2,fileCSV4D,NameRegistry,NameRegistryLog,NameRegistryGlobal,cubo_datos)
                f.close()
            try:
                cubo_datos
            except NameError:
                cubo_datos=DataCube(Path_DataCube)
                
            INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
            end=-1
            if name!=NameSortedStart[-1]:
                end=len(NewListFilesSorted)
                NameSortedStart.pop(0)
            else:
                ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
                NameSortedAux=get_names_datacubes(ListFiles)
                for n in NameSortedAux:
                    if n not in NameSorted:
                        NameSortedStart.append(n)
                
            for fileCSV4D in NewListFilesSorted[:end]:
                
                f = open(PathOUT2+fileprint, 'a')
                sys.stdout = f
                if NewListFilesSorted[-1]==fileCSV4D:
                    ntf.check_read_file(fileCSV4D)
                    
                
                ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
                ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
                save_historical_data(fileCSV4D,PathOUT2+name,INFO,cubo_datos)
                f.close()
                    
       
        time.sleep(3)

    
def update_dtm(fileCSV4D,EndDate,filename_dtm,INFO):
    
    _,_,_,n_cols,lim_axes,cell_size,_,_=INFO  
    x_min,_,y_min,_=lim_axes  
    if type(EndDate)!=type(str()):
        EndDate=ntu.datetime_to_str(EndDate)
    
    XY=get_XY_from_H5(filename_dtm) 
    with h5py.File(filename_dtm, 'a') as  hf:
        XY_Z=ntf.get_z_from_csv4d(fileCSV4D,5)
        ListZ=np.empty(len(XY),dtype=np.float32)
        ListZ[:]=np.nan
        dist=[]
        for x,y in XY_Z:
            i,j=ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size)
            index=j+i*n_cols
            ListZ[index]=XY_Z[(x,y)]
            xl,yl=XY[index,0],XY[index,1]
            dist.append(np.sqrt((x-xl)**2 + (y-yl)**2))
            
        dist_max=np.max(dist)
        dist_min=np.min(dist)
        
        cont=len(np.argwhere(~np.isnan(ListZ)))
        hf.create_dataset(EndDate, data=ListZ,dtype=np.float32)
        print('Fecha del archivo csv4d: ',EndDate,';total celdas guardadas: ',cont,'de',len(XY_Z),'; dist max y min: ',dist_max,dist_min,';fecha:', datetime.now())
        
        


def init_data_dtm(PathOUT,PathOUT2,fileCSV4D,NameRegistryLog,NameRegistryGlobal,filename_dtm):
    INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
    Name,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
                
    
    
    _,EndDate,_=ntf.get_info_from_csv4d(fileCSV4D)
    XY=create_list_XY(x_min,n_cols,y_min,n_rows,cell_size,5)
                    
    # se inicializa el archivo h5 
    with h5py.File(filename_dtm, 'w') as  hf:
        hf.create_dataset('X',shape=(len(XY),1), dtype=np.float64, data=XY[:,0])
        hf.create_dataset('Y',shape=(len(XY),1), dtype=np.float64, data=XY[:,1])
        # hf.create_dataset('X-Y',shape=(len(List_XY),1), dtype='S24', data=List_XY)
        print('Se inicializó el h5 como dtm : ',filename_dtm,'; fecha:', datetime.now())
                    
                    
        with open(PathOUT2+NameRegistryLog,'w') as f:
            print('Se inicializó el registro log de cubo de datos : ',NameRegistryLog,' fecha:', datetime.now())
                   
        update_dtm(fileCSV4D,EndDate,filename_dtm,INFO)
        ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
 
def process_dtm(PathIN,PathOUT):
    PathOUT1=PathOUT
    
    fileprint='prints_dtm.txt'
    NameRegistry='read_csv4d_dtm.dat'
    NameRegistryLog='registry_csv4d_dtm.log'
    NameRegistryGlobal='Header_Data.csv'
    
    while True:
        ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
        if len(ListFiles)>1:
            NameSorted=get_names_datacubes(ListFiles)
            NameSortedStart=[name for name in NameSorted]
            break
        time.sleep(3)
    
    intervalTime=(1,8,0) # day, hour, minutes
    while True: 
        name=NameSortedStart[0]
        PathOUT=PathOUT1+name+'/'
        PathOUT2=PathOUT+'Data_DTM/'
        ntf.make_dir(PathOUT2)
        
        
        ListFiles=get_files_for_datacube(PathIN,name)
        NewListFiles=ntf.get_new_files_with_data(ListFiles,PathOUT2+NameRegistry)
        
        if len(NewListFiles)>1 and os.path.exists(PathOUT+NameRegistryGlobal):
            NewListFilesSorted=ntf.sorted_files_list_for_date(NewListFiles)
            
            f = open(PathOUT2+fileprint, 'a')
            sys.stdout = f
            fileCSV4D=NewListFilesSorted[0]
            filename_dtm=PathOUT2+name+'_DTM'+'.h5'
            if not os.path.exists(filename_dtm):
                init_data_dtm(PathOUT,PathOUT2,fileCSV4D,NameRegistryLog,NameRegistryGlobal,filename_dtm)
            f.close()
               
            end=-1
            if name!=NameSortedStart[-1]:
                end=len(NewListFilesSorted)
                NameSortedStart.pop(0)
            else:
                ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
                NameSortedAux=get_names_datacubes(ListFiles)
                for n in NameSortedAux:
                    if n not in NameSorted:
                        NameSortedStart.append(n)
                  
            ListDates=get_dates_from_H5(filename_dtm)     
            lastDate=ListDates[-1] + ntu.delta_time(ListDates[-1], intervalTime)     
                    
            NewListFiles=ntf.get_files_for_time_interval(NewListFilesSorted,lastDate,intervalTime)
            for fileCSV4D in NewListFiles:
                f = open(PathOUT2+fileprint, 'a')
                sys.stdout = f
                if NewListFilesSorted[-1]==fileCSV4D:
                    ntf.check_read_file(fileCSV4D)
                _,EndDate,_=ntf.get_info_from_csv4d(fileCSV4D)
                
                # ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
                ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
                INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
                update_dtm(fileCSV4D,EndDate,filename_dtm,INFO)
                f.close()
        
       
        time.sleep(1)
    



def set_time_series(fileCSV4D,TS,Cond,TW,INFO,EndDate):
    
    data=ntf.get_xyzd_from_csv4d(fileCSV4D,5)
    n,m=TS.shape
    TW[0:-1]=TW[1:]
    TW[-1]=EndDate
    
    _,_,_,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    n,m=TS.shape
    
    TS[:,0:-1]=TS[:,1:]
    TS[:,-1]=np.nan
    desplazamiento={}
    
    dist=[]
    t1=time.time()
    for x,y,_,desp in data:
        i,j=ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size)
        index=j+i*n_cols
        TS[index,-1]=desp
        desplazamiento[index]=None
        # Cond[index]=len(np.argwhere(~np.isnan(TS[index,:])))
    
    # Se repite el valor anterior de aquellas celdas que no tienen datos
    indexs=np.argwhere(np.isnan(TS[:,-1]))
    TS[indexs,-1]=TS[indexs,-2]
    
    indexs=np.argwhere(~np.isnan(TS[:,-1]))
    Cond[indexs]=Cond[indexs]+1
    
    indexs=np.argwhere(np.isnan(TS[:,-1]))
    Cond[indexs]=Cond[indexs]-1
        
    cont=len(desplazamiento)
    Cond[Cond<0]=0
    Cond[Cond>m]=m
    
    # indexs=[i[0] for i in np.argwhere(Cond==m)]
    # if len(indexs)>0:
    #     for i in indexs:
    #         TS[i,:]=TS[i,:]-TS[i,0]
    # print('csv4d: ',EndDate,'celdas guardadas: ',cont,'de',len(data),'; dist max y min: ',dist_max,dist_min,'; celdas con 144 reg: ',len(np.argwhere(Cond==m)),';fecha:', datetime.now())
    print('csv4d: ',EndDate,'celdas guardadas: ',cont,'de',len(data),'; celdas con 144 reg: ',len(np.argwhere(Cond==m)),';fecha:', datetime.now())
    return TS,Cond,TW


def get_data_times_series(PathOUT_filename):
    filenpz=np.load(PathOUT_filename,allow_pickle=True)
    # Acceso a los arreglos almacenados en el archivo .npz
    Cond=filenpz['arr_0']
    XY=filenpz['arr_1']
    TS=filenpz['arr_2']
    TW=filenpz['arr_3']
    filenpz.close()      
    return Cond,XY,TS,TW

def update_times_series(fileCSV4D,PathOUT2,INFO,NameTS):
    
    StartDate1,EndDate1,_=ntf.get_info_from_csv4d(fileCSV4D)

    pahtfilename=ntf.get_all_files(PathOUT2,ext='.npz')
    if len(pahtfilename)==0:
        print('Error: No existe un arhivo .npz en ',PathOUT2,' fecha:', datetime.now())
    elif len(pahtfilename)>1:
        print('Error: Existen más de un arhivo .npz en ',PathOUT2,' fecha:', datetime.now())
    else:
        
        oldfilename,_ = os.path.splitext(os.path.basename(pahtfilename[0]))

        Cond,XY,TS,TW=get_data_times_series(PathOUT2+ oldfilename+'.npz')
        os.remove(PathOUT2+oldfilename+'.npz')
        n,m=TS.shape
        TS, Cond, TW = set_time_series(fileCSV4D,TS,Cond,TW,INFO,EndDate1)
        
        StartDate=ntu.datetime_to_str(TW[0],fmt='%Y_%b_%d-%H_%M')
        EndDate=ntu.datetime_to_str(TW[-1],fmt='%Y_%b_%d-%H_%M')
        newfilename=StartDate+'_to_'+EndDate
        
       
        np.savez(PathOUT2+newfilename+'.npz',Cond,XY,TS,TW)
        save_name_timeserie(PathOUT2+NameTS,newfilename)
        
        return PathOUT2+newfilename+'.npz'
        
        
#        TS[:,0:-1]=TS[:,1:]
#        for i,xy in enumerate(XY):
#            if xy in data:
#                if abs(data[xy]-TS[i,-2])<=4.3:
#                    TS[i,-1]=data[xy]-TS[i,-2]
#                TS[i,:]=TS[i,:]-TS[i,0]
#            else:
#                TS[i,-1]=TS[i,-2]
                
def get_time_series_eval(path_npz):
    Cond,XY,TS,TW=get_data_times_series(path_npz)
    n,m=TS.shape
    indexs=[i[0] for i in np.argwhere(Cond==m)]
    XY=XY[indexs]
    TS=TS[indexs,:]
    for i in range(len(TS)):
        TS[i]=TS[i]-TS[i,0]
    return XY,TS,TW

def init_time_serie(PathOUT,PathOUT2,NameRegistry,NameRegistryLog,INFO,NameTS,NewListFilesSorted):
    t1=time.time()
    _,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    
    
    XY=create_list_XY(x_min,n_cols,y_min,n_rows,cell_size,5)
    Cond=np.array(np.zeros(len(XY),dtype=np.int32))
    TS=np.empty((len(XY),144), dtype=np.float32)
    TS[:,:]=np.nan
    TW=np.empty(144, dtype=datetime)
    
    
                
    #los arreglos almcanados son el siguiente orden : Condicion, 
    # coordendas, time series y ventana temporal, cuyos accesos son mediante
    # 'arr_0','arr_1','arr_2','arr_3'
    
    print('Tiempo en inicializar y guardar npz:',time.time()-t1,' fecha:', datetime.now())
    with open(PathOUT2+NameRegistry,'w') as f:
        print('Se inicializa el registro de csv4d para la TS',' fecha:', datetime.now())
        pass
                    
    with open(PathOUT2+NameRegistryLog,'w') as f:
        print('Se inicializa el registro log de csv4d para la TS',' fecha:', datetime.now())
        pass    
    
    with open(PathOUT2+NameTS,'w') as f:
        print('Se inicializa el registro log de nombres para la TS',' fecha:', datetime.now())
        pass
    
    if len(NewListFilesSorted)>144:
       NewList=NewListFilesSorted[:144]
    else:
       NewList=NewListFilesSorted[:-1]
    
    for fileCSV4D in NewList:
        
        _,EndDate,_=ntf.get_info_from_csv4d(fileCSV4D)
        TS, Cond, TW = set_time_series(fileCSV4D,TS,Cond,TW,INFO,EndDate)
        
        ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
        ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
        
        
    _,StartDate,_=ntf.get_info_from_csv4d(NewList[0])
    _,EndDate,_=ntf.get_info_from_csv4d(NewList[-1])
    
    StartDate=ntu.datetime_to_str(StartDate,fmt='%Y_%b_%d-%H_%M')
    EndDate=ntu.datetime_to_str(EndDate,fmt='%Y_%b_%d-%H_%M')
    
    newfilename=StartDate+'_to_'+EndDate
    np.savez(PathOUT2+newfilename+'.npz',Cond,XY,TS,TW)
    save_name_timeserie(PathOUT2+NameTS,newfilename)

def save_name_timeserie(NameTS,filename):
    with open(NameTS,'a') as f:
        f.write('%s;%s\n'%(filename,datetime.now()))
        
def process_time_series(PathIN,PathOUT,PathModel,NameModel):
    PathOUT1=PathOUT
    
    fileprint='prints_ts.txt'
    NameRegistry='read_csv4d_ts.dat'
    NameRegistryGlobal='Header_Data.csv'
    NameRegistryLog='registry_ts.log'
    NameTS='name_registry_ts.log'
    
    while True:
        ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
        if len(ListFiles)>1:
            NameSorted=get_names_datacubes(ListFiles)
            NameSortedStart=[name for name in NameSorted]
            break
        time.sleep(3)
        
        
    NameSortedStart=[NameSortedStart[-1]]
    name=NameSortedStart[0]
    PathOUT=PathOUT1+name+'/'
    PathOUT2=PathOUT+'temp_Time_Serie/'
    
    if os.path.exists(PathOUT+NameRegistryGlobal):
        t1=time.time()
        INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
        DataColors_list=create_colors_map(INFO)
        print('tiempo en crear colores: ',time.time()-t1)
        if ntf.exist_file_with_ext(PathOUT2,'.npz'):
            t1=time.time()
            path_npz=ntf.get_all_files(PathOUT,ext='.npz')[0]
            X,Y,Z,PM=create_matrixs_XYZ_PM(path_npz,INFO)
            print('tiempo en crear matrices X,Y,Z y PM: ',time.time()-t1)
            t1=time.time()
            ListFiles=get_files_for_datacube(PathIN,name)
            fileCSV4D=ntf.sorted_files_list_for_date(ListFiles)[-1]
            Z=set_values_Z(INFO,fileCSV4D,Z)
            print('tiempo de llenar la matriz Z: ',time.time()-t1)
            t1=time.time()
            process_evaluation(PathOUT,PathModel,NameModel,path_npz,INFO,DataColors_list,X,Y,Z,PM)
            print('tiempo de proceso de evaluación: ',time.time()-t1)

        
    while True: 
        name=NameSortedStart[0]
        PathOUT=PathOUT1+name+'/'
        PathOUT2=PathOUT+'temp_Time_Serie/'
        ntf.make_dir(PathOUT2)
        
        ListFiles=get_files_for_datacube(PathIN,name)
        NewListFiles=ntf.get_new_files_with_data(ListFiles,PathOUT2+NameRegistry)
        
        if len(NewListFiles)>0 and os.path.exists(PathOUT+NameRegistryGlobal):
            
            NewListFilesSorted=ntf.sorted_files_list_for_date(NewListFiles)
            
            if not ntf.exist_file_with_ext(PathOUT2,'.npz'):
                f = open(PathOUT2+fileprint, 'a')
                sys.stdout = f
                INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
                init_time_serie(PathOUT,PathOUT2,NameRegistry,NameRegistryLog,INFO,NameTS,NewListFilesSorted)
                DataColors_list=create_colors_map(INFO)
                path_npz=ntf.get_all_files(PathOUT,ext='.npz')[0]
                X,Y,Z,PM=create_matrixs_XYZ_PM(path_npz,INFO)
                f.close()
            else:
                
                if name!=NameSortedStart[-1]:
                    NameSortedStart.pop(0)
                else:
                    ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
                    NameSortedAux=get_names_datacubes(ListFiles)
                    for n in NameSortedAux:
                        if n not in NameSorted:
                            NameSortedStart.append(n)
                    
                for fileCSV4D in NewListFilesSorted:
                    
                    f = open(PathOUT2+fileprint, 'a')
                    sys.stdout = f
                    
                    if NewListFilesSorted[-1]==fileCSV4D:
                        ntf.check_read_file(fileCSV4D)
                    
                    path_npz=update_times_series(fileCSV4D,PathOUT2,INFO,NameTS)
                    ntf.update_registry_log(PathOUT2+NameRegistryLog,fileCSV4D)
                    ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
                    
                    Z=set_values_Z(INFO,fileCSV4D,Z)
                    process_evaluation(PathOUT,PathModel,NameModel,path_npz,INFO,DataColors_list,X,Y,Z,PM)
                    f.close()
                    
     
        time.sleep(1)

def create_colors_map(INFO):
    _,_,n_rows,n_cols,_,_,_,_=INFO
    
    interval=[0.0,0.5,0.7,0.9,1.001]
    n_pal=len(interval)-1
    cmap = mpl.colors.ListedColormap(['limegreen','yellow','orange', 'red'])
    norm = mpl.colors.BoundaryNorm(interval, cmap.N)
    cmap1 = cmap(np.linspace(0,1,n_pal))
    white=[1.,1.,1.,1.]
    color_map=np.array([[white for _ in range(n_cols)] for _ in range(n_rows)])
    
    return norm,cmap,cmap1,color_map,interval

def create_matrixs_XYZ_PM(path_npz,INFO):
    _,XY,_,_=get_data_times_series(path_npz)
    _,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    
    PM=np.empty((n_rows,n_cols), dtype=np.float32)
    PM[:,:]=np.nan
    
    X=np.empty((n_rows,n_cols), dtype=np.float32)
    Y=np.empty((n_rows,n_cols), dtype=np.float32)
    Z=np.empty((n_rows,n_cols), dtype=np.float32)
    Z[:,:]=np.nan
    
    t1=time.time()
    
    
    IJ=np.array([ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size) for x,y in XY],dtype=np.int32)
    X[IJ[:,0],IJ[:,1]]=XY[:,0]
    Y[IJ[:,0],IJ[:,1]]=XY[:,1]
    # for xy  in XY:
    #     x,y=strXY_to_floatXY(xy,5)
    #     i,j=get_ij_from_xy(x,y,x_min,y_min,cell_size)
    #     X[i,j]=x
    #     Y[i,j]=y
    print('Tiempo en llenar las matrices X e Y: ',time.time()-t1)
    return X,Y,Z,PM

def set_values_Z(INFO,fileCSV4D,Z):
    _,_,_,_,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    dataZ=ntf.get_xyzd_from_csv4d(fileCSV4D,5)
    for x,y,z,_ in dataZ:
        i,j=ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size)
        Z[i,j]=z
    return Z

 
    
      
def process_evaluation(PathOUT,PathModel,NameModel,path_npz,INFO,DataColors_list,X,Y,Z,PM):
    
    
    PathOUT2=PathOUT+'Results/'+NameModel+'/'
    PathImg=PathOUT2+'Images/'
    PathPM=PathOUT2+'Prob_Matrix/'
    PathAC=PathOUT2+'Adj_Cells/'
    ntf.make_dir(PathImg)
    ntf.make_dir(PathPM)
    ntf.make_dir(PathAC)
    
    #Se obtiene las caracteristicas de las celdas del radar
    ProyectName,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    
    #Se carga el modelo 
    model = load_model(PathModel+NameModel+'.h5')
    
    #Caracteristicas de colores
    
    norm,cmap,cmap1,CellsColors,interval=DataColors_list

    CellsColors[:,:]=[1.,1.,1.,1.]
    t1=time.time()
    XY_eval,TS_eval,TW=get_time_series_eval(path_npz)
    # TS_eval[:,-1]=TS_eval[:,-2]
    print('shape TS_eval: ',TS_eval.shape)
   
    print('Tiempo en obtener ts para eval: ', time.time()-t1)
    t1=time.time()
    results=model.predict(TS_eval)
    print('tiempo en predecir la cantidad de %d ts: %.1f '%(len(TS_eval),time.time()-t1))
    Prob=results[:,1]
    #Se completa la matriz de probabilidades
    Contador_ClasTS=len(cmap1)*[0]
    
    t1=time.time()
    ColorNorm=norm(Prob)
    IJ=np.array([ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size) for x,y in XY_eval],dtype=np.int32)
    PM[:,:]=0
    PM[IJ[:,0],IJ[:,1]]=Prob
    
    
    
    ii=ProyectName.upper().find('IBIS')
    NameRadar=ProyectName[ii:ii+6].replace('_',' ').replace('-',' ')
    
    # azimut,elev=
    FechaHora=ntu.datetime_to_str(TW[-1])
    title='Radar '+NameRadar+'\n'+'Fecha: '+FechaHora+'\n'+NameModel
    
    #Evaluar solo una zona con time serie
    Zona=False
    if Zona:
        xc,yc=58750.0,91250.0
        radio = 250
        ic,jc=ntu.get_ij_from_xy(xc,yc,x_min,y_min,cell_size)
        Ncells=int(np.round(radio/cell_size))+1
        ij_tupla=(ic,jc)
        indexs=[i for i,xy in enumerate(XY_eval) if abs(xy[0]-xc)<=radio and  abs(xy[1]-yc)<=radio]
        ts_min=np.nanmin(TS_eval)-2
        ts_max=np.nanmax(TS_eval)+2
        fig,axs,ax2D,ax3D=ntg.create_graph_TS_2D_3D(ts_min,ts_max,TW,title)
        t1=time.time()
        for index,ij,ts in zip(ColorNorm[indexs],IJ[indexs],TS_eval[indexs]):
            i,j=ij[0],ij[1]
            CellsColors[i,j]=cmap1[index]
            Contador_ClasTS[index]=Contador_ClasTS[index]+1
            pos_axes=len(cmap1) - 1 - index
            axs[pos_axes,0].plot(TW,ts,color=cmap1[index])
    
        print('tiempo en asignar valores a MP, CellsColor y graficar ts : ', time.time()-t1)
        axs[len(cmap1) - 1,0].set_title('N° CNR  : %d'%(Contador_ClasTS[0]))
    
        for i in range(0,len(cmap1)-1):
            axs[i,0].set_title('N° CR  : %d'%(Contador_ClasTS[len(cmap1)-1-i]))
            
        t1=time.time()
        ntg.init_point_graph_2D(ax2D,X,Y,Z)
        ntg.Graph_Point2D(fig,ax2D,XY_eval,Prob,cmap,norm,interval,indexs)
        print('Tiempo en graficar 2D: ',time.time()-t1)
        t1=time.time()
        
        ntg.Graph_Point3D(fig,ax3D,X,Y,Z,CellsColors,norm,cmap,cmap1,ij_tupla,Ncells=Ncells)
        print('Tiempo en graficar 3D: ',time.time()-t1)
    else:
        
        # fig,ax2D,ax3D=ntg.create_graph_2D_3D(title)
        fig2D, ax2D=ntg.create_graph_2D(title)
        fig3D, ax3D=ntg.create_graph_3D(title)
        t1=time.time()
        for index,ij in zip(ColorNorm,IJ):
            i,j=ij[0],ij[1]
            CellsColors[i,j]=cmap1[index]
            Contador_ClasTS[index]=Contador_ClasTS[index]+1

        print('tiempo en asignar valores a MP, CellsColor y graficar ts : ', time.time()-t1)
        
        t1=time.time()
        ntg.init_point_graph_2D(ax2D,X,Y,Z)
        ntg.Graph_Point2D(fig2D,ax2D,XY_eval,Prob,cmap,norm,interval)
        print('Tiempo en graficar 2D: ',time.time()-t1)
        t1=time.time()
        
        ntg.Graph_Point3D(fig3D,ax3D,X,Y,Z,CellsColors,norm,cmap,cmap1)
        print('Tiempo en graficar 3D: ',time.time()-t1)
     
    
    
    
    
    NameImg=ntu.datetime_to_str(TW[-1]).replace('-','').replace(':','_').replace(' ','_')
    # plt.tight_layout()
    if Zona:
        t1=time.time()
        fig.savefig(PathImg+'TS_2D_3D_'+NameImg+'.png',dpi=500)
        print('Tiempo en guardar TS_2D_3D: ',time.time()-t1)
        del fig, axs,ax2D, ax3D
    else:
        t1=time.time()
        fig2D.savefig(PathImg+'2D_'+NameImg+'.png',dpi=400)
        print('Tiempo en guardar 2D: ',time.time()-t1)
        t1=time.time()
        fig3D.savefig(PathImg+'3D_'+NameImg+'.png',dpi=500)
        print('Tiempo en guardar 3D: ',time.time()-t1)
    #    fig.canvas.mpl_connect('button_press_event', OnSelect)
        del fig2D,fig3D,ax2D, ax3D
    
    plt.close('all')
    
    
    
    
    
    #######################################################################
        
    #Se aplica el criterio de las celdas adyacentes
    
    # umbral=0.5
    # len_cluster=1

    # Mask=create_mask(A,umbral=umbral)
    # All_Claster=do_claster(A,Mask,r=1,len_cluster=len_cluster)
    
    #######################################################################
    
    
    
    
    
    

    
def test_graph():
    
    import matplotlib.pylab as plt
    import matplotlib as mpl
    from mpl_toolkits.mplot3d import Axes3D 
    
    PathIN="C:/EMT/Data_CSV4D/Nueva Data_CSV4D/IBIS_2/"
    csv4d='MASTER_MLP_24122019_IBIS-2_MLP_24122019_Displacement_Map_from_19_Dec_24-00_00_to_20_Jan_07-02_31.csv4d'
    t1=time.time()
    XYZ=ntf.get_xyz_from_csv4d(PathIN+csv4d)
    print('Tiempo en extraer datos csv4d: ', time.time()-t1)
    
    t1=time.time()
    data=ntf.get_xyzd_from_csv4d(PathIN+csv4d,5)
    print('Tiempo en extraer desplazamiento csv4d: ', time.time()-t1)
    
    t1=time.time()
    data=ntf.get_xyzd_from_csv4d(PathIN+csv4d,5)
    print('Tiempo en extraer desplazamiento 2 csv4d: ', time.time()-t1)
    # fig= plt.figure(1,figsize=(14,8))
    # ax = fig.gca(projection='3d')
    
    _,info=create_all_cells(PathIN+csv4d,5,NumCell=1)
    m,n,x_min,x_max,y_min,y_max,cell_size,dist_x,dist_y=info
    
    X=np.empty((n,m))
    Y=np.empty((n,m))
    Z=np.empty((n,m))
    X[:,:]=np.nan
    Y[:,:]=np.nan
    Z[:,:]=np.nan
    
    t1=time.time()
    IJ=np.array([ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size) for x,y,z in XYZ],dtype=np.int32)
    print('Tiempo en crear ij  : ', time.time()-t1)
    t1=time.time()
    X[IJ[:,0],IJ[:,1]]=XYZ[:,0]
    Y[IJ[:,0],IJ[:,1]]=XYZ[:,1]
    Z[IJ[:,0],IJ[:,1]]=XYZ[:,2]
    # for ij,xyz in zip(IJ,XYZ):
    #     X[ij[0],ij[1]]=xyz[0]
    #     Y[ij[0],ij[1]]=xyz[1]
    #     Z[ij[0],ij[1]]=xyz[2]
    print('Tiempo en asignar valores a X,Y y Z : ', time.time()-t1)
    return 0
    cmap = mpl.colors.ListedColormap(['limegreen','red'])
    
    # ax.plot_surface(X, Y, Z,cmap=cmap ,cstride=1,rstride=1,linewidth=0.2, antialiased=True,shade=True)
    # X,Y,Z=XYZ[:,0],XYZ[:,1],XYZ[:,2]
    prob=np.random.rand(len(X))
    
    
    interval=[0.0,0.5,0.7,0.9,1.00]
    n_pal=len(interval)-1
    cmap = mpl.colors.ListedColormap(['limegreen','yellow','orange', 'red'])
    cmap1 = cmap(np.linspace(0,1,n_pal))
    norm = mpl.colors.BoundaryNorm(interval, cmap.N)
    colores=[cmap1[norm(p)] for p in prob]
    t1=time.time()
    # ax.plot_trisurf(X, Y, Z, cmap=mpl.colors.ListedColormap(colores),linewidth=0.2, antialiased=True)
    print('tiempo en graficar trisurf: ',time.time()-t1)
    t1=time.time()
    # fig.savefig('3DD.png',dpi=500)
    print('tiempo en guardar trisurf: ',time.time()-t1)
    # ax.plot_trisurf(X, Y, Z, linewidth=0.2)
    
    # fig2, ax = plt.subplots(figsize=(20,14))
    # ax.grid(linewidth=1)
    # ax.set_axisbelow(True)
    # ax.scatter(X,Y,marker='s',c='limegreen',s=0.25,linewidths=0.25,alpha=0.8)
    # theta=np.linspace(0,2*np.pi,1000)
    # r1=30
    # r2=60
    # r3=100
    # ax.plot(X[11500] + r1*np.cos(theta), Y[11500] +  r1*np.sin(theta),'--k',alpha=0.8)
    # ax.plot(X[11500] + r2*np.cos(theta), Y[11500] +  r2*np.sin(theta),'--k',alpha=0.8)
    # ax.plot(X[11500] + r3*np.cos(theta), Y[11500] +  r3*np.sin(theta),'--k',alpha=0.8)
    # ax.set_aspect('equal') 
    
    # # plt.plot(X,Y,'ob',ms=0.01)
    # fig2.savefig('2D.png',dpi=1500)
    # print('tiempo en generara y guardar imagenes 2D y 3D: ',time.time()-t1)
    
    
    
    # _,info=create_all_cells(PathIN+csv4d,5,NumCell=1)
    # m,n,x_min,x_max,y_min,y_max,cell_size,dist_x,dist_y=info
    
    # X=np.empty((n,m))
    # Y=np.empty((n,m))
    # Z=np.empty((n,m))
    # X[:,:]=np.nan
    # Y[:,:]=np.nan
    # Z[:,:]=np.nan
    
    # for x,y,z in XYZ:
    #     i,j=get_ij_from_xy(x,y,x_min,y_min,cell_size)
    #     X[i,j]=x
    #     Y[i,j]=y
    #     Z[i,j]=z
            
    
    from mayavi import mlab
    
    # X, Y = np.meshgrid( , )
    # Z = np.zeros_like(X)
    # im = np.sin(X/10 + Y/100)
    
    #fig = plt.figure()
    #x = fig.add_subplot(111, projection='3d')
    
    # src = mlab.pipeline.array2d_source(X,Y,Z)
    # warp = mlab.pipeline.warp_scalar(src)
    # normals = mlab.pipeline.poly_data_normals(warp)
    # surf = mlab.pipeline.surface(normals)
    # mlab.show()
    
    

    # x, y = np.mgrid[-7.:7.05:0.1, -5.:5.05:0.05]
    n,m=Z.shape
    prob=np.random.rand(n,m)
    cellcolors=np.array([])
    colores=[cmap1[norm(prob[i,j])] for j in range(m) for i in range(n)]
    s = mlab.surf(Z,colormap=cmap(norm(prob)))
    #cs = contour_surf(x, y, f, contour_z=0)
    
def f(x, y):
        sin, cos = np.sin, np.cos
        return sin(x + y) + sin(2 * x - y) + cos(3 * x + 4 * y)



def test_h5():
     Path=r"C:\Users\JOliva\Documents\DataOUT\IBIS_2\MASTER_MLP_24122019_IBIS-2_MLP_24122019\Data Cube\MASTER_MLP_24122019_IBIS-2_MLP_24122019.h5"
     t1=time.time()
     L=get_XY_from_H5(Path)
     print('Tiempo en obtener XY de h5: ',time.time()-t1)
     print(L)
if __name__=='__main__':
    
    
    # test_h5()
    # import matplotlib.pylab as plt
    
    # csv4d='MASTER_MLP_24122019_IBIS-1_MLP_24122019_Displacement_Map_from_19_Dec_24-00_00_to_19_Dec_24-14_25.csv4d'
    if False:
        PathIN="C:/EMT/Data_CSV4D/Nueva Data_CSV4D/IBIS_2/"
        PathOUT="C:/Users/joliva/Documents/DataOUT/IBIS_2/"
        PathModel="C:/Users/joliva/Documents/modelos/Modelos por CD/MMLP_Media_EV_1-2-3_Nobalanced/"
    else:
        PathIN="D:/EMT/Data_CSV4D/Nueva Data_CSV4D/IBIS_2/"
        PathOUT="C:/Users/JOliva/Documents/DataOUT/IBIS_2/"
        PathModel="C:/Users/JOliva/Documents/modelos/Modelos por CD/MMLP_Media_EV_1-2-3_Nobalanced/"
    NameModel="MMLP_Media_EV_1-2-3_Nobalanced"
    
    opc=2
    
    
    # data=placement_from_csv4d(PathIN+csv4d)
    # xy=np.array([strXY_to_floatXY(xy,5) for xy in list(data.keys())])
    # plt.plot(xy[:,0],xy[:,1],'go')
    
    
    # opc=int(sys.argv[1])
    # PathIN=sys.argv[2]
    # PathOUT=sys.argv[3]
    # PathModel=sys.argv[4get_des]
    # NameModel=sys.argv[5]
    
    ntf.make_dir(PathOUT)
    print(PathIN,PathOUT)
    
    # time.sleep(10)
    # test_graph()

    print('Escoger uno de los procesos a realizar:')
    print('[1] Generar cubo de datos')
    print('[2] Generar DTM')
    print('[3] Generar Time Serie Temporal')
    print('opcion seleccionada: ',opc)
    if opc==1:
        print('opcion 1')
        process_data_cube(PathIN,PathOUT)
    elif opc==2:
        print('opcion 2')
        process_dtm(PathIN,PathOUT)
    elif opc==3:
        print('opcion 3')
        process_time_series(PathIN,PathOUT,PathModel,NameModel)
    # time.sleep(15)
    
#    process_time_series(PathIN,PathOUT)
    # process_dtm(PathIN,PathOUT)
    # process_evaluation(PathIN,PathOUT)
#    test_read()
#    test_Pto_cercano()
#    test_read_write_file()
#    np.random.seed(0)
#    n=5000
#    X=np.linspace(0,1,n)
#    Y=np.linspace(0,1,n)
        
    
    # PathOUT='C:/Users/joliva/Documents/DataOUT/IBIS_4/MASTER_MLP_24122019_IBIS-2_MLP_24122019/'  
    # PathOUT1=PathOUT+'Data Cube/'
    # # PathOUT2='D:/DataOUT_Radar/IBIS_1/Data DTM/'
    
    # NameRegistryGlobal='Header_Data.csv'
    
    # INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
    # Name,_,_,n_cols,lim_axes,cell_size,_,_=INFO
    # x_min,_,y_min,_=lim_axes
    
    # fileCD=Name
    # # fileDTM=Name+'_DTM'
    # XY_CD=get_XY_from_H5(PathOUT1+fileCD+'.h5')
    # with open('C:/Users/joliva/Documents/ptos_xy.txt','w') as f:
    #     f.write('X;Y\n')
    #     for xy in XY_CD:
    #         x,y=strXY_to_floatXY(xy,5)
    #         f.write('%s;%s\n'%(x,y))
    # XY_DTM=get_XY_from_H5(PathOUT2+fileDTM+'.h5')
    
    # cont=0
    # for xy1,xy2 in zip(XY_CD,XY_DTM):
    #     if xy1==xy2:
    #         cont=cont+1
    
    
    # for i,xy in enumerate(XY):
    #     index=get_pos_for_XY(xy,x_min,y_min,cell_size,n_cols,5)
    #     if  abs(i-index)>0:
    #         print(xy,i,index,abs(i-index))
    #         # break
        
#    XY=np.array(['%.5f-%.5f'%(x,y) for x in X for y in Y],dtype=np.str)
##    print(XY[:10])
##    dic_XY={xy: 0 for xy in XY}
##    for i,xy in enumerate(dic_XY):
##        print(xy)
##        if i>10: break
#    
#    np.random.seed(0)
#    v= '%.5f-%.5f'%(np.random.rand(),np.random.rand())
#    v='0.00000-0.00917'
#    t1=time.time()
#    if v in XY:
#        print('EL valor esta dentro del arreglo')
#    else:
#        print('El valor NO esta dentro del arreglo')
#    print('El tiempo de buscar es: ',time.time()-t1)
#    
#    
#    t1=time.time()
#    if v in dic_XY:
#        print('EL valor esta dentro del arreglo')
#    else:
#        print('El valor NO esta dentro del arreglo')
#    print('El tiempo de buscar es: ',time.time()-t1)