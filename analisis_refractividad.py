# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 10:39:03 2020

@author: joliva
"""

#Analsis de refractividad

import new_tool_utility as ntu
import new_tool_files as ntf
import new_tool_graphic as ntg
import tool_cluster as tc

import numpy as np
import datetime as dt
import numba
import time 
import gc

# PathDirCSV4D=r'C:\piloto\tool_storage'


def IBIS_property(numIBIS):
    if numIBIS==2:
        n_cols=1202
        n_rows=1202
        x_min=56380.92215
        y_min=88637.04205
        cell_size=3.30210
    if numIBIS==3:
        n_cols=1202
        n_rows=1203
        x_min=57411.17735
        y_min=89152.16965
        cell_size=3.30210
        
    return n_cols,n_rows,x_min,y_min,cell_size


    

def matched_csv4d_and_npy(PathDirCSV4D,PathDirIBISCSV4D,PathDirNPY,StartDate,EndDate):
    
    CSV4D_IBIS_list=ntf.sorted_files_for_date(PathDirIBISCSV4D,ext='csv4d',StartDate=StartDate,EndDate=EndDate)

    CSV4D_list=ntf.sorted_files_for_date(PathDirCSV4D,ext='csv4d',StartDate=StartDate,EndDate=EndDate)
    
    NPY_list=ntf.get_all_files(PathDirNPY,ext='npy')
    
    DiccIBIS={ntf.get_info_from_csv4d(fileCSV4D)[1]: [fileCSV4D] for fileCSV4D in CSV4D_IBIS_list}
    
    for fileCSV4D in CSV4D_list:
        EndDate=ntf.get_info_from_csv4d(fileCSV4D)[1]
        if EndDate in DiccIBIS:
            DiccIBIS[EndDate].append(fileCSV4D)
    
    
    Dicc_filter={}
    for file_npy in NPY_list:
        EndDate=ntf.get_dates_from_pathnpy(file_npy)
        if EndDate in DiccIBIS:
            if len(DiccIBIS[EndDate])>1:
                DiccIBIS[EndDate].append(file_npy)
                Dicc_filter[EndDate]=DiccIBIS[EndDate]
    
    return Dicc_filter


def get_xyzd_from_csv4d(fileCSV4D,k,sep=';'):
    
    array=np.genfromtxt(fileCSV4D,delimiter=sep,skip_header=1,usecols=(1,2,3,4),dtype=np.float64)
    # array=np.round(array,k)
    
    return array         

# def get_IJ_from_XY(X,Y,x_min,y_min,cell_size):
#     I=np.round(((Y-y_min)/cell_size))
#     J=np.round(((X-x_min)/cell_size))
#     return np.array(list(map(int,I))),np.array(list(map(int,J)))
    
def do_matrix_level_from_csv4d_colors(Data_array,ArrayIBIS,PM,n_rows,n_cols,x_min,y_min,cell_size,norm,cmap1):
    Mask=np.empty((n_rows,n_cols), dtype=np.bool)
    
    A=np.empty((n_rows,n_cols), dtype=np.float16)
    A2=np.empty((n_rows,n_cols), dtype=np.float16)
    # Z=np.empty((n_rows,n_cols), dtype=np.float16)
    A[:,:]=np.nan
    A2[:,:]=np.nan
    # Z[:,:]=np.nan
    Mask[:,:]=False
    colors=[]
    colors2=[]
    t1=time.time()
    
    I,J=ntu.get_IJ_from_XY(ArrayIBIS[:,0],ArrayIBIS[:,1],x_min,y_min,cell_size)
    Mask[I,J]=True
    # A2[I,J]=norm(PM[I,J])
   
    # for i,j in zip(I,J):
    #     # Z[i,j]=z
    #     # A2[i,j]=norm(PM[i,j])
    #     colors2.append(cmap1[int(A2[i,j])])
    # print('tiempo en crear mascara, asignar valores A2 y colors2: ',time.time()-t1)
            
    x_max=x_min+cell_size*n_cols
    y_max=y_min+cell_size*n_rows
    
    t1=time.time()
    XYZL=np.array([(x,y,z,level) for x,y,z,level in Data_array if (x_min <= x< x_max-1 and y_min <= y< y_max-1)])
    # print('tiempo en crear XYZL : ',time.time()-t1)
    
    t1=time.time()
    IJ_I=np.array([ntu.get_ij_from_xy(v[0],v[1],x_min,y_min,cell_size) + (i,)  for i,v in enumerate(XYZL) if Mask[ntu.get_ij_from_xy(v[0],v[1],x_min,y_min,cell_size)]] )

    # print('tiempo en crear IJ_I : ',time.time()-t1)

    t1=time.time()
    XY=[(XYZL[index,0],XYZL[index,1]) for _,_,index in IJ_I]
    A[IJ_I[:,0],IJ_I[:,1]]=[XYZL[index,3]-1 for _,_,index in IJ_I]
    A2[IJ_I[:,0],IJ_I[:,1]]=norm(PM[IJ_I[:,0],IJ_I[:,1]])
    colors=[cmap1[int(A[i,j])] for i,j,_ in IJ_I]
    colors2=[cmap1[int(A2[i,j])] for i,j,_ in IJ_I]
    # for i,j,index in IJ_I:
    #     # XY.append((XYZL[index,0],XYZL[index,1]))
    #     A[i,j]=XYZL[index,3]-1
    #     A2[i,j]=norm(PM[i,j])
    #     colors.append(cmap1[int(A[i,j])])
    #     colors2.append(cmap1[int(A2[i,j])])
    colors,colors2=np.array(colors),np.array(colors2)
    # print('tiempo en crear A y colors : ',time.time()-t1)
    return colors,colors2,A,A2,np.array(XY)


def count_level(A,level_list,umbral=3):
    counts=[]
    for level in level_list:
        counts.append(len(np.argwhere(A==level)))
    cluster1=tc.do_cluster(A,umbral=umbral,len_min_cluster=4)
    cluster2=tc.do_cluster(A,umbral=umbral,len_min_cluster=9)
    counts.append(len(cluster1))
    counts.append(len(cluster2))
    return counts

def main_data_graph2D(numIBIS,NameModel):
    pathdir='Data_IBIS%s/%s/'%(numIBIS,NameModel)
    pathdirImg='Data_IBIS%s/%s/img/'%(numIBIS,NameModel)
    ntf.make_dir(pathdirImg)
    PathDirCSV4D=r'C:\EMT\Data_CSV4D\TEST\Con Refractividad'+'/'
    PathDirIBISCSV4D=r'C:\EMT\Data_CSV4D\IBIS_'+str(numIBIS)+'/'
    
    
    with open(pathdir+'datos_couts_level_IBIS_'+str(numIBIS)+'.csv','a') as f:
        
        header=['fecha','N','N_L0','N_L1','N_L2','N_L3','G40','G100','N2','N2_L0','N2_L1','N2_L2','N2_L3','G40','G100']
        StartDate=dt.datetime(2020,2,5,17,39)
        f.write('%s\n'%(','.join(map(str,header))))
        EndDate=dt.datetime.now()
        
        if numIBIS==2:
            PathDirNPY=r'C:\EMT\Data_OUT\IBIS_2\MASTER_MLP_18012020_IBIS-2_MLP_24122019\Results' + '/' + NameModel+'\Prob_Matrix'+'/'
        if numIBIS==3:
            PathDirNPY=r'C:\EMT\Data_OUT\IBIS_3\MASTER_MLP_18012020_IBIS-3_MLP_24122019\Results' + '/' + NameModel+'\Prob_Matrix'+'/'
        
        umbrals_list=[0.0, 0.5, 0.7, 0.9, 1.01]

        levels=[0,1,2,3]
        colors_list=['lime','yellow','red','blue']
        cmap,cmap1,norm=ntg.get_colors_map(umbrals_list,colors_list)
        
        n_cols,n_rows,x_min,y_min,cell_size=IBIS_property(numIBIS)
        
        
        Dicc_filter=matched_csv4d_and_npy(PathDirCSV4D,PathDirIBISCSV4D,PathDirNPY,StartDate,EndDate)
        
        # data=[]
    
        
        
        
        figsize=(14,14)
        fig1, ax2D1=ntg.create_graph_2D('',figsize=figsize)
        fig2, ax2D2=ntg.create_graph_2D('',figsize=figsize)
        
        for date in Dicc_filter:
            t11=time.time()
            print('fecha: ',date)
    
            fileCSV4D_IBIS,fileCSV4D,fileNPY=Dicc_filter[date]
            
            if ntf.check_data_in_csv4d(fileCSV4D_IBIS) and ntf.check_data_in_csv4d(fileCSV4D):
    
                t1=time.time()
                DataIBIS_array=get_xyzd_from_csv4d(fileCSV4D_IBIS,5)
                Data_array=get_xyzd_from_csv4d(fileCSV4D,5)
                PM=np.load(fileNPY)
                print('Tiempo en cargar datos del csv4d y npy: ',time.time()-t1)
                
                t1=time.time()
                colors,colors2,A,A2,XY=do_matrix_level_from_csv4d_colors(Data_array,DataIBIS_array,PM,n_rows,n_cols,x_min,y_min,cell_size,norm,cmap1)
                print('Tiempo en obtener matriz A,A2 y colores: ',time.time()-t1)
                
              
                fig1.suptitle('Radar %s\n %s\n%s'%(numIBIS,date,'Hazard Map'))
                ntg.Graph_Point2D(ax2D1,XY,colors)
                ntg.set_arrow_north(ax2D1)
                
                fig2.suptitle('Radar %s\n %s\n%s'%(numIBIS,date,NameModel))
                ntg.Graph_Point2D(ax2D2,XY,colors2)
                ntg.set_arrow_north(ax2D2)
                
                
                
                fmtDate=ntu.datetime_to_str(date,fmt='%Y_%m_%d_%H_%M')
                t1=time.time()
                fig1.savefig(pathdirImg+fmtDate+'_HM.png',dpi=300)
                fig2.savefig(pathdirImg+fmtDate+'_MM.png',dpi=300)
                # print('Tiempo en guardar imagenes: ',time.time()-t1)
                
                N=len(colors)
                N2=len(colors)
                # fig1.clf(),fig2.clf()
                ax2D1.clear(),ax2D2.clear()
            # except:
                # print('Error en dimensiones de XY y colores')
                # print(len(colors),len(colors2),DataIBIS_array[:,0:2].shape)
                # pass
                
                # del fig1,ax2D1,fig2,ax2D2
                del DataIBIS_array,Data_array
                gc.collect()
               
                t1=time.time()
                N_counts=count_level(A,levels,umbral=2)
                N2_counts=count_level(A2,levels,umbral=3)
                d=[date,N]+N_counts +[N2]+ N2_counts
                # data.append(d)
                # print('tiempo en contar cada nivel: ',time.time()-t1)
                f.write('%s\n'%(','.join(map(str,d))))
                print('tiempo total del un ciclo: ',time.time()-t11)
    
    
        
    # ntf.save_list_to_csv(pathdir+'datos_couts_level'+str(numIBIS),data,header=header)
def datetime_from_str(string):
    #print(string,type(string),type(string.decode("utf8")))
    return dt.datetime.strptime(string.decode("utf8"), '%Y-%m-%d %H:%M:%S')
def main_graph_data(numIBIS,NameModel):
    import matplotlib as mpl
    mpl.use('Qt5Agg')
    pathdir1='Data_IBIS%s/%s/'%(numIBIS,NameModel[0])
    pathdir2='Data_IBIS%s/%s/'%(numIBIS,NameModel[1])
    
    
    namecols=['fecha','N','N_L0','N_L1','N_L2','N_L3','G40','G100','N2','N2_L0','N2_L1','N2_L2','N2_L3','G2_40','G2_100']
    namefile1=pathdir1+'datos_couts_level_IBIS_'+str(numIBIS)+'.csv'
    namefile2=pathdir2+'datos_couts_level_IBIS_'+str(numIBIS)+'.csv'
    
    data1 = np.genfromtxt(namefile1, names=namecols, delimiter=",",skip_header=1,dtype=None,converters={'fecha': datetime_from_str})
    data2 = np.genfromtxt(namefile2, names=namecols, delimiter=",",skip_header=1,dtype=None,converters={'fecha': datetime_from_str})
    
    fig,ax=ntg.Set_Properties_Graphic(figsize=(14,8),fmtEjeX='%d %H:%M',fontsize=10,Num=10)
    L_HM=(data1['N_L3'])/data1['N']
    L_MM1=(data1['N2_L3'])/data1['N']
    L_MM2=(data2['N2_L3'])/data2['N']
    
    ax.set_title('Indice celdas activas')
    ax.plot(data1['fecha'],L_HM,label='Hazard Map')
    ax.plot(data1['fecha'],L_MM1,label=NameModel[0])
    ax.plot(data2['fecha'],L_MM2,label=NameModel[1])
    ax.legend()
    
    # with open(pathdir+'datos_couts_level_IBIS_'+str(numIBIS)+'.csv','r') as f:
    #     pass
        
      
    
if __name__=='__main__':
    NameModel1='MMLP_Media_EV_1-2-3_Nobalanced'
    NameModel2='MMLP_Media_EV_6-7-8-9-10-13-16_Nobalanced'
    main_graph_data(3,[NameModel1,NameModel2])
    
    # main(2,NameModel)
    # main(3,NameModel)
    # NameModel='MMLP_Media_EV_6-7-8-9-10-13-16_Nobalanced'
    # main(2,NameModel)
    # main(3,NameModel)
    
        
        
    