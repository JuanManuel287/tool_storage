# -*- coding: utf-8 -*-
import os
import sys
import time
from datetime import datetime

from data_struct import DataCube
import new_tool_files as ntf
import new_tool_utility as ntu



def init_datacube(PathOUT,PathOUT2,fileCSV4D,NameRegistry,NameRegistryGlobal,CuboDatos):
    StartDate,_,Name=ntf.get_info_from_csv4d(fileCSV4D)
    StartDate=ntu.datetime_to_str(StartDate)
    
    XY, info = ntu.create_all_cells(fileCSV4D)
    ntf.save_registry_global(PathOUT+NameRegistryGlobal,Name,StartDate,info)
    
    CuboDatos.set_xy(XY[:,0],XY[:,1])
    print('Se inicializa el cubo de datos : ',Name,' fecha:', datetime.now())
                    
    with open(PathOUT2+NameRegistry,'w') as f:
    	pass
    print('Se inicializa el registro de cubo de datos : ',NameRegistry,' fecha:', datetime.now())
        

def run_process_data_cube(PathIN,PathOUT):
    PathOUT1=PathOUT
    
    NameRegistry='read_csv4d_datacube.dat'
    NameRegistryGlobal='Header_Data.csv'
    
    while True:
        ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
        if len(ListFiles)>1:
            NameSorted=ntf.get_names_datacubes(ListFiles)
            NameSortedStart=[name for name in NameSorted]
            break
        time.sleep(3)
        
    while True: 
        name=NameSortedStart[0]
        PathOUT=PathOUT1+name+'/'
        PathOUT2=PathOUT+'Data Cube/'
        ntf.make_dir(PathOUT2)
        
        ListFiles=ntf.get_files_for_datacube(PathIN,name)
        NewListFiles=ntf.get_new_files_with_data(ListFiles,PathOUT2+NameRegistry)
        
        if len(NewListFiles)>1:
            NewListFilesSorted=ntf.sorted_files_list_for_date(NewListFiles)
            Path_DataCube=PathOUT2+name+'.h5'
            if not os.path.exists(Path_DataCube):
                CuboDatos=DataCube(Path_DataCube)
                fileCSV4D=NewListFilesSorted[0]
                init_datacube(PathOUT,PathOUT2,fileCSV4D,NameRegistry,NameRegistryGlobal,CuboDatos)
            INFO=ntf.get_registry_global(PathOUT+NameRegistryGlobal)
            end=-1
            if name!=NameSortedStart[-1]:
                end=len(NewListFilesSorted)
                NameSortedStart.pop(0)
            else:
                ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
                NameSortedAux=get_names_datacubes(ListFiles)
                for n in NameSortedAux:
                    if n not in NameSorted:
                        NameSortedStart.append(n)
                
            for fileCSV4D in NewListFilesSorted:
                
                if NewListFilesSorted[-1]==fileCSV4D:
                    ntf.check_read_file(fileCSV4D)
                try:
                    ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
                    CuboDatos.set_data_from_csv4d(fileCSV4D)
                except:
                    pass
                    
       
        time.sleep(3)


if __name__=='__main__':

	
	if len(sys.argv)>1:
        PathIN=sys.argv[1]
        PathOUT=sys.argv[2]

    run_process_data_cube(PathIN,PathOUT)

