# -*- coding: utf-8 -*-
"""
Created on Sun Jan 12 15:30:48 2020

@author: JOliva
"""
import numpy as np
from datetime import datetime,timedelta
import numba
import time
import new_tool_files as ntf
        

def str_to_datetime(StrDate,fmt=None):
    if fmt is None:
        try:
            fmt='%Y-%m-%d %H:%M'
            return datetime.strptime(StrDate,fmt)
        except:
            fmt='%Y-%m-%d %H:%M:%S'
            return datetime.strptime(StrDate,fmt)
    else:
        return datetime.strptime(StrDate,fmt)

def datetime_to_seconds(DateTime):
    return (DateTime - datetime(1970, 1, 1)).total_seconds()
def seconds_to_datetime(seconds):
    return datetime(1970, 1, 1) + timedelta(seconds=seconds)

def mean_datetimes(ListDateTimes):
    DatesSeconds=[datetime_to_seconds(f) for f in ListDateTimes]
    mean=np.mean(DatesSeconds)
    DateTime=seconds_to_datetime(mean)
    if DateTime.second>30:
        DateTime=DateTime+timedelta(seconds=60-DateTime.second)
    else:
        DateTime=DateTime+timedelta(seconds=-DateTime.second)
    return DateTime
def std_datetimes(ListDateTimes):
    '''
    Parameters
    ----------
    ListDateTimes : lista de datetimes
        
    Returns
    -------
    std : desviación estandar en segudnos

    '''
    DatesSeconds=[datetime_to_seconds(f) for f in ListDateTimes]
    std=int(np.std(DatesSeconds))
    return std

def strXY_to_floatXY(StrCoord,k):
    xy=np.array([float(v) for v in StrCoord.split('-')])
    # xc=np.trunc(xy[0]*10**k)/10**k
    # yc=np.trunc(xy[1]*10**k)/10**k
    xc=np.round(xy[0],k)
    yc=np.round(xy[1],k)
    return xc,yc

def floatXY_to_strXY(x,y,k):
    # x=np.trunc(x*10**k)/10**k
    # y=np.trunc(y*10**k)/10**k
    # x=np.round(x,k)
    # y=np.round(y,k)
    formato='%.{}f-%.{}f'.format(k,k)
    return formato%(x,y)
    
def datetime_to_str(DateTime,fmt=None):
    if fmt is not None:
        return DateTime.strftime(fmt)
    else:
        return DateTime.strftime('%Y-%m-%d %H:%M')
    
@numba.jit
def get_pos_for_XY(xy,x_min,y_min,cell_size,n_cols,k=5):
    x,y=strXY_to_floatXY(xy,k)
    i,j=get_ij_from_xy(x,y,x_min,y_min,cell_size)
    index=j+i*n_cols
    return index

@numba.jit
def get_index_for_ij(i,j,n_cols):
    index=j+i*n_cols
    return index

@numba.jit
def get_ij_from_xy(x,y,x_min,y_min,cellsize):
    i=int(np.round((y-y_min)/cellsize))
    j=int(np.round((x-x_min)/cellsize))
    return i,j

@numba.jit
def get_xy_from_ij(i,j,x_min,y_min,cellsize):
    x=np.round(j*cellsize+x_min,5)
    y=np.round(i*cellsize+y_min,5)
    return x,y

def get_IJ_from_XY(X,Y,x_min,y_min,cell_size):
    I=np.round(((Y-y_min)/cell_size))
    J=np.round(((X-x_min)/cell_size))
    return np.array(list(map(int,I))),np.array(list(map(int,J)))

def get_XY_from_IJ(I,J,x_min,y_min,cell_size):
    X=np.round(J*cell_size+x_min,5)
    Y=np.round(I*cell_size+y_min,5)
    return X,Y

def find_date_nearest(DateIn,DatesLists):
    dif=datetime.now()-datetime(1900,1,1)
    
    for i,d in enumerate(DatesLists):
        if abs(d-DateIn)<dif:
            NearestDate=d
            dif=abs(d-DateIn)
            index=i
    return NearestDate,index

def find_date_nearest2(DateIn,DatesArray):
    
    if DateIn<DatesArray[0]:
        return DatesArray[0],0
    if DateIn>DatesArray[-1]:
        return DatesArray[-1],len(DatesArray)-1
    
    
    subDatesArray_up=DatesArray[(DateIn<=DatesArray)]
    subDatesArray_down=DatesArray[(DateIn>=DatesArray)]
    
    StartIndex=int(np.argwhere(DatesArray==subDatesArray_down[-1]))
    EndIndex=int(np.argwhere(DatesArray==subDatesArray_up[0]))
    
    if StartIndex!=EndIndex:
        dif=datetime.now()-datetime(1900,1,1)
        
        for i in range(StartIndex,EndIndex+1):
            if abs(DatesArray[i]-DateIn)<dif:
                NearestDate=DatesArray[i]
                dif=abs(DatesArray[i]-DateIn)
                index=i
        return NearestDate,index
    else:
        return DatesArray[StartIndex],StartIndex
        
    


def delta_time(date, interval_time):
    days, hour, minute=interval_time
    dif=timedelta(days=days,hours=hour-date.hour,minutes=minute-date.minute)
    return dif
    
def set_values_Z(INFO,fileCSV4D,Z):
    _,_,_,_,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    dataZ=ntf.get_xyzd_from_csv4d(fileCSV4D,5)
    for x,y,z,_ in dataZ:
        i,j=get_ij_from_xy(x,y,x_min,y_min,cell_size)
        Z[i,j]=z
    return Z

 
def get_xyzg_from_cluster(clusters,Z,x_min,y_min,cell_size):
    xyzg_list=[]
    for num,ij in enumerate(clusters):
        ij_list,_=clusters[ij]
        for i,j in ij_list:
            x,y=get_xy_from_ij(i,j,x_min,y_min,cell_size)
            z=Z[i,j]
            if z is np.nan:
                z=np.nanmean(Z[i-3:i+3,j-3:j+3])
            xyzg_list.append((x,y,z,num))
    return xyzg_list

def get_data_times_series(PathOUT_filename):
    filenpz=np.load(PathOUT_filename,allow_pickle=True)
    # Acceso a los arreglos almacenados en el archivo .npz
    Cond=filenpz['arr_0']
    XY=filenpz['arr_1']
    TS=filenpz['arr_2']
    TW=filenpz['arr_3']
    filenpz.close()      
    return Cond,XY,TS,TW

def create_matrixs_XYZ_PM(path_npz,INFO):
    _,XY,_,_=get_data_times_series(path_npz)
    _,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    
    PM=np.empty((n_rows,n_cols), dtype=np.float32)
    PM[:,:]=np.nan
    VM=np.array(np.zeros((n_rows,n_cols)), dtype=np.float32)
    
    X=np.empty((n_rows,n_cols), dtype=np.float32)
    Y=np.empty((n_rows,n_cols), dtype=np.float32)
    Z=np.empty((n_rows,n_cols), dtype=np.float32)
    Z[:,:]=np.nan
    
    t1=time.time()
    
    
    IJ=np.array([get_ij_from_xy(x,y,x_min,y_min,cell_size) for x,y in XY],dtype=np.int32)
    X[IJ[:,0],IJ[:,1]]=XY[:,0]
    Y[IJ[:,0],IJ[:,1]]=XY[:,1]
    
    white=[1.,1.,1.,1.]
    CellsColors=np.array([[white for _ in range(n_cols)] for _ in range(n_rows)])
    
    print('Tiempo en llenar las matrices X e Y: ',time.time()-t1)
    return X,Y,Z,PM,VM,CellsColors

def get_all_xy_nearest(xc,yc,xy_array,r, form=0):
    if form==0:
        #forma de cuadrado
        indexs=[i for i,xy in enumerate(xy_array) if abs(xy[0]-xc)<=r and  abs(xy[1]-yc)<=r]
    if form==1:
        #forma circular
        indexs=[i for i,xy in enumerate(xy_array) if np.sqrt((xy[0]-xc)**2 +(xy[1]-yc)**2)<=r]
    return xy_array[indexs], indexs

def traslation_time_serie(TS):
    indexs_nonan=np.argwhere(~np.isnan(TS[:,0]))
    newTS=np.array([TS[i,:]-TS[i,0] for i in indexs_nonan],dtype=np.float32)
    return newTS, indexs_nonan

def create_all_cells(pathCSV4D,margin=100):
    XYZD=ntf.get_xyzd_from_csv4d(pathCSV4D,5)
    XY=XYZD[:,:2]
    X=sorted(list({x:0 for x in XY[:,0]}.keys()))
    Y=sorted(list({y:0 for y in XY[:,1]}.keys()))
    DistMinX=np.round(np.min(np.diff(X)),5)
    DistMinY=np.round(np.min(np.diff(Y)),5)

    if DistMinX!=DistMinY:
        print('La malla tiene celdas rectangular %.5f x %.5f '%(DistMinY,DistMinX))
    cell_size=min(DistMinX,DistMinY)
    x_min,x_max=X[0] - cell_size*margin ,X[-1] + cell_size*margin
    y_min,y_max=Y[0] - cell_size*margin ,Y[-1] + cell_size*margin

    n=int((y_max-y_min)/cell_size)+1
    m=int((x_max-x_min)/cell_size)+1

    XY=create_list_XY(x_min,m,y_min,n,cell_size,5)


    info=[m,n,x_min,x_max,y_min,y_max,cell_size,DistMinX,DistMinY]
    
    return XY,info

def create_list_XY(x_min,m,y_min,n,cell_size,k):
    X=np.array([x_min + cell_size*i for i in range(m)])
    Y=np.array([y_min + cell_size*i for i in range(n)])

    return np.round(np.array([(x,y) for y in Y for x in X],dtype=np.float32),k)



    
    
    
    