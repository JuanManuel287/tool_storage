# -*- coding: utf-8 -*-
"""
Created on Wed Dec  4 18:27:31 2019

@author: JOliva
"""
import numpy as np
from time import time
import pickle
import datetime as dt
import new_tool_utility as ntu
import new_tool_files as ntf



#@numba.jit( nopython = True)
def do_cluster(A,umbral=0.5,len_min_cluster=1,r=1):
    '''
    Mask es un array de n,m que contiene valores bool, este representa 
    si se ha visitado o no la celda
    r: es un entero positivo mayor o igual a 1 que determina la cantidad de celdas 
    en las cuatro direcciones
    '''
    All_cluster={}
    if len_min_cluster>=1:
        Mask=create_mask(A,umbral=umbral)
        n,m=Mask.shape
        cont=1
        for i in range(n):
            for j in range(m):
                if Mask[i,j]:
                    Mask[i,j]=False
                    cluster,prob,centroide=search_adjoining_cells(A,Mask,i,j,r)
                    if len_min_cluster<=len(cluster):
                        All_cluster[centroide]=[cluster,prob]
                        cont=cont+1
    else:
        print('len_min_cluster debe ser mayor a 0')
    return All_cluster
                 
                 
                         
#@numba.jit( nopython = True)                   
def search_adjoining_cells(A,Mask,i,j,r):
    n,m=Mask.shape
    cluster=[(i,j)]
    prob=[A[i,j]]
    pos_cluster=0
    while len(cluster)>pos_cluster:
        i,j=cluster[pos_cluster]
        left=j-r
        right=j+r
        up=i-r
        down=i+r
        if left<0: left=0
        if right>m-1: right=n-1
        if up<0: up=0
        if down>n-1: down=n-1
        for  ii in range(up,down+1):
            for jj in range(left,right+1):
                if Mask[ii,jj]:
                    cluster.append((ii,jj))
                    prob.append(A[ii,jj])
                    Mask[ii,jj]=False
        
        pos_cluster=pos_cluster+1
        
    ic,jc=0,0
    for ii,jj in cluster:
        ic=ic+ii
        jc=jc+jj
    ic=int(np.round(ic/len(cluster)))
    jc=int(np.round(jc/len(cluster)))
    return cluster,prob,(ic,jc)

def create_mask(A,umbral=0.5):

#    Mask=[[True if A[i,j]>=umbral else False for j in range(m)] for i in range(n)]
    Mask=np.where(A>=umbral,True,False)

    return Mask

#def do_contour(Mask):
#    #recorrido horizontal 
#    n,m=Mask.shape
#    contour=np.ones((n,m))
#    for i in range(n):
#        for j in range(m-1):
#            if Mask[i,j]!=Mask[i,j+1]:
#                if not Mask[i,j]:
#                    contour[i,j]=0
#                else:
#                    contour[i,j+1]=0
#    #recorrido vertical
#    for j in range(m):
#        for i in range(n-1):
#            if Mask[i,j]!=Mask[i+1,j]:
#                if not Mask[i,j]:
#                    contour[i,j]=0
#                else:
#                    contour[i+1,j]=0
##    for j in range(m):
##        for i in range(n-1):
##            if Mask[i,j]!=Mask[i+1,j]:
##                contour[i,j]=0
#    return contour 

def do_contour(Mask):
    ''' Input binary 2D (NxM) image. Ouput array (2xK) of K (y,x) coordinates
        where 0 <= K <= 2*M.
    '''
    topbottom = np.empty((1,2*Mask.shape[1]), dtype=np.uint16)
    topbottom[0,0:Mask.shape[1]] = np.argmax(Mask, axis=0)
    topbottom[0,Mask.shape[1]:] = (Mask.shape[0]-1)-np.argmax(np.flipud(Mask), axis=0)
    mask      = np.tile(np.any(Mask, axis=0), (2,))
    xvalues   = np.tile(np.arange(Mask.shape[1]), (1,2))
    return np.vstack([topbottom,xvalues])[:,mask].T



def join_clusters(clusters,cells_size,r=15):
    #para len(clusters)<1500 se realiza en menos de 2 seg
    ptos_nearest=[]
    if len(clusters)>1:  
        ij_list=list(clusters.keys())
        point_nearest=[]
        ti=time()
        while len(ij_list)>0:
            i1,j1=ij_list[0][0],ij_list[0][1]
            point_nearest_aux=[(i1,j1)]
            ij_list.remove((i1,j1))
            i=0
            while i<len(ij_list):
            # for i2,j2 in ij_list:
                i2,j2=ij_list[i][0],ij_list[i][1]
                dist=np.sqrt((i1-i2)**2 + (j1-j2)**2)*cells_size
                if dist<r:
                    point_nearest_aux.append((i2,j2))
                    ij_list.remove((i2,j2))
                else:
                    i=i+1
            point_nearest.append(point_nearest_aux)       
        print('Tiempo en buscar centroides mas cercanos: ',time()-ti)
        
        
        clusterNew={}
        ti=time()
        for ptos in point_nearest:
            ij_array=np.array(ptos,dtype=np.int32)
            ic=int(np.sum(ij_array[:,0])/len(ij_array))
            jc=int(np.sum(ij_array[:,1])/len(ij_array))
            list1=[]
            list2=[]
            for p in ptos:
                list1=list1+clusters[p][0]
                list2=list2+clusters[p][1]
            clusterNew[(ic,jc)]=[list1,list2]
        print('Tiempo en unir centroides mas cercanos: ',time()-ti)
        return clusterNew
            
    else:
        return clusters
    




class Cluster(object):
    def __init__(self,idd=None,ijcenter=None,ijcellslist=[],probcellslist=[]):
        self._ID=idd
        self._ijcenter=ijcenter
        self._IJ=np.array(ijcellslist,dtype=np.int32) 
        self._Prob=np.array(probcellslist,dtype=np.float32) 
        self._ncells=len(ijcellslist) 

        self._Velocity=[]
        self._LevelPM=[]
        self._LevelVM=[]

        self._xyzcenter=None
        self._XYZ=[]

        self.factor1=None
        self.factor2=None

        self._NcellsThresholdProb=[]
        self._nPersintence=None
        self._timePersistence=None
        self._nIntermittence=None

        self._xmin=None
        self._xmax=None
        self._ymin=None
        self._ymax=None

        self._xyzcenterPrev=None

        
        self._IDnext=None
        self._IDprevious=None
        
        
    def __len__(self):
        return self._ncells

    #########################SET########################
    def set_xyz_cells(self,X,Y,Z):
        self._XYZ=np.array([(X[i,j],Y[i,j],Z[i,j]) for i,j in self._IJ],dtype=np.float32)
        i,j=self._ijcenter
        self._xyzcenter=(X[i,j],Y[i,j],Z[i,j])
        self._xmin=np.nanmin(self._XYZ[:,0])
        self._xmax=np.nanmax(self._XYZ[:,0])
        self._ymin=np.nanmin(self._XYZ[:,1])
        self._ymax=np.nanmax(self._XYZ[:,1])

    def set_probability_cells(self,PM):
        self._Prob=np.array([PM[i,j] for i,j in self._IJ],dtype=np.float32)

    def set_velocity_cells(self,VM):
        self._Velocity=np.array([VM[i,j] for i,j in self._IJ],dtype=np.float32)
    
    def set_level_probability_cells(self,LPM):
        self._LevelPM=np.array([LevelPM[i,j] for i,j in self._IJ],
                                dtype=np.int32)

    def set_level_velocity_cells(self,LevelVM):
        self._LevelVM=np.array([LevelVM[i,j] for i,j in self._IJ],
                                dtype=np.int32)
    
    def set_area(self,cellsize):
        self.area=np.float32(self._ncells*cellsize**2)
    
    def set_factor_1(self):
        pass
    def set_factor_2(self):
        pass

    def count_ncells_above_thresholdProb(self,TimesSerieProb,thresholdProb):
        TSP=np.transpose(TimesSerieProb)
        self._NcellsThresholdProb=np.array([len(tsp[tsp>=thresholdProb]) for tsp in TSP],dtype=np.int32)

    def calculate_persistence(self,thresholdCells,TimeWindow):
        n=len(self._NcellsThresholdProb)
        self._nPersistence=1
        self._timePersistence=0
        if n>0:
            for i in range(n-2,-1,-1):
                if thresholdCells<=self._NcellsThresholdProb[i]:
                    self._nPersistence+=1
                    self._timePersistence=(TimeWindow[-1]-TimeWindow[i]).total_seconds()/60.
                else:
                    break

    def calculate_intermittence(self,thresholdCells,TimeWindow):
        n=len(self._NcellsThresholdProb)
        self._nIntermitencia=0
        if n>0:
            self._nIntermitencia=len(np.where(self._NcellsThresholdProb[self._NcellsThresholdProb>=thresholdCells]))
            self._porcIntermitencia=self._nIntermitencia/n
            





    
    ##############GET############################
    
    def get_id(self):
        return self._ID
    
    def get_IJ_cells(self):
        return self._IJ
    
    def get_XYZ_cells(self):
        return self._XYZ

    def get_xyz_center(self):
        self._xyzcenter

    def get_velocity_cells(self,VelMatrix):
        return self._Velocity

    def get_level_probability_cells(self,LevelPM):
        return self._LevelPM

    def get_level_velocity_cells(self,LevelVM):
        return self._LevelVM

    def get_area(self,cellsize):
        return self.area

    def get_xlim(self):
        return (self._xmin,self._xmax)

    def get_ylim(self):
        return (self._ymin,self._ymax)
    
    # def get_time_series_cells(self,DataCube,StartDate,EndDate):
    #     if self._XYZ is not None:
    #         XY=XYZ[:,:-1]
    #         XY_eval,TS_eval,TW_eval=DataCube.get_time_serie_for_eval(XY,StartDate,EndDate)
    #         return XY_eval,TS_eval,TW_eval
    def save_xyz_cluster_as_csv(self,PathOut,filename):
        xyzg_list=[(x,y,z,self._ID) for x,y,z in self._XYZ]
        ntf.create_csv_for_ibis(PathOut,filename,xyzg_list)

class Clusters():
    def __init__(self,Date,NameDataCube,radar,cellsize):

        self._date=Date
        self._NameDataCube=NameDataCube
        self._ClustersList=[]
        self._radar=radar
        self._cellsize=cellsize
        self._ClustersDictSingle={}
        self._ClustersDictJoin={}
        self._nClusters=0
        self._TimeWindow=[]
        self._NClustersCriteria=0

    def __len__(self):
        return self._nClusters

    # def set_property(self,Date,NameDataCube,radar,cellsize):
    #     self._date=Date
    #     self._NameDataCube=NameDataCube
    #     self._radar=radar
    #     self._cellsize=cellsize

    def create_clusters_list(self,PM,umbral,len_min_cluster,radio):
        self._ClustersDictSingle=do_cluster(PM,umbral=umbral,len_min_cluster=len_min_cluster)
        self._ClustersDictJoin=join_clusters(self._ClustersDictSingle,self._cellsize,r=radio)
       

        for num,ijcenter in enumerate(self._ClustersDictJoin):
            ijcellslist,probcellslist=self._ClustersDictJoin[ijcenter]
            cluster=Cluster(num,ijcenter,ijcellslist,probcellslist)
            self._ClustersList.append(cluster)

        self._nClusters=len(self._ClustersList)

    

    def set_data_clusters(self,X,Y,Z,VM,LevelPM,LevelVM):
        for cluster in self._ClustersList:
            cluster.set_xyz_cells(X,Y,Z)
            cluster.set_velocity_cells(VM)
            cluster.set_level_probability_cells(LevelPM)
            cluster.set_level_velocity_cells(LevelVM)
            cluster.set_area(self._cellsize)

    def set_velocity(self,VM):
        for cluster in self._ClustersList:
            cluster.set_velocity_cells(VM)

    def set_xyz(self,X,Y,Z):
        for cluster in self._ClustersList:
            cluster.set_xyz_cells(X,Y,Z)

    def set_level_probability(self,LevelPM):
        for cluster in self._ClustersList:
            cluster.set_level_probability_cells(LevelPM)

    def set_level_velocity(self,LevelVM):
        for cluster in self._ClustersList:
            cluster.set_level_velocity_cells(LevelVM)

    def calculate_criteria(self,DataCubeProbability,StartDate,EndDate,thresholdProb=0.9,thresholdCells=4):
        lenEnd=self.get_lengths_clusters()
        lenStart=[0] + lenEnd[:-1]
        XYZ=get_xyz_clusters()

        TimeSerieProb,TW=DataCubeProbability.get_timeserie_probability(XYZ[:,-1],StartDate,EndDate)
        self._TimeWindow=TW
        for start,end, cluster in zip(lenStart,lenEnd,self._ClustersList):
            SubTimesSerieProb=TimeSerieProb[start:end,:]
            cluster.count_ncells_above_thresholdProb(SubTimesSerieProb,thresholdProb)
            cluster.calculate_persitence(thresholdCells,TW)
            #cluster.calculate_intermittence(thresholdCells,TW)
    

    def filter_clusters_by_criteria(self,timedeltaPersistence=15):
        
        listIndexs=[]
        for cluster in self._ClustersList:
            if cluster._timePersistence>=timedeltaPersistence:
                listIndexs.append(cluster._ID)

        self._NClustersCriteria=len(listIndexs)

        IDold=0
        for i, index in enumerate(listIndexs):
            cluster1=self._ClustersList[i]
            cluster2=self._ClustersList[index]

            cluster1._ID=index
            cluster2._ID=i

            self._ClustersList[i]=cluster2
            self._ClustersList[index]=cluster1

    def get_clusters_meet_criteria(self):
        return self._ClustersList[:self._NClustersCriteria]







            
    def get_all_clusters_as_dict(self):
        return self._ClustersDictJoin

    def get_cluster(self,index):
        return self._ClustersList[index]

    def get_clusters(self):
        return self._ClustersList

    def get_date(self):
        return self._date

    def get_xyz_clusters(self):
        allXYZ=np.vstack([cluster._XYZ for cluster in self._ClustersList])
        return allXYZ

    def get_lengths_clusters(self):
        return [len(cluster) for cluster in self._ClustersList]        

    def save_xyz_clusters(self,PathOut,filename):
        for cluster in self._ClustersList:
            xyzgList=[(x,y,z,cluster._id) for x,y,z in cluster._XYZ]
        ntf.create_csv_for_ibis(PathOut,filename,xyzgList)




    def save(self,pathName):
        with open(pathName+".pickle","wb") as f_out:
            pickle.dump(self, f_out)

    def load(self,pathName):
        with open(pathName+".pickle","rb") as f_in:
            return pickle.load(f_in)

    def do_matching(self,ClustersPrev,radio=15):
        
        ClustersPrevList=ClustersPrev.get_clusters()
        for cluster in self._ClustersList:
            distmin=radio
            index=None
            for i, clusterprev in enumerate(ClustersPrevList):
                dist=np.norm(cluster._xyzcenter,clusterprev._xyzcenter)
                if dist<=distmin:
                    self._IDprevious=clusterprev._id
                    distmin=dist
                    index=i
                    self._xyzcenterPrev=clusterprev._xyzcenter
            if index is not None:
                del ClustersPrevList[index]






def load_clusters(path_name):
    with open(path_name+".pickle","rb") as f_in:
        return pickle.load(f_in)


if __name__=='__main__':

    print('A')
    from data import Displacements, Probability


    path=r"G:\Respaldo OUT\IBIS_2\MASTER_MLP_18012020_IBIS-2_MLP_24122019\Data Cube"
    nameCD=r"\MASTER_MLP_18012020_IBIS-2_MLP_24122019.h5"

    umbral=0.9
    len_min_cluster=10
    radio=2


    np.random.seed(0)
    A=np.random.rand(20,20)
    A=np.array(A,dtype=np.float16)
    t1=time()

    DCDesplazamiento=Displacements(path+nameCD)

    NameDataCube=DCDesplazamiento.get_name()
    radar=DCDesplazamiento.get_radar()
    cellsize=DCDesplazamiento.get_cells_size()
    print(DCDesplazamiento)
    n=DCDesplazamiento.get_nrows()
    m=DCDesplazamiento.get_ncols()
    Date=DCDesplazamiento.get_dates()[-1]
    XY=DCDesplazamiento.get_xy()
    X=XY[:,0].reshape((n, m))
    Y=XY[:,1].reshape((n, m))
    Z=np.random.randint(2500,4500,(n,m))
    VM=np.random.randint(0,10,(n,m))
    PM=np.random.rand(n,m)
    LevelsVM=np.random.randint(0,4,(n,m))
    LevelsPM=np.random.randint(0,4,(n,m))


    t1=time()

    Grupos=Clusters(Date,NameDataCube,radar,cellsize)
    Grupos.create_clusters_list(PM,umbral,len_min_cluster,radio)
    Grupos.set_data_clusters(X,Y,Z,VM,LevelsPM,LevelsVM)
    

    print('numero de grupos:',len(Grupos))
    # for grupo in Grupos.get_Clusters():
    #     print('grupo: %d tiene %d celdas'%(grupo.get_id(),len(grupo)))

    #print(Grupos.get_cluster(0).get_XYZ_cells())
    print('tiempo en hacer los grupos: ',time()-t1)
#    
#    print('tamaño en memoria MB:',getsizeof(A)/1000000)
    
    Grupos.save('grupos_test')
    # import pickle
    # with open("grupos_test.pickle","wb") as f_out:
    #     pickle.dump(Grupos, f_out)
    
    Grupos_in=Clusters().load('grupos_test')
    # with open("grupos_test.pickle","rb") as f_in:
    #     Grupos_in = pickle.load(f_in)

    cluster0=Grupos_in.get_cluster(0)

    

    import new_tool_graphic as ntg
    import matplotlib.pyplot as plt
    import matplotlib as mpl
    import datetime as dt
    mpl.use('Qt5Agg')
    
    
    
    
    xc,yc=58395.2,91166.5
    radio = 15
    XY=DCDesplazamiento.get_xy()
    indexs=[i for i,xy in enumerate(XY) if abs(xy[0]-xc)<=radio and  abs(xy[1]-yc)<=radio]
    XY_eval=XY[indexs]



    dates=DCDesplazamiento.get_dates()
    StartDate=dt.datetime(2020,2,13,23,0)#dates[-144]#
    EndDate=dt.datetime(2020,2,14,12,0)#dates[-1]#
    StartDate,StartIndex=ntu.find_date_nearest2(StartDate,dates)
    EndDate,EndIndex=ntu.find_date_nearest2(EndDate,dates)
    print(cluster0._XYZ)

   
    
    XY_eval,TS_eval,TW_eval=DCDesplazamiento.get_time_serie_for_eval(XY_eval,StartDate,EndDate)
    ts_min=np.nanmin(TS_eval)-2
    ts_max=np.nanmax(TS_eval)+2
    fig,ax,ax2D=ntg.create_graph_TS_2D(ts_min,ts_max,TW_eval,'Example','%H:%M\n%d-%b')

    for ts in TS_eval:
        ax.plot(TW_eval,ts)

    plt.show()
    # for grupo1, grupo in zip(Grupos_in.get_Clusters(),Grupos.get_Clusters()):
    #     print('grupo1: %d tiene %d celdas\ngrupo0: %d tiene %d celdas\n'%(grupo1.get_id(),len(grupo1),grupo.get_id(),len(grupo)))
    # xyz=Grupos.get_cluster(0).get_XYZ_cells()
    # xyz1=Grupos_in.get_cluster(0).get_XYZ_cells()
    # if xyz.all()==xyz1.all():
    #     print('son iguales')
    # if Grupos.get_cluster(0).get_XYZ_cells()==Grupos_in.get_cluster(0).get_XYZ_cells():
    #     print('son iguales')

#    for i in range(100):
    # ti=time()
    
    # All_cluster=do_cluster(A,umbral,len_min_cluster=len_min_cluster)
    
    # print('tiempo en definir los grupos:',time()-ti)
    # print('numero de grupos: ',len(All_cluster))
    
    
    # import matplotlib.pylab as plt
    # fig = plt.figure(figsize=(10,10))
    # cont=1
    # for i,j in All_cluster:
    #     ij=np.array(All_cluster[(i,j)][0])
    #     plt.plot(ij[:,1],ij[:,0],'ob')
    #     plt.plot(j,i,'or')
    #     plt.text(j,i,'g'+str(cont))
    #     cont=cont+1
    # plt.show()
    
    # fig2 = plt.figure(figsize=(10,10))
    # NewCluster=join_clusters(All_cluster,1,r=5)
    # print('Nuevo numero de grupos: ',len(NewCluster))
    # cont=1
    # for i,j in NewCluster:
    #     ij=np.array(NewCluster[(i,j)][0])
    #     plt.plot(ij[:,1],ij[:,0],'ob')
    #     plt.plot(j,i,'or')
    #     plt.text(j,i,'g'+str(cont))
    #     cont=cont+1
    # plt.show()

        
    
    
    
    
#    for i,c in enumerate(All_cluster):
#        print('Suma prob del cluster ',c,': ',All_cluster[c][1])
#        print('Total de celdas del cluster: ',len(All_cluster[c][0]))
#        print('Promedio entre las prob    :',All_cluster[c][1]/len(All_cluster[c][0]))
#        print('Lista de indices del grupo: ',All_cluster[c][0])


    
    
    