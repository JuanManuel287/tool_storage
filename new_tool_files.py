# -*- coding: utf-8 -*-
"""
Created on Sun Jan 12 15:35:53 2020

@author: JOliva
"""

import os 
from datetime import datetime,timedelta
import numpy as np
import new_tool_utility as ntu
import time

def make_dir(PathDir):
    if not os.path.exists(PathDir):
        os.makedirs(PathDir)

def get_all_files(PathDir,ext):
    '''
    retrona una lista con las direcciones de todos los archivos de un directorio 
    con una determinada extension
    '''
    ListPathFile=[]
    for folder, subfolders, files in os.walk(PathDir):
        for file in files:
            if file.endswith(ext):
                ListPathFile.append(folder+'/'+file)
    return ListPathFile

def get_all_directoy(PathDir):
    '''
    retrona una lista con las direcciones de todos los archivos de un directorio 
    con una determinada extension
    '''
    ListPathFile=[]
    for folder, subfolders, files in os.walk(PathDir):
        for sub in subfolders:
            ListPathFile.append(folder+'/'+sub+'/')
    return ListPathFile

def get_info_from_csv4d(Path_FileName):
    '''
    Función que extrae la fecha a partir del nombre del fihero que tiene el siguiente formato
    IBIS-3_RAMPASUR_2019_Displacement_Map_from_19_Feb_27-13_04_to_19_Mar_12-10_30
    Path_FileName:= Dirección del archivo
    Retorna la fecha DateStart, DateEnd y Nombre
    '''
    Meses1={'ENE':1,'FEB':2,'MAR':3,'ABR':4,'MAY':5,'JUN':6,'JUL':7,'AGO':8,'SEP':9,'OCT':10,'NOV':11,'DIC':12}
    Meses2={'JAN':1,'FEB':2,'MAR':3,'APR':4,'MAY':5,'JUN':6,'JUL':7,'AUG':8,'SEP':9,'OCT':10,'NOV':11,'DEC':12}
    
    OnlyFileName, file_extension = os.path.splitext(os.path.basename(Path_FileName))
#    print('basename: ',os.path.basename(Path_FileName))
    OnlyFileName=OnlyFileName.upper()
    Nombre=OnlyFileName[:OnlyFileName.rfind('FROM')]
    FN=OnlyFileName[OnlyFileName.rfind('FROM'):]
    FN=FN.replace('FROM_','')
    FN=FN.replace(file_extension,'')
    FN=FN.replace('-','_')
    DateStart,DateEnd=FN.split('_TO_')
    DateStart=DateStart.split('_')
    DateEnd=DateEnd.split('_')
    if 'MAPA DE DESPLAZAMIENTO' in OnlyFileName:
        Nombre=OnlyFileName.split('MAPA DE DESPLAZAMIENTO')[0]
    elif 'MAPA_DE_DESPLAZAMIENTO' in OnlyFileName:
        Nombre=OnlyFileName.split('MAPA_DE_DESPLAZAMIENTO')[0]
    elif 'DISPLACEMENT MAP' in OnlyFileName:
        Nombre=OnlyFileName.split('DISPLACEMENT MAP')[0]
    elif 'DISPLACEMENT_MAP' in OnlyFileName:
        Nombre=OnlyFileName.split('DISPLACEMENT_MAP')[0]
        
#    p=OnlyFileName.find('IBIS')
#    Radar=OnlyFileName[p:p+6].replace('_','').replace('-','').replace(' ','')
#    if len(Radar)==6:
#        if not Radar[5].isnumeric():
#            Radar=Radar.replace(Radar[5],'')
#    if not Radar[4].isnumeric():
#        Radar=Radar.replace(Radar[4],'')
    
        
    if DateStart[1] in Meses1 and DateEnd[1] in Meses1:
        DateStart=datetime(2000+int(DateStart[0]), Meses1[DateStart[1]], int(DateStart[2]),int(DateStart[3]),int(DateStart[4]))
        DateEnd=datetime(2000+int(DateEnd[0]), Meses1[DateEnd[1]], int(DateEnd[2]),int(DateEnd[3]),int(DateEnd[4]))
     
    else: 
        DateStart=datetime(2000+int(DateStart[0]), Meses2[DateStart[1]], int(DateStart[2]),int(DateStart[3]),int(DateStart[4]))
        DateEnd=datetime(2000+int(DateEnd[0]), Meses2[DateEnd[1]], int(DateEnd[2]),int(DateEnd[3]),int(DateEnd[4]))
    
    if Nombre[-1]=='_':
        Nombre=Nombre[:-1]

    return DateStart,DateEnd,Nombre


def get_desplacement_from_csv4d_2(fileCSV4D,k,sep=';'):
    data={}
    with open(fileCSV4D,'r') as f:
        next(f)
        for line in f:
            line=line.replace(' ','').split(sep)
            x,y=ntu.strXY_to_floatXY(line[1]+'-'+line[2],k)
            xy=ntu.floatXY_to_strXY(x,y,k)
            d=line[4]
            data[xy]=d
    return data
def get_IDxyzd_from_csv4d(fileCSV4D,k,sep=';'):
    array=np.genfromtxt(fileCSV4D,delimiter=sep,skip_header=1,usecols=(0,1,2,3,4),dtype=np.float64)
    array=np.round(array,k)
    
    return array


def get_xyzd_from_csv4d(fileCSV4D,k,sep=';'):
    
    array=np.genfromtxt(fileCSV4D,delimiter=sep,skip_header=1,usecols=(1,2,3,4),dtype=np.float64)
    array=np.round(array,k)
    
    return array

def get_z_from_csv4d(fileCSV4D,k,sep=';'):
    data={}
    with open(fileCSV4D,'r') as f:
        next(f)
        for line in f:
            line=line.replace(' ','').split(sep)
            x,y=ntu.strXY_to_floatXY(line[1]+'-'+line[2],k)
            xy=ntu.floatXY_to_strXY(x,y,k)
            z=line[3]
            data[xy]=z
    return data

def get_z_from_csv4d_2(fileCSV4D,k,sep=';'):
    
    array=np.genfromtxt(fileCSV4D,delimiter=sep,skip_header=1,usecols=(1,2,3,4),dtype=np.float64)
    array=np.round(array,k)
    data={(x,y): z for x,y,z,d in array}
    return data
    

def get_xyz_from_csv4d(fileCSV4D,sep=';'):
    data=[]
    with open(fileCSV4D,'r') as f:
        next(f)
        for line in f:
            line=line.replace(' ','').split(sep)
            x,y,z=float(line[1]),float(line[2]),float(line[3])
            data.append((x,y,z))
            
    return np.array(data,dtype=np.float32)

def update_registry2(fileCSV4D,PathOUT,file,NameRegistry,DateEnd):
    
    filename=file+'_to_'+DateEnd
    with open(PathOUT+NameRegistry,'r') as f:
        Lista=f.readlines()
        Lista={l.replace('\n',''):'' for l in Lista}
    with open(PathOUT+NameRegistry,'a') as f:
        if filename not in Lista:
            print('se almaceno el archivo : ',filename)
            f.write(filename+'\n')

# def check_read_file(PathIN_file):
#     try: 
#         ff=open(PathIN_file,'r')
#         ff.close()
#         return True
#     except IOError as x:
#         print('Error: ',x.errno)
#         print('el archivo esta abierto: ',PathIN_file,' fecha:', datetime.now())
#         return False

def check_read_file(PathIN_file):
    while True:
        file1 = os.stat(PathIN_file) 
        file1_size = file1.st_size
        time.sleep(1)
        file2 = os.stat(PathIN_file) 
        file2_size = file2.st_size
        comp = file2_size - file1_size 
        if comp == 0:
            break
        else:
            time.sleep(5)
            

def sorted_files_for_date(PathDir,ext,StartDate=None,EndDate=None):
    
    ListFiles=get_all_files(PathDir,ext=ext)
    
    ListFilesSorted=sorted_files_list_for_date(ListFiles,StartDate,EndDate)
    return ListFilesSorted



def sorted_files_list_for_date(ListFiles,StartDate=None,EndDate=None):
    Dates={}
    for i, filename in enumerate(ListFiles):
        _,DateEnd,_=get_info_from_csv4d(filename)
        Dates[DateEnd]=i
    
    
    if StartDate is not None and EndDate is not None:
        ListFilesSorted=[ListFiles[Dates[f]] for f in sorted(Dates) if StartDate<=f<=EndDate]
    elif StartDate is not None and EndDate is None:
        ListFilesSorted=[ListFiles[Dates[f]] for f in sorted(Dates) if StartDate<=f]
    elif StartDate is None and EndDate is not None:
        ListFilesSorted=[ListFiles[Dates[f]] for f in sorted(Dates) if f<=EndDate]
    else:
        ListFilesSorted=[ListFiles[Dates[f]] for f in sorted(Dates)]
    return ListFilesSorted


    

def exist_file_with_ext(Path,ext):
    List_Files=get_all_files(Path,ext=ext)
    if len(List_Files)>0:
        return True
    else:
        return False
def get_registry_global(path_registry):
    with open(path_registry,'r') as f:
        info=f.readlines()
        info=[f.split(':')[1] for f in info]
        
        NameProyect=info[0].replace('\n','')
        StartDate=info[1].replace('\n','')
        n_cols=int(info[2].replace('\n',''))
        n_rows=int(info[3].replace('\n',''))
        x_min=float(info[4].replace('\n',''))
        x_max=float(info[5].replace('\n',''))
        y_min=float(info[6].replace('\n',''))
        y_max=float(info[7].replace('\n',''))
        cell_size=float(info[8].replace('\n',''))
        dist_x=float(info[9].replace('\n',''))
        dist_y=float(info[10].replace('\n',''))
        
        return NameProyect,StartDate,n_rows,n_cols,[x_min,x_max,y_min,y_max],cell_size,dist_x,dist_y
    
    
def check_data_in_csv4d(fileCSV4D):
    has_info=False
    with open(fileCSV4D,'r') as f:
        for i,line in enumerate(f):
            if i>2:
                has_info=True
                break
    return has_info
                
def update_registry_log(PathNameRegistryLog,PathIN_file):
    OnlyFileName, _ = os.path.splitext(os.path.basename(PathIN_file))
    date_now=ntu.datetime_to_str(datetime.now(),fmt='%Y-%b-%d %H:%M:%S')
    with open(PathNameRegistryLog,'a') as f:
        f.write('%s; Fecha %s\n'%(OnlyFileName,date_now))
        
def save_registry_global(PathOUTFile,Name,StartDate,info):
    
    with open(PathOUTFile,'w+') as f:
        f.write('Name_Proyect:%s\n'%(Name))
        f.write('Start_Date:%s\n'%(StartDate))
        f.write('n_cols:%d\n'%(info[0]))
        f.write('n_rows:%d\n'%(info[1]))
        f.write('x_min:%.5f\n'%(info[2]))
        f.write('x_max:%.5f\n'%(info[3]))
        f.write('y_min:%.5f\n'%(info[4]))
        f.write('y_max:%.5f\n'%(info[5]))
        f.write('cell_size:%.5f\n'%(info[6]))
        f.write('distancia_x:%.5f\n'%(info[7]))
        f.write('distancia_y:%.5f\n'%(info[8]))
    

def get_new_files_with_data2(ListFile, DicFile):
    NewListFile=[]
    for fileCSV4D in ListFile:
        if fileCSV4D not in  DicFile and check_data_in_csv4d(fileCSV4D) :
            DicFile[fileCSV4D]=0
            NewListFile.append(fileCSV4D)
    return NewListFile

def get_new_files_with_data(ListFiles,PathNameRegistry):
    '''
    Esta función almacena los path/namefile.ext de los archivos de la ListFiles que no estan 
    guardados en el registro
    ListFiles contiene los path/namefile.ext
    El registro contiene solo los nombres de los archivos sin la extensión
    '''
    NewListFile=[]
    if os.path.exists(PathNameRegistry):
        with open(PathNameRegistry,'r') as f:
            ListSaved=f.readlines()
            if len(ListSaved)>0:
                LastFile=ListSaved[-1].replace('\n','')
                _,EndDate,_=get_info_from_csv4d(LastFile)
                DicListSave={ls.replace('\n',''):0 for ls in ListSaved}
                for lf in ListFiles:
                    _,EndDate1,_=get_info_from_csv4d(lf)
                    if EndDate1>EndDate:
                        OnlyFileName, ext = os.path.splitext(os.path.basename(lf))
                        if OnlyFileName not in DicListSave and check_data_in_csv4d(lf):
                            NewListFile.append(lf)
            else:
                NewListFile=[lf  for lf in ListFiles if check_data_in_csv4d(lf)]
    else:
        NewListFile=[lf  for lf in ListFiles if check_data_in_csv4d(lf)]
        
    return NewListFile

# def get_new_files2(ListFiles,path_fileh5):
    
#     NewListFile=[]
#     ListDates=get_dates_from_H5(path_fileh5)
#     if len(ListDates)==0:
#         NewListFile=[lf for lf in ListFiles]
#     else:
#         last_date=ListDates[-1]
#         for lf in ListFiles:
#             _,datefile,_=get_info_from_csv4d(lf)
#             if last_date<datefile:
#                 NewListFile.append(lf)
#     return NewListFile

def get_files_for_time_interval(ListFiles,lastDate,intervalTime):
    
    NewListFile=[]
    for lf in ListFiles:
        _,datefile,_=get_info_from_csv4d(lf)
        if lastDate <= datefile:
            lastDate= lastDate + ntu.delta_time(lastDate, intervalTime)
            NewListFile.append(lf)
    return NewListFile
            
           
def update_registry(PathNameRegistry,PathIN_file):
    OnlyFileName, _ = os.path.splitext(os.path.basename(PathIN_file))
    with open(PathNameRegistry,'a') as f:
        f.write('%s\n'%(OnlyFileName))

def update_log(PathNameLog,newfilename):
    with open(PathNameLog,'r') as f:
        oldfilename=f.readlines()[-1].replace('\n','')
    with open(PathNameLog,'a') as f:
        f.write('%s\n'%(newfilename))
    return oldfilename

def create_csv_for_ibis(PathOut,filename,xyzg_list):
    '''
    Parameters
    ----------
    PathOut : string
        path de salida del a
    filename : string sin extensión
        nombre del archivo de salida
    xyzg_list : lista
        lista que contiene x,y,z,g
    Returns
    -------
    None.
    '''
    f=None
    i=0
    for i,xyzg in enumerate(xyzg_list):
        x,y,z,g=xyzg
        if i%500==0:
            if f is not None: f.close()
            Num=i//500
            f=open(PathOut+filename+'_'+str(Num)+'.csv','w')
            Header='X, Y, Z, Label, Description\n'
            f.write(Header)
        f.write("%.3f, %.3f, %.1f, Punto_%s, grupo_%s\n"%(x,y,z,str(i),str(g)))
        i=i+1
    f.close()

def get_info_radares(path_excel,radar,SheetName='radares'):
    import pandas as pd
    df=pd.ExcelFile(path_excel)
    info=df.parse(SheetName)
    
    NumRow=radar-1
    if not pd.isnull(info.iloc[NumRow][0]):
        x=float(info.iloc[NumRow][0])
        y=float(info.iloc[NumRow][1])
        z=float(info.iloc[NumRow][2])
        Name=str(info.iloc[NumRow][3])
        azi=float(info.iloc[NumRow][4])
        elev=float(info.iloc[NumRow][5])
        return x,y,z,Name,azi,elev
    

def get_info_security_alerts(path_excel,DateMin,SheetName='Alertas de Seguridad'):
    import pandas as pd
    df=pd.ExcelFile(path_excel)
    info=df.parse(SheetName)
    
    NumRow=0
    Alerts_list=[]
    # print(info.iloc[NumRow][0])
    cond=not pd.isnull(info.iloc[NumRow][0])
    print('longitud de datos info: ',len(info))
    for NumRow in range(len(info)):
        
        try:
            Date=ntu.str_to_datetime(str(info.iloc[NumRow][2]),'%d/%m/%Y %H:%M')
        except:
            try:
                Date=ntu.str_to_datetime(str(info.iloc[NumRow][2]),'%Y-%m-%d %H:%M')
            except:
                Date=ntu.str_to_datetime(str(info.iloc[NumRow][2]),'%Y-%m-%d %H:%M:%S')
        if Date>=DateMin:
            x=float(info.iloc[NumRow][11])
            y=float(info.iloc[NumRow][12])
            z=float(info.iloc[NumRow][13])
            Tipo=str(info.iloc[NumRow][4])
            Fecha=Date
            Alerts_list.append((x,y,z,Tipo,Fecha))
        
    return Alerts_list
    
    
    
def get_dates_from_pathnpz(path_npz):
    filename, _ = os.path.splitext(os.path.basename(path_npz))
    StartDate,EndDate=filename.split('_to_')
    StartDate=ntu.str_to_datetime(StartDate,'%Y_%b_%d-%H_%M')
    EndDate=ntu.str_to_datetime(EndDate,'%Y_%b_%d-%H_%M')
    
    return StartDate, EndDate

def get_dates_from_pathnpy(path_npy):
    #20200120_20_20
    f, _ = os.path.splitext(os.path.basename(path_npy))
    if f.count('_')!=4:
        EndDate=f[:4]+'_'+f[4:6]+'_'+f[6:]
        EndDate=EndDate.replace('__','_')
    else:
        EndDate=f
    return ntu.str_to_datetime(EndDate,'%Y_%m_%d_%H_%M')
def create_file(path_file):
    if not os.path.exists(path_file):
        with open(path_file,'w') as f:
            pass
        
def get_csv4d_for_Z(ListFiles,Date):
    for fileCSV4D in ListFiles:
        _,EndDate,_=get_info_from_csv4d(fileCSV4D)
        if EndDate>=Date:
            return fileCSV4D
        
def save_list_to_csv(PathFile,Data,header,sep=','):
    with open(PathFile+'.csv','w') as f:
        f.write(sep.join(map(str,header))+'\n')
        for data in Data:
            f.write('%s\n'%(sep.join(map(str,data))))


def get_names_datacubes(ListFiles):
    DiccNamesData={}
    NameSorted=[]
    DateBig=datetime(1900,1,1)
    for fileCSV4D in ListFiles:
        _,EndDate,Name=get_info_from_csv4d(fileCSV4D)
        if check_data_in_csv4d(fileCSV4D):
            if Name not in DiccNamesData :
                DiccNamesData[Name]=datetime(1900,1,1)
        
            if DiccNamesData[Name]<EndDate:
                DiccNamesData[Name]=EndDate
            if DateBig<EndDate:
                DateBig=EndDate
                
    for name in DiccNamesData:
        if DiccNamesData[name]>=DateBig:
            LastName=name
        else:
            NameSorted.append(name)
    NameSorted.append(LastName)
    
    return NameSorted

def get_files_for_datacube(PathIN,name=None):
    ListFiles=get_all_files(PathIN,ext='.csv4d')
    if name is None:
        return ListFiles
    else:
        NewListFile=[fileCSV4D for fileCSV4D in ListFiles if name in fileCSV4D]
        return NewListFile