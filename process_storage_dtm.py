# -*- coding: utf-8 -*-
import os
import sys
import time


from data_struct import DataCube
import new_tool_files as ntf
import new_tool_utility as ntu

def update_data_dtm(fileCSV4D,CuboDatosDTM):
    
    EndDate=ntf.get_info_from_csv4d(fileCSV4D)[1]
    x_min,y_min,n_cols,_,cell_size= CuboDatosDTM.get_parameters()
    XY=CuboDatosDTM.get_xy()

	XYZD=ntf.get_xyzd_from_csv4d(fileCSV4D,5)
	Z=np.empty(len(XY),dtype=np.float32)
	Z[:]=np.nan
	dist=[]
	for x,y,z,desp in XYZD:
	    i,j=ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size)
	    index=j+i*n_cols
	    Z[index]=z
	    xl,yl=XY[index,0],XY[index,1]
	    dist.append(np.sqrt((x-xl)**2 + (y-yl)**2))
    dist_max=np.nanmax(dist)
    dist_min=np.nanmin(dist)
    DataCubeDTM.set_data(EndDate,Z,lenCSV4D=len(XYZD),DistMaxMin=(dist_max,dist_min)) 


def run_process_dtm(PathIN,PathOUT):

    PathOUT1=PathOUT
    
    NameRegistry='read_csv4d_dtm.dat'
    NameRegistryLog='registry_csv4d_dtm.log'
    NameRegistryGlobal='Header_Data.csv'
    
    while True:
        ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
        if len(ListFiles)>1:
            NameSorted=ntf.get_names_datacubes(ListFiles)
            NameSortedStart=[name for name in NameSorted]
            break
        time.sleep(3)
    
    intervalTime=(1,8,0) # day, hour, minutes
    while True: 
        name=NameSortedStart[0]
        PathOUT=PathOUT1+name+'/'
        PathOUT2=PathOUT+'Data_DTM/'
        ntf.make_dir(PathOUT2)
        
        
        ListFiles=ntf.get_files_for_datacube(PathIN,name)
        NewListFiles=ntf.get_new_files_with_data(ListFiles,PathOUT2+NameRegistry)
        
        if len(NewListFiles)>1 and os.path.exists(PathOUT+NameRegistryGlobal):
            NewListFilesSorted=ntf.sorted_files_list_for_date(NewListFiles)
            
            fileCSV4D=NewListFilesSorted[0]
            filename_cubodatos=PathOUT+'Datacube/'+name+'.h5'
            filename_dtm=PathOUT2+name+'_DTM'+'.h5'
            if not os.path.exists(filename_dtm):
            	CuboDatos = DataCube(filename_cubodatos)
            	XY=CuboDatos.get_xy()
            	CuboDatosDTM=DataCube(filename_dtm)
            	CuboDatosDTM.set_xy(XY[:,0],XY[:,1])
            	update_data_dtm(fileCSV4D,CuboDatosDTM)
            	del CuboDatos               
            end=-1
            if name!=NameSortedStart[-1]:
                end=len(NewListFilesSorted)
                NameSortedStart.pop(0)
            else:
                ListFiles=ntf.get_all_files(PathIN,ext='.csv4d')
                NameSortedAux=ntf.get_names_datacubes(ListFiles)
                for n in NameSortedAux:
                    if n not in NameSorted:
                        NameSortedStart.append(n)
                  
            ListDates=get_dates_from_H5(filename_dtm)     
            lastDate=ListDates[-1] + ntu.delta_time(ListDates[-1], intervalTime)
                    
            NewListFiles=ntf.get_files_for_time_interval(NewListFilesSorted,lastDate,intervalTime)
            
            for fileCSV4D in NewListFiles:
                
                if NewListFilesSorted[-1]==fileCSV4D:
                    ntf.check_read_file(fileCSV4D)
                
                # ntf.update_registry(PathOUT2+NameRegistry,fileCSV4D)
                try:
                    update_data_dtm(fileCSV4D,CuboDatosDTM)
                except:
                    pass
        
       
        time.sleep(1)

if __name__=='__main__':
	
	if len(sys.argv)>1:
        PathIN=sys.argv[1]
        PathOUT=sys.argv[2]
	run_process_dtm(PathIN,PathOUT)