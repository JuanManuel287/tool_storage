# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 16:08:37 2020

@author: joliva
"""
from datetime import timedelta, datetime
import numpy as np

def find_time_movil(TW,DeltaTime=timedelta(minutes=30)):
   
    time_start=TW[-1]-DeltaTime
    subTW=TW[(time_start<=TW)]
    StartIndex=int(np.argwhere(TW==subTW[0]))
    EndIndex=-1
   
    return StartIndex,EndIndex,subTW

def get_velocity_media_movil(TW,TS,DeltaTime):
    StartIndex,EndIndex,subTW=find_time_movil(TW,DeltaTime)
    Vel=(TS[:,EndIndex]-TS[:,StartIndex])*3600/(subTW[-1]-subTW[0]).total_seconds()
    return Vel
    


