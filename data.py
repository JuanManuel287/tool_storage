# -*- coding: utf-8 -*-
"""
Created on Sun Jan 12 13:09:07 2020

@author: JOliva
"""
import os
import datetime as dt
import numpy as np
import h5py
import time

from data_struct import DataCube
import new_tool_utility as ntu
import new_tool_graphic as ntg
import new_tool_files as ntf
import numba 
from tensorflow.keras.models import load_model
import copy

import gc



#@numba.jit(nopython=True)
def update_time_serie(TS,displacement):
    TS[:,:-1]=TS[:,1:]
    TS[:,-1]=displacement
    indexs=np.where(np.isnan(TS[:,-1]))[0]
    if len(indexs)>0:
        TS[indexs,-1]=TS[indexs,-2]
    #return TS

#@numba.jit(nopython=True)
def traslation_time_serie(TS):
    return np.transpose(np.subtract(np.transpose(TS), TS[:,0]))
    #for k in range(len(TS)):
    #    TS[k]=TS[k]-TS[k,0]
    #return TS

#@numba.jit(nopython=True)
def update_and_traslation_time_serie(TS,ts):
    TS=update_time_serie(TS,ts)
    newTS=traslation_time_serie(TS)
    return newTS,TS
def count_nan(TS):
    n,m=TS.shape
    cont=np.zeros(n)
    for j in range(m):
        cont=cont+np.where(np.isnan(TS[:,j]),0,1)
    return cont
def get_indexs_nonan(TS):
    if len(TS.shape)>1:
        return [k for k in range(len(TS)) if ~np.isnan(TS[k,:]).all()]
    else:
        return np.where(~np.isnan(TS[:]))[0]


def complete_nan_time_serie(TS):
    t1=time.time()
    index_nonan=get_indexs_nonan(TS)
    print('Tiempo en buscar los nonan : ',time.time()-t1)
    newTS=TS[index_nonan]
    
    
    # t1=time.time()
    # indexs_nan=np.where(np.isnan(newTS[:,0]))[0]
    # for k in indexs_nan:
    #     i=np.where(~np.isnan(newTS[k,:]))[0][0]
    #     newTS[k,0]=newTS[k,i]
    # print('tiempo en cambiar nan de la primera columna con valores: ',time.time()-t1)
       
   
    t1=time.time()
    mask = np.isnan(newTS)
    idx = np.where(~mask,np.arange(mask.shape[1]),0)
    np.maximum.accumulate(idx,axis=1, out=idx)
    newTS[mask] = newTS[np.nonzero(mask)[0], idx[mask]]
    TS[index_nonan]=newTS
    del newTS, mask, idx,index_nonan
    gc.collect()
    #return TS

# @numba.jit(nopython=True)
def complete_nan(XY,TS):
    t1=time.time()
    index_nonan=[k for k in range(len(TS)) if ~np.isnan(TS[k,:]).all()]
    print('Tiempo en buscar los nonan : ',time.time()-t1)
    newTS=TS[index_nonan]
    newXY=XY[index_nonan]
    
    
    t1=time.time()
    indexs_nan=np.where(np.isnan(newTS[:,0]))[0]
    for k in indexs_nan:
        i=np.where(~np.isnan(newTS[k,:]))[0][0]
        newTS[k,0]=newTS[k,i]
    print('tiempo en cambiar nan de la primera columna con valores: ',time.time()-t1)
       
    # k_INDEX_nan=[[k,[i[0] for i in np.argwhere(np.isnan(TS[k,:]))]] for k in range(len(TS))]
   
    # t1=time.time()
    # for k, indexs_nan in k_INDEX_nan:
    #     for j in indexs_nan:
    #         TS[k,j]=TS[k,j-1]
    #     TS[k]=TS[k]-TS[k,0]
    t1=time.time()
    mask = np.isnan(newTS)
    idx = np.where(~mask,np.arange(mask.shape[1]),0)
    np.maximum.accumulate(idx,axis=1, out=idx)
    newTS[mask] = newTS[np.nonzero(mask)[0], idx[mask]]

    return newXY,newTS


# def format_time_serie(XY,TS):
#     newXY,newTS=complete_nan(XY,TS)
#     TimeSerieInit=newTS[:,0]
#     #newTS=traslation_time_serie(newTS)
#     print('timepo en completar los nan por los valores anteriores: ',time.time()-t1)

#     return newXY,newTS,TimeSerieInit


class TimeSerie(DataCube):
    def __init__(self,path):
        DataCube.__init__(self,path)

    def get_displacement_xy_for_dates(self, StartDate, Nsamples=1,EndDate=None,indexs=None):
        dates=self.get_dates()
        try :
            if dates[0]>StartDate:
                print('La fecha StartDate es menor que: ', dates[0])
            NewStartDate,StartIndex=ntu.find_date_nearest(StartDate,dates)
            
            if EndDate is None and len(dates[StartIndex:])<Nsamples:
                print('contiene menos muestras que : ',Nsamples)
                EndIndex=len(dates)
            elif EndDate is None and len(dates[StartIndex:])>=Nsamples:
                EndIndex=StartIndex+Nsamples
                
            elif EndDate is not None and len(dates[StartIndex:])>=Nsamples:
                if EndDate >dates[-1]:
                    print('La fecha EndDate es mayor que: ', dates[-1])
                NewEndDate,EndIndex=ntu.find_date_nearest(EndDate,dates)
                EndIndex=EndIndex+1
            
            
            TW=[d for d in dates[StartIndex:EndIndex]]
            NameCols=[ntu.datetime_to_str(d) for d in TW]
            
            if indexs is None:
                with h5py.File(self._path, 'r',libver='latest', swmr=True) as  hf:
                    TimeSerie=np.array(hf.get(NameCols[0]),dtype=np.float32)
                    for Name in NameCols[1:]:
                        d=np.array(hf.get(Name),dtype=np.float32)
                        TimeSerie=np.vstack((TimeSerie,d))
                    TimeSerie=np.transpose(TimeSerie)
            else:
                
                with h5py.File(self._path, 'r',libver='latest', swmr=True) as  hf:
                    TimeSerie=np.array(hf.get(NameCols[0]),dtype=np.float32)[indexs]
                    for Name in NameCols[1:]:
                        d=np.array(hf.get(Name),dtype=np.float32)[indexs]
                        TimeSerie=np.vstack((TimeSerie,d))
                    TimeSerie=np.transpose(TimeSerie)
            return TimeSerie,TW
        except:
            if dates[-1]<StartDate:
                print('La fecha inicial es mayor que la ultima fecha del cubo de datos')
    
                
    

    def get_time_series(self,XY,StartDate,EndDate,indexs=None):
        t1=time.time()
        if indexs is None:
            indexs=self.get_INDEX_nearest_point(XY[:,0],XY[:,1])
        #print('tiempo en crear index ts: ',time.time()-t1)
        t1=time.time()
        TS,TW=self.get_subdata_block(StartDate,EndDate,indexs=indexs)
        #print('tiempo sub ts: ',time.time()-t1)
        return TS,TW

    def get_time_serie_complete_nan(self,XY,StartDate,EndDate):

        TS,TW=self.get_time_series(XY,StartDate,EndDate)
        t1=time.time()
        newXY,newTS=complete_nan(XY,TS)
        return newXY,newTS,TW

    def get_time_serie_for_eval(self,XY,StartDate,EndDate):
        newXY,TS,TW=self.get_time_serie_complete_nan(XY,StartDate,EndDate)
        newTS=traslation_time_serie(TS)
        #print('tiempo format ts: ',time.time()-t1)
        return newXY,newTS,TW

    def get_time_serie_for_eval_sequential(self,XY,StartDate,EndDate,Nsamples=144,pathRec=''):
        dates=self.get_dates()
        StartDate,StartIndex=ntu.find_date_nearest(StartDate,dates)
        EndDate,EndIndex=ntu.find_date_nearest(EndDate,dates)
        
        NewStartDate=dates[StartIndex-Nsamples+1]
        NewEndDate=dates[StartIndex]
        
        all_indexs=self.get_INDEX_nearest_point(XY[:,0],XY[:,1])
        t1=time.time()
        all_TS,TW=self.get_time_series(XY,NewStartDate,NewEndDate,indexs=all_indexs)
        if os.path.exists(pathRec):
            filenpz=np.load(pathRec,allow_pickle=True)
            all_TS[:,0]=filenpz['arr_0']
            filenpz.close() 
        
        del TW
        print('Tiempo en obtener all_TS: %.2f'%(time.time()-t1))
        t1=time.time()
        complete_nan_time_serie(all_TS)
        Cond=count_nan(all_TS)
        print('Tiempo en completar y contar los nan: %.2f'%(time.time()-t1))

    
        #indexs=self.get_INDEX_nearest_point(newXY[:,0],newXY[:,1])

        for i in range(StartIndex,EndIndex+1):
            NewStartDate=dates[i-Nsamples+1]
            NewEndDate=dates[i]
            
            t1=time.time()
            #indexs_nonan=get_indexs_nonan(all_TS[:,-1])
            indexs_nonan=np.where(Cond==Nsamples)[0]
            #print('tiempo en obtener los indices nonan: %.2f'%(time.time()-t1))
            t1=time.time()
            newTS=traslation_time_serie(all_TS[indexs_nonan])
            #print('tiempo en hacer la traslacion : %.2f'%(time.time()-t1))
            displacement=self.get_subdata(XY,NewEndDate,indexs=all_indexs)#-TimeSerieInit
            t1=time.time()
            update_time_serie(all_TS,displacement)
           #print('tiempo en actualizar all_TS: %.2f'%(time.time()-t1))
            t1=time.time()
            np.savez(pathRec,all_TS[:,0])
            #print('tiempo en guardar: %.2f'%(time.time()-t1))

            t1=time.time()
            indexs=np.where(~np.isnan(all_TS[:,-1]))[0]
            Cond[indexs]=Cond[indexs]+1
            
            indexs=np.where(np.isnan(all_TS[:,-1]))[0]
            Cond[indexs]=Cond[indexs]-1
                
            Cond[Cond<0]=0
            Cond[Cond>Nsamples]=Nsamples
            #print('tiempo en actualizar Cond: %.2f'%(time.time()-t1))
            del indexs,displacement
            gc.collect()
            yield XY[indexs_nonan],newTS,dates[i-Nsamples+1:i+1],indexs_nonan

    

        

class Probability(DataCube):
    def __init__(self,path,nameTSP,pathmodel,namemodel):
        ntf.make_dir(path+'/'+namemodel+'/')
        pathDataRecover=path+'/'+namemodel+'/data_recover.npz'
        path=path+'/'+namemodel+nameTSP
        DataCube.__init__(self,path)
        self._pathDataRecover=pathDataRecover
        self._pathmodel=pathmodel
        self._namemodel=namemodel
        #self.set_pathmodel(pathmodel,namemodel)
        

    def get_pathmodel(self):
        with h5py.File(self._path, 'r',libver='latest', swmr=True) as hf:
            data=np.array(hf.get('model'),dtype=np.str)
        
        self._pathmodel=data
        self._namemodel=data
        return self._pathmodel,self._namemodel


    def set_pathmodel(self,pathmodel,namemodel):
        with h5py.File(self._path, 'r',libver='latest', swmr=True) as  hf:
            Listakey=list(hf.keys())
        if 'model' in Listakey:
            self.get_pathmodel()
        else:
            self._pathmodel=pathmodel
            self._namemodel=namemodel
            data=[pathmodel,namemodel]
            print('data:',data)
            with h5py.File(self._path, 'a') as  hf:
                hf.create_dataset('model', (len(data),1),'|S24',data)

    def create_ts_probability_from_ts_TimeSerie(self,CDTimeSerie):
        model=load_model(self._pathmodel+self._namemodel+'.h5')
        XY=CDTimeSerie.get_xy()
        self.set_xy(XY[:,0],XY[:,1])
        dates=CDTimeSerie.get_dates()
        datesProb=self.get_dates()
        #self.check_data_status()
        StartDate=dates[143]
        if len(datesProb)>0:
            print('ultima fecha guardada: %s'%(datesProb[-1]))
            StartDate=dates[dates>=datesProb[-1]][0]


        print(StartDate)
        EndDate=dates[-1]
        t1=time.time()
        XY_TS_TW_INDEXS=CDTimeSerie.get_time_serie_for_eval_sequential(XY,StartDate,EndDate,pathRec=self._pathDataRecover)
        print('tiempo en obtener toda las time serie como generador: ',time.time()-t1)
        t2=time.time()
        data=np.empty(len(XY),dtype=np.float16)
        
        for XY_eval, TS_eval, TW_eval, indexs  in XY_TS_TW_INDEXS:
            error=True
            while error:
                try:
                    t1=time.time()
                    
                    data[:]=np.nan
                    Date=TW_eval[-1]
                    
                    data[indexs]=model.predict(TS_eval)[:,1]
                    self.set_data(Date,data,dtype=np.float16)
                    print('tiempo en un ciclo:%.1f '%(time.time()-t1), ', Num celdas: %d, fecha:%s, momento:%s'%(len(XY_eval),Date,dt.datetime.now()))
                    gc.collect()
                    error=False
                except:
                    pass
           

        print('tiempo total: ',time.time()-t2)



    def get_sub_ts_probability(self,XY,StartDate,EndDate):
        indexs = self.get_INDEX_nearest_point(XY[:,0],XY[:,1])
        PM, TW = self.get_subdata_block(StartDate,EndDate,indexs=indexs)
        return PM, TW

    def get_number_cell_above_threshold(self,XY,StartDate,EndDate,threshold=0.9):

        TSProbablity, TW=self.get_sub_ts_probability(XY,StartDate,EndDate)
        

        # index_nonan=[k for k in range(len(PM)) if ~np.isnan(PM[k,:]).all()]
        # if len(index_nonan)>0:
        #     PM=PM[index_nonan]
        #     XY=XY[index_nonan]
        NCells=[]

        n,m = TSProbablity.shape
        for v in np.transpose(TSProbablity):
            NCells.append(len(v[v>threshold]))
        NCells=np.array(NCells,dtype=np.int32)
        return NCells,TW







    
        

        
if __name__=='__main__':
    if False:
        path=r"C:\Users\JOliva\Documents\DataOUT\IBIS_2\MASTER_MLP_24122019_IBIS-2_MLP_24122019\Data Cube"
        nameCD=r"\MASTER_MLP_24122019_IBIS-2_MLP_24122019.h5"
    else:
        if False:
            path=r"C:\EMT\Data_OUT\IBIS_2\MASTER_MLP_18012020_IBIS-2_MLP_24122019\Data Cube"
            nameCD=r"\MASTER_MLP_18012020_IBIS-2_MLP_24122019.h5"
        else:
            path=r"D:\Respaldo OUT\IBIS_2\MASTER_MLP_18012020_IBIS-2_MLP_24122019\Data Cube"
            nameCD=r"\MASTER_MLP_18012020_IBIS-2_MLP_24122019.h5"
    

    PathModel='models/'
    NameModel='MMLP_Media_EV_1-2-3_Nobalanced'
    path_probability=path.replace('Data Cube','Results')
    nameTSProb=nameCD.replace('.','_TSProb.')
    TSProbability=Probability(path_probability,nameTSProb,PathModel,NameModel)
    # print(TSProbability.get_pathmodel())

    CuboDatos=TimeSerie(path+nameCD)
    
    TSProbability.create_ts_probability_from_ts_TimeSerie(CuboDatos)
    print(CuboDatos)
    dates=CuboDatos.get_dates()
    print('Total de fechas: ',len(dates))
    print('Fecha de inicio: ',dates[0])
    print('Fecha de final : ',dates[-1])
    
    print('Radar:',CuboDatos.get_radar())
    t1=time.time()
    XY=CuboDatos.get_xy()
    print('tiempo en obtener XY del Cubo de datos: ',time.time()-t1)
    print(XY.shape)
    
    xc,yc=58395.2,91166.5
    radio = 15
    TS,TW=CuboDatos.get_displacement_xy_for_dates(dates[0],Nsamples=2)
    
    print('Dimension TS: ',TS.shape)
    indexs=np.where(~np.isnan(TS))[0]
    # XY_eval=XY[indexs]
    XY_eval=XY
    if True:
        indexs=[i for i,xy in enumerate(XY) if abs(xy[0]-xc)<=radio and  abs(xy[1]-yc)<=radio]
        XY_eval=XY[indexs]
    
    
    print('Numero de indexs inicial: ',len(XY_eval))
    t2=time.time()
    StartDate=dt.datetime(2020,2,13,23,0)#dates[-144]#
    EndDate=dt.datetime(2020,2,14,12,0)#dates[-1]#

    if True:

        import matplotlib.pyplot as plt
        import matplotlib as mpl
        mpl.use('Qt5Agg')
        
        XY_eval,TS_eval,TW_eval=CuboDatos.get_time_serie_for_eval(XY_eval,StartDate,EndDate)
        ts_min=np.nanmin(TS_eval)-2
        ts_max=np.nanmax(TS_eval)+2
        
        fig,ax,ax2D=ntg.create_graph_TS_2D(ts_min,ts_max,TW_eval,'Example','%H:%M\n%d-%b')



        for ts in TS_eval:
            ax.plot(TW_eval,ts)
        plt.show
        plt.pause(5)

    #print('tiempo en obtener sub time serie para evaluar: ',time.time()-t2)
    
    StartDate,StartIndex=ntu.find_date_nearest2(StartDate,dates)
    EndDate,EndIndex=ntu.find_date_nearest2(EndDate,dates)
    TW_global=dates[StartIndex:EndIndex+1]
    # t1=time.time()
    model = load_model(PathModel+NameModel+'.h5')
    print(type(model),model)


    
    # results=model.predict(TS_eval)
    # print('Tiempo en evaluar modelo: ',time.time()-t1)
    # print('Tiempo en obtener y evaluar modelo: ',time.time()-t2)
    t1=time.time()
    SecuenciaTimeSerie=CuboDatos.get_time_serie_for_eval_sequential(XY_eval,StartDate,EndDate)
    print('Tiempo en generar time series secuencial: ',time.time()-t1)
    
    
    results=[]

    PathDir='IMG/'
    if not os.path.exists(PathDir):
        os.makedirs(PathDir)

    import matplotlib.pyplot as plt
    import matplotlib as mpl
    mpl.use('Qt5Agg')
    
    ts_min=np.nanmin(TS_eval)-2
    ts_max=np.nanmax(TS_eval)+2
    
    fig,ax,ax2D=ntg.create_graph_TS_2D(ts_min,ts_max,TW_eval,'Example','%H:%M\n%d-%b')
    # fig,ax=ntg.Set_Properties_Graphic(fmtEjeX='%H:%M\n%d-%b',Num=10)
    
        # 
        

    # for r in TS_eval:
    #     ax.plot(TW_eval,r)
    #fig2, ax2 = plt.subplots()
    # t2=time.time()
    # for i,data in enumerate(SecuenciaTimeSerie):
    #     XY_eval,TS_eval,TW_eval,_ = data


    #     t1=time.time()
    #     prob=model.predict(TS_eval)[:,1]
    #     results.append(prob)
        
    #     print('tiempo en evaluar en el modelo la time serie : ',time.time()-t1)

    #     if i==0:
    #         s=310.84577978/radio
    #         Labels=[str(x)+'-'+str(y) for x,y in XY]
    #         ntg.graph_2D_only_xy(ax2D,XY_eval,Labels,s=s,linewidths=s)

    #     ts_min=np.nanmin(TS_eval)-2
    #     ts_max=np.nanmax(TS_eval)+2

    #     ntg.set_properties_graphic_TS(ax,ts_min,ts_max)
    #     ax.set_xlim([TW_eval[0],TW_eval[-1]])
    #     for ts in TS_eval:
    #         ax.plot(TW_eval,ts)
    #     #print('fecha min: %s, max: %s, num fechas: %d '%(TW_eval[0],TW_eval[-1],len(TW_eval)))

    #     fig.savefig(PathDir+'%s.png'%(i))
    #     ax.clear()
    #     print('min: %.2f ,  max: %.2f '%(ts_min,ts_max))

    # print('Tiempo en obtener time serie en varios instantes: ',time.time()-t2)

    # results=np.array(results).T

    
    
    # Labels=[str(x)+'-'+str(y) for x,y in XY]
    # print('results.shape: ',results.shape)
    # print('len(TW_global: ',len(TW_global))
    # for r in results:
    #     ax.plot(TW_eval,r)
    
    #ntg.graph_2D_only_xy(ax2D,XY_eval,Labels,s=s,linewidths=s)
    
    
    #print('Numero de indexs final: ',len(XY_eval))
    
    
    
   
    
    
    
    
    # plt.plot(TS_eval)
    # plt.figure(2)
    # fig, ax=ntg.create_graph_2D('title')
    # ax.scatter(XY[indexs,0],XY[indexs,1],marker='s',c='silver',s=3,linewidths=1,alpha=0.8)
    # plt.figure(3)
    # x=np.array(range(0,10))
    # y=np.nan*x
    # plt.plot(x,y)
    # plt.plot(x,np.sin(x))
    
    
    
    
    

    
            
            
            
            
            
            
            
        

        
    
        
        
        