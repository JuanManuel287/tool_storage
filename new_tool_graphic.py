# -*- coding: utf-8 -*-
"""
Created on Sun Sep 29 03:22:04 2019

@author: juan oliva 2
"""

import matplotlib.pyplot as plt
from matplotlib import dates
import matplotlib as mpl

import new_tool_utility as ntu
import new_tool_files as ntf
import tool_cluster as tc

import numpy as np
from mpl_toolkits.mplot3d import Axes3D 
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib.backends.backend_pdf import PdfPages
import time

import gc


def set_properties_graphic_TS(ax,ymin,ymax,fmtEjeX='%H:%M',fontsize=14,Num=10,ylabel='mm'):
    
    hfmt = dates.DateFormatter(fmtEjeX)
    ax.xaxis.set_major_formatter(hfmt)
    ax.xaxis.set_major_locator(plt.MaxNLocator(Num))
    ax.set_ylabel(ylabel)
    ax.set_ylim([ymin,ymax])
    ax.grid(True)
    
def Set_Properties_Graphic(n=1,m=1,figsize=(14,8),fmtEjeX='%H:%M',fontsize=14,Num=10):
    
    fig, axs = plt.subplots(n, m,figsize=figsize)
    
    hfmt = dates.DateFormatter(fmtEjeX)
    
    if m>1:
        if n==1:
            for i in range(0,m):
                axs[i].xaxis.set_major_formatter(hfmt)
                axs[i].xaxis.set_major_locator(plt.MaxNLocator(Num))
                axs[i].grid(True)
            
        else:
            for i in range(0,n):
                for j in range(0,m):
                    axs[i,j].xaxis.set_major_formatter(hfmt)
                    axs[i,j].xaxis.set_major_locator(plt.MaxNLocator(Num))
                    axs[i,j].grid(True)
    else:
        if n>1:
            for i in range(0,n):
                axs[i].xaxis.set_major_formatter(hfmt)
                axs[i].xaxis.set_major_locator(plt.MaxNLocator(Num))
                axs[i].grid(True)
        else:
            axs.xaxis.set_major_formatter(hfmt)
            axs.xaxis.set_major_locator(plt.MaxNLocator(Num))
            axs.grid(True)
            
            
        

    return fig,axs


def Make_GraphBar(PathSave,yData,xData,title,xlabel,ylabel,legends,width=0.5,
                  colors=[]):

    plt.figure(figsize=(14,8))
    
    n,m=np.shape(yData)
    

    x = np.arange(1,2*m+1,2)
    dist=width*n/2
    #x=np.linspace(1,(m+1)*1.5,1.5)
    
    if colors!=[]:
        for i in range(0,n):
            plt.bar(x - dist + width*i, yData[i],color =colors[i%m],width = width)
    else:
        for i in range(0,n):
            plt.bar(x, yData[i],width = width)
        
    
    plt.legend(legends)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.xticks(x, [str(x) for x in xData],rotation=45)
    plt.savefig(PathSave+'.png',dpi=300)
    
def get_colors_map(umbrals_list,colors_list):
    n_pal=len(umbrals_list)-1
    cmap = mpl.colors.ListedColormap(colors_list)
    norm = mpl.colors.BoundaryNorm(umbrals_list, cmap.N)
    cmap1 = cmap(np.linspace(0,1,n_pal))
    return cmap,cmap1,norm

def normalize_array_to_colors(M_array,norm):
    return norm(M_array)
    
    
        
def create_colors_map(INFO):
    _,_,n_rows,n_cols,_,_,_,_=INFO
    
    interval=[0.0,0.5,0.7,0.9,1.001]
    n_pal=len(interval)-1
    cmap = mpl.colors.ListedColormap(['limegreen','yellow','orange', 'red'])
    norm = mpl.colors.BoundaryNorm(interval, cmap.N)
    cmap1 = cmap(np.linspace(0,1,n_pal))
    white=[1.,1.,1.,1.]
    color_map=np.array([[white for _ in range(n_cols)] for _ in range(n_rows)])
    
    return norm,cmap,cmap1,color_map,interval
    
def create_graph_2D(title=None,figsize=(20,14)):
    
    fig, ax2D = plt.subplots(figsize=figsize,constrained_layout=True)
    if title is not None:
        fig.suptitle(title,fontsize=15)
        
    set_arrow_north(ax2D)
   
    return fig, ax2D

def create_graphs_2D(n=1,m=1,title=None,figsize=(20,14)):

    fig, ax2D = plt.subplots(n,m,figsize=figsize,constrained_layout=True)
    if title is not None:
        fig.suptitle(title,fontsize=15)
        
   
    return fig, ax2D

def create_graph_3D(title):
    fig= plt.figure(figsize=(20,14))
    ax3D = fig.add_subplot(projection='3d')
    
    ax3D.set_xlabel('E')
    # ax3D.set_ylabel('N $\longleftarrow$',fontsize=18)
    ax3D.set_ylabel('N',fontsize=18)
    fig.suptitle(title,fontsize=15)
    return fig, ax3D

def create_graph_2D_3D(title):
    fig = plt.figure(figsize=(20,14))
    ax2D = fig.add_subplot(1, 2, 1)
    ax2D.annotate('N', xytext=(1.05, 1),xy=(1.05, 0.9),ha= 'center',fontsize=14, xycoords='axes fraction', 
            arrowprops=dict(arrowstyle="<-", color='k',shrinkB=10,lw=3))
    ax3D = fig.add_subplot(1, 2, 2, projection='3d')
    fig.suptitle(title,fontsize=15)
    return fig,ax2D,ax3D
    

def create_graph_TS_2D(ymin,ymax,TW,title,fmtEjeX,figsize=(14,8)):
    n=1
    m=2
    fig,axs=Set_Properties_Graphic(n=n,m=m,figsize=figsize,fmtEjeX=fmtEjeX)
    
    for i in range(0,n):
        axs[i].set_ylim([ymin,ymax])
        axs[i].set_xlim([TW[0],TW[-1]])
        axs[i].set_ylabel('mm')
    
    
    axs[0].legend(loc='upper right')
    
    gs1 = axs[1].get_gridspec()
   
    axs[1].remove()
    ax2D = fig.add_subplot(gs1[1])
    
    
    
    fig.suptitle(title,fontsize=12)
    # fig.set_constrained_layout(True)
    return fig,axs[0],ax2D


def create_graph_TS_2D_3D(ymin,ymax,TW,title,n=4,m=3,figsize=(22,10)):
    fig,axs=Set_Properties_Graphic(n=n,m=m,figsize=figsize)

    for i in range(0,n):
        axs[i,0].set_ylim([ymin,ymax])
        axs[i,0].set_xlim([TW[0],TW[-1]])
        axs[i,0].set_ylabel('mm')
        
    
    axs[0,0].legend(loc='upper right')
    
    gs1 = axs[0, 1].get_gridspec()
    gs2 = axs[0, 2].get_gridspec()
    
    for i in range(1,m):
        for ax in axs[:, i]:
            ax.remove()
    ax2D = fig.add_subplot(gs1[:, 1])
    ax2D=set_arrow_north(ax2D)
    ax3D = fig.add_subplot(gs2[:, 2],projection='3d')
    
    # ax3D.set_ylabel('N $\longleftarrow$',fontsize=18)
    ax3D.set_ylabel('N',fontsize=18)
    fig.suptitle(title,fontsize=15)
    return fig,axs,ax2D,ax3D

def insert_point_graph_2D(ax,x,y,r=30,text=None,marker='o',ms=1,mc='k',colorCircle='k',fontsize=10):
    ax.plot(x, y, marker=marker,color=mc,ms=ms,alpha=0.6)
    if text is not None:
        ax.text(x, y,text,fontsize=fontsize,color='k')
    if r>0:
        theta=np.linspace(0,2*np.pi,100)
        ax.plot(x+r*np.cos(theta), y+r*np.sin(theta),ls='--',color=colorCircle,alpha=0.9)

def insert_point_graph_3D(ax,x,y,z,text=None,s=60,color='k',fontsize=10):
    ax.scatter(x, y, z, marker='o',color=color,s=s,alpha=0.3)
    if text is not None:
        ax.text(x,y,z, text, color='k',fontsize=fontsize)   
    
def init_point_graph_2D(ax,X,Y,Z):
    indexs=np.argwhere(~np.isnan(Z))
    XY_array=np.array([(X[i,j],Y[i,j]) for i,j in indexs],
                      dtype=np.float32)
    
    ax.scatter(XY_array[:,0],XY_array[:,1],marker='s',c='silver',s=0.25,
               linewidths=0.25,alpha=0.8)
    
def set_arrow_north(ax):
    ax.annotate('N', xytext=(1.05, 1),xy=(1.05, 0.90),ha= 'center',fontsize=14, xycoords='axes fraction', 
            arrowprops=dict(arrowstyle="<-", color='k',shrinkB=10,lw=3))
    return ax


    
def graph_2D_only_xy(ax,XY,Labels,s=0.25,linewidths=0.25):
    for xy,label in zip(XY,Labels):
        x,y=xy
        ax.plot(x,y,label=label,marker='s', c='green',ms=s,lw=linewidths,alpha=0.8)

    ax.grid(linewidth=1)
    ax.set_axisbelow(True)
    ax.set_xlabel('E')
    ax.set_ylabel('N')
    ax.yaxis.set_label_position("right")
    ax.yaxis.set_ticks_position('right')
    ax=set_arrow_north(ax)
    ax.set_aspect('equal','box')   

    
    
    
def Graph_Point2D(ax,XY_eval,colors_array,s=0.25,linewidths=0.25):
    ax.scatter(XY_eval[:,0],XY_eval[:,1],marker='s',c=colors_array,s=s,linewidths=linewidths,alpha=0.8)
        
    ax.grid(linewidth=1)
    ax.set_axisbelow(True)
    
    ax.set_aspect('equal','box')   
    ax.set_xlabel('E')
    ax.set_ylabel('N')

    
def set_colorbar_graph2D(fig,ax,cmap,norm,interval):
    ax.yaxis.set_label_position("right")
    ax.yaxis.set_ticks_position('right')
    
    axins = inset_axes(ax,width=0.12,height='100%', loc='lower left',
                       bbox_to_anchor=(-.04, 0., 1, 1),bbox_transform=ax.transAxes,
                       borderpad=0)
    
    clb=fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax,ticks=interval,
                 spacing='proportional',orientation='vertical',cax=axins)
    clb.ax.set_title('Probabilidad')
    clb.ax.yaxis.set_ticks_position('left')
    

def Graph_Point3D(fig,ax,X,Y,Z,CellsColors,azim,elev):
    t1=time.time()

    indexs=np.argwhere(~np.isnan(Z))
    imin,imax=np.min(indexs[:,0]),np.max(indexs[:,0])
    jmin,jmax=np.min(indexs[:,1]),np.max(indexs[:,1])
    XX=X[imin:imax+1,jmin:jmax+1]
    YY=Y[imin:imax+1,jmin:jmax+1]
    ZZ=Z[imin:imax+1,jmin:jmax+1]
    CC=CellsColors[imin:imax+1,jmin:jmax+1]
    print('Tiempo en hacer submatriz: ',time.time()-t1)
    
    surf=ax.plot_surface(XX, YY, ZZ,cstride=1,rstride=1,facecolors=CC,linewidth=0, 
                         antialiased=True, shade=True)
   
    # t1=time.time()
    # n,m,k=CC.shape
    # surf.set_facecolor(CC.reshape(n*m,4))
    # print('Tiempo aplicar funcion facecolor: ',time.time()-t1)
    
    
    # ax.view_init(azim=-73,elev=24.)
    ax.view_init(azim=azim,elev=elev)
    ax.margins(x=0.1, y=-0.1)
    
def do_graph_TS_2D_3D(num,xc,yc,zc,ic,jc,radio,Ncells,title,colors_array,TW,TS,XY,X,Y,Z,IJ,IndexColor,
                      CellsColors,DataColors_list,azi,elev):
    
    norm,cmap,cmap1,interval=DataColors_list
    n_color=len(cmap1)
    Contador_ClasTS=n_color*[0]

    imin,imax=ic-Ncells,ic+Ncells
    jmin,jmax=jc-Ncells,jc+Ncells
    
    imin1,imax1=ic-2*Ncells,ic+2*Ncells
    jmin1,jmax1=jc-2*Ncells,jc+2*Ncells
    
    XX=X[imin1:imax1+1,jmin1:jmax1+1]
    YY=Y[imin1:imax1+1,jmin1:jmax1+1]
    ZZ=Z[imin1:imax1+1,jmin1:jmax1+1]
    # CellsColors[imin:imax,jmin:jmax+1]
    white=[1.,1.,1.,1.]
    CC=np.array([[CellsColors[i,j] if imin<=i<=imax and  jmin<=j<=jmax else white for j in range(jmin1,jmax1+1)] for i in range(imin1,imax1+1)])
    
    indexs=[i for i,xy in enumerate(XY) if abs(xy[0]-xc)<=radio and  abs(xy[1]-yc)<=radio]
    
    ts_min=np.nanmin(TS[indexs])-2
    ts_max=np.nanmax(TS[indexs])+2
    
    fig,axs,ax2D,ax3D=create_graph_TS_2D_3D(ts_min,ts_max,TW,title)
    
    t1=time.time()
    for index,ts in zip(IndexColor[indexs],TS[indexs]):
        Contador_ClasTS[index]=Contador_ClasTS[index]+1
        pos_axes=n_color - 1 - index
        axs[pos_axes,0].plot(TW,ts,color=cmap1[index])
    
    print('tiempo en asignar valores a MP, CellsColor y graficar ts : ', time.time()-t1)
    axs[len(cmap1) - 1,0].set_title('N° CNR  : %d'%(Contador_ClasTS[0]))
    
    for i in range(0,n_color-1):
        axs[i,0].set_title('N° CR  : %d'%(Contador_ClasTS[n_color-1-i]))
    

    t1=time.time()
    init_point_graph_2D(ax2D,XX,YY,ZZ)
    
    Graph_Point2D(ax2D,XY[indexs],colors_array[indexs],s=3,linewidths=3)
    set_colorbar_graph2D(fig,ax2D,cmap,norm,interval)
    insert_point_graph_2D(ax2D,xc,yc,ms=3,r=radio )
    print('Tiempo en graficar 2D: ',time.time()-t1)
    t1=time.time()
    Graph_Point3D(fig,ax3D,XX,YY,ZZ,CC,azi,elev)
    insert_point_graph_3D(ax3D,xc,yc,zc,s=20)
    print('Tiempo en graficar 3D: ',time.time()-t1)
    plt.tight_layout()
    return fig,axs,ax2D,ax3D

def insert_cluster_graph2D(ax2D,clusters,x_min,y_min,cell_size,r=30,colorCircle='k',fontsize=10):
    if len(clusters)>0:
        for num,ij in enumerate(clusters):
            i,j=ij[0],ij[1]
            x,y=ntu.get_xy_from_ij(i,j,x_min,y_min,cell_size)
            insert_point_graph_2D(ax2D,x,y,r=r,text=str(num),colorCircle=colorCircle,fontsize=fontsize)
            # insert_point_graph_2D(ax2D,x,y,text=str(num))
            

def insert_cluster_graph3D(ax3D,clusters,Z,x_min,y_min,cell_size,s=60,color='k',fontsize=5):
    if len(clusters)>0:
        for num,ij in enumerate(clusters):
            i,j=ij[0],ij[1]
            x,y=ntu.get_xy_from_ij(i,j,x_min,y_min,cell_size)
            z=Z[ij[0],ij[1]]
            if z is np.nan:
                z=np.nanmean(Z[ij[0]-3:ij[0]+3,ij[1]-3:ij[1]+3])
            insert_point_graph_3D(ax3D,x,y,z,text=str(num),s=s,color=color,fontsize=fontsize)
                
def set_cells_color(CellsColors,IndexColor,IJ,cmap1):
    t1=time.time()
    CellsColors[:,:]=[1.,1.,1.,1.]
    # CellsColors[IJ[:,0],IJ[:,1]]=[cmap1[index] for index in IndexColor]
    for index,ij in zip(IndexColor,IJ):
        i,j=ij[0],ij[1]
        CellsColors[i,j]=cmap1[index]
    
    print('tiempo en asignar valores a MP, CellsColor : ', time.time()-t1)
def do_pdf_for_clusters(clusters,pdf,x_min,y_min,cell_size,title,radio,Ncells,colors_array,TW,TS,XY,X,Y,Z,IJ,
                            IndexColor,CellsColors,DataColors_list,azi,elev):
  
    for num,ij in enumerate(clusters):
        print('############## cluster: ',num,' ####################')
        i,j=ij[0],ij[1]
        x,y=ntu.get_xy_from_ij(i,j,x_min,y_min,cell_size)
        z=Z[ij[0],ij[1]]
        if z is np.nan:
            z=np.nanmean(Z[ij[0]-3:ij[0]+3,ij[1]-3:ij[1]+3])
        
        NCells=len(clusters[ij][0])
        Area=NCells*cell_size**2
        title1=title+'\nGrupo: '+str(num)+'\n centroide x-y-z : %.1f-%.1f-%.1f'%(x,y,z)
        title1=title1+'\n Num cells: %d , Area: %.1f m2'%(NCells,Area)
        fig,axs1,ax2D,ax3D=do_graph_TS_2D_3D(num,x,y,z,i,j,radio,Ncells,title1,colors_array,TW,TS,XY,X,Y,Z,IJ,
                                          IndexColor,CellsColors,DataColors_list,azi,elev)
                
        fig.savefig(pdf, format='pdf')
        fig.clf()
        ax2D.clear(),ax3D.clear()
        plt.close(fig)
        del fig,axs1,ax2D,ax3D
        gc.collect()
            
def create_page_with_title(TitlePage,pdf):
    PageFig = plt.figure(figsize=(21,18))
    PageFig.clf()
    PageFig.text(0.5,0.5,TitlePage, transform=PageFig.transFigure, size=24, ha="center")
    PageFig.savefig(pdf,format='pdf')
    plt.close(PageFig)
    del PageFig
    gc.collect()
    

    
def do_graphs(clusters,clusters2,X,Y,Z,PM,Prob,XY,TS,TW,CellsColors,DataColors_list,INFO,PathImg,SaveName,title,radar_list,alerts_list,NameRadar):
    mpl.use('Agg')
    ProyectName,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
    x_min,_,y_min,_=lim_axes
    IJ=np.array([ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size) for x,y in XY],dtype=np.int32)
    ################################graficas###################################
    
    #Caracteristicas de colores
    norm,cmap,cmap1,interval=DataColors_list
    IndexColor=norm(Prob)
    
    
    
    
    #Evaluar solo una zona con time serie
    
        
    # fig,ax2D,ax3D=create_graph_2D_3D(title)
    fig2D, ax2D=create_graph_2D(title)
    fig3D, ax3D=create_graph_3D(title)
    
        
    t1=time.time()
    colors_array=np.array([CellsColors[i,j] for i,j in IJ])
    init_point_graph_2D(ax2D,X,Y,Z)
    Graph_Point2D(ax2D,XY,colors_array)
    set_colorbar_graph2D(fig2D,ax2D,cmap,norm,interval)
    
    
    radar_dic={name:(azi,elev) for _,_,_,name,azi,elev in radar_list}
    
    azi,elev=radar_dic[NameRadar]
    for x,y,z,texto,_,_ in radar_list:
        insert_point_graph_2D(ax2D,x,y,r=5,text=texto)
    print('Tiempo en graficar 2D: ',time.time()-t1)
    
    
    
        
    t1=time.time()
    Graph_Point3D(fig3D,ax3D,X,Y,Z,CellsColors,azi,elev)
    for x,y,z,Tipo,Fecha in alerts_list:
        text=('%s\n%s')%(Tipo,Fecha)
        insert_point_graph_3D(ax3D,x,y,z,text=text,s=5,color='b',fontsize=2)
    print('Tiempo en graficar 3D: ',time.time()-t1)
    
    t2=time.time()
    
    if len(clusters)>0:
        # se inserta los puntos de cada conjunto de celdas contiguas 
        # en los graficos 2D y 3D
        
        radio=60
        Ncells=int(np.round(radio/cell_size))+1
        
        t1=time.time()
        insert_cluster_graph2D(ax2D,clusters,x_min,y_min,cell_size,r=30)
        insert_cluster_graph2D(ax2D,clusters2,x_min,y_min,cell_size,r=10,colorCircle='w',fontsize=2)
        print('Tiempo que se tarda en insertar los cluster en 2D: ', time.time()-t1)
        
        t1=time.time()
        insert_cluster_graph3D(ax3D,clusters,Z,x_min,y_min,cell_size)
        print('Tiempo que se tarda en insertar los cluster en 3D: ', time.time()-t1)
        # insert_cluster_graph3D(ax3D,clusters2,Z,x_min,y_min,cell_size,s=5,color='w',fontsize=2)
        
        TitlePage='Grupos de Celdas con CR a un radio de '+str(30)
        pdf1 = PdfPages(PathImg+SaveName+'.pdf')
        
        create_page_with_title(TitlePage,pdf1)
        
        do_pdf_for_clusters(clusters,pdf1,x_min,y_min,cell_size,title,radio,Ncells,colors_array,TW,TS,XY,X,Y,Z,IJ,
                            IndexColor,CellsColors,DataColors_list,azi,elev)
        pdf1.close()
        if len(clusters)<15:
            pdf2 = PdfPages(PathImg+SaveName+'_0.pdf')
            TitlePage='Conjunto de Celdas Contiguas'
            create_page_with_title(TitlePage,pdf2)
            
            radio=30
            Ncells=int(np.round(radio/cell_size))+1
            
            do_pdf_for_clusters(clusters2,pdf2,x_min,y_min,cell_size,title,radio,Ncells,colors_array,TW,TS,XY,X,Y,Z,IJ,
                                IndexColor,CellsColors,DataColors_list,azi,elev)
            pdf2.close()

        
        xyzg_list=ntu.get_xyzg_from_cluster(clusters,Z,x_min,y_min,cell_size)
        ntf.create_csv_for_ibis(PathImg,SaveName,xyzg_list)
    print('Tiempo en hacer pdf de los grupos: : ',time.time()-t2)
        
    t1=time.time()
    fig2D.savefig(PathImg+'2D_'+SaveName+'.png',dpi=400,optimize=True)
    print('Tiempo en guardar 2D: ',time.time()-t1)
    t1=time.time()
    fig3D.savefig(PathImg+'3D_'+SaveName+'.png',dpi=400,optimize=True)
    # fig3D.savefig(pp, format='pdf')
    print('Tiempo en guardar 3D: ',time.time()-t1)
    
    
    # plt.tight_layout()
    
    
    # fig.canvas.mpl_connect('button_press_event', OnSelect)
    fig2D.clf(),fig3D.clf()
    ax2D.clear(),ax3D.clear()
    plt.close(fig2D)
    plt.close(fig3D)
    del fig2D,fig3D,ax2D, ax3D
    gc.collect()
    

# def do_graphs2(X,Y,Z,PM,Prob,XY,TS,TW,DataColors_list,INFO,PathImg,SaveName,NameModel,radar_list,alerts_list):
#     ProyectName,_,n_rows,n_cols,lim_axes,cell_size,_,_=INFO
#     x_min,_,y_min,_=lim_axes
#     IJ=np.array([ntu.get_ij_from_xy(x,y,x_min,y_min,cell_size) for x,y in XY],dtype=np.int32)
#     ################################graficas###################################
    
#     #Caracteristicas de colores
#     norm,cmap,cmap1,CellsColors,interval=DataColors_list
#     IndexColor=norm(Prob)
    

#     CellsColors[:,:]=[1.,1.,1.,1.]
    
#     ii=ProyectName.upper().find('IBIS')
#     NameRadar=ProyectName[ii:ii+6].replace('_','').replace('-','').replace(' ','')
    
#     # azimut,elev=
#     FechaHora=ntu.datetime_to_str(TW[-1])
#     title='Radar '+NameRadar+'\n'+'Fecha: '+FechaHora+'\n'+NameModel
    
#     #Evaluar solo una zona con time serie
    
        
#     # fig,ax2D,ax3D=create_graph_2D_3D(title)
#     fig2D, ax2D=create_graph_2D(title)
#     fig3D, ax3D=create_graph_3D(title)
#     t1=time.time()
#     for index,ij in zip(IndexColor,IJ):
#         i,j=ij[0],ij[1]
#         CellsColors[i,j]=cmap1[index]
    
#     print('tiempo en asignar valores a MP, CellsColor : ', time.time()-t1)
        
#     t1=time.time()
#     init_point_graph_2D(ax2D,X,Y,Z)
#     Graph_Point2D(fig2D,ax2D,XY,Prob,cmap,norm,interval)
    
    
#     radar_dic={name:(azi,elev) for _,_,_,name,azi,elev in radar_list}
#     azi,elev=radar_dic[NameRadar]
#     for x,y,z,texto,_,_ in radar_list:
#         insert_point_graph_2D(ax2D,x,y,r=5,text=texto)
#     print('Tiempo en graficar 2D: ',time.time()-t1)
    
    
    
        
#     t1=time.time()
#     Graph_Point3D(fig3D,ax3D,X,Y,Z,CellsColors,azi,elev)
#     for x,y,z,Tipo,Fecha in alerts_list:
#         text=('%s\n%s')%(Tipo,Fecha)
#         insert_point_graph_3D(ax3D,x,y,z,text=text,s=5,color='b',fontsize=2)
#     print('Tiempo en graficar 3D: ',time.time()-t1)
        
#     t1=time.time()
#     clusters=tc.do_cluster(PM,umbral=0.9,len_min_cluster=10)
#     clusters=tc.join_clusters(clusters,cell_size,r=30)
#     print('tiempo en obtener los clusters : ', time.time()-t1)
    
  
#     radio=30
#     Ncells=int(np.round(radio/cell_size))+1
 
   

#     if len(clusters)>0:
#         # se inserta los puntos de cada conjunto de celdas contiguas 
#         # en los graficos 2D y 3D
#         for num,ij in enumerate(clusters):
#             i,j=ij[0],ij[1]
#             x,y=ntu.get_xy_from_ij(i,j,x_min,y_min,cell_size)
            
#             insert_point_graph_2D(ax2D,x,y,text=str(num))
#             z=Z[ij[0],ij[1]]
#             if z is np.nan:
#                 z=np.nanmean(Z[ij[0]-3:ij[0]+3,ij[1]-3:ij[1]+3])
#             insert_point_graph_3D(ax3D,x,y,z,text=str(num),fontsize=6)
        
        
#         pp = PdfPages(PathImg+SaveName+'.pdf')
        
#         for num,ij in enumerate(clusters):
#             print('############## cluster: ',num,' ####################')
#             i,j=ij[0],ij[1]
#             x,y=ntu.get_xy_from_ij(i,j,x_min,y_min,cell_size)
#             z=Z[ij[0],ij[1]]
#             if z is np.nan:
#                 z=np.nanmean(Z[ij[0]-3:ij[0]+3,ij[1]-3:ij[1]+3])
            
#             NumCells=len(clusters[ij][0])
#             Area=NumCells*cell_size**2
#             fig,axs1,ax2D1,ax3D1=do_graph_TS_2D_3D(num,x,y,z,i,j,radio,Ncells,title,Prob,TW,TS,XY,X,Y,Z,IJ,
#                                       IndexColor,CellsColors,cmap1,cmap,norm,interval,azi,elev)
            
#             fig.savefig(pp, format='pdf')
#             fig.clf()
#             ax2D1.clear(),ax3D1.clear()
#             plt.close(fig)
#             del fig,axs1,ax2D1,ax3D1
#             gc.collect()

            
#         xyzg_list=ntu.get_xyzg_from_cluster(clusters,Z,x_min,y_min,cell_size)
#         ntf.create_csv_for_ibis(PathImg,SaveName,xyzg_list)
        
#     t1=time.time()
#     fig2D.savefig(PathImg+'2D_'+SaveName+'.png',dpi=400)
#     print('Tiempo en guardar 2D: ',time.time()-t1)
#     t1=time.time()
#     fig3D.savefig(PathImg+'3D_'+SaveName+'.png',dpi=400)
#     # fig3D.savefig(pp, format='pdf')
#     print('Tiempo en guardar 3D: ',time.time()-t1)
#     pp.close()
    
#     # plt.tight_layout()
    
    
#     # fig.canvas.mpl_connect('button_press_event', OnSelect)
#     fig2D.clf(),fig3D.clf()
#     ax2D.clear(),ax3D.clear()
#     plt.close(fig2D)
#     plt.close(fig3D)
#     del fig2D,fig3D,ax2D, ax3D
#     gc.collect()
    